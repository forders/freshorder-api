/**
 * @Filename : constants.js
 * @Description : To write a server side constants.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var path = require('path');
var logpath = path.dirname(path.dirname(__dirname));
console.log(logpath);
exports.DIRNAME = logpath;
exports.SERVERNAME = "http://localhost:2001";

// SMS CREDENTIALS
exports.smsgatewayuri = "http://api.mVaayoo.com/mvaayooapi/MessageCompose";
exports.smsgatewayuser = "nambi@kilometer.in";
exports.smsgatewaypassword = "kani.mozhi9&";
exports.smsgatewaysenderID = "ORDERS";

// FILE PATH

/*Production filepath
exports.LOG = "/home/ec2-user/freshorder-api/log/freshorder.log";
exports.PRODUCTS = "/home/ec2-user/freshorder-api/server/storage/products/";
exports.EXPORT_PATH = "/home/ec2-user/freshorder-api/server/storage/export/";*/

/*DEV filepath*/
exports.PRODUCTS = logpath + '/server/storage/products/';
exports.EXPORT_PATH = logpath + '/server/storage/export/';
exports.CUSTOMER_PATH = logpath + '/server/storage/customer/';

//PUSH NOTIFICATION
exports.gcmServerApiKey = 'AIzaSyCod101OZcrAwyne-x45pVBwqUkw1Qzj6o';
exports.gcmAppname = 'freshorders-1349';
exports.senderid = '859643025277';

//INSTAMOJO/PAYMENT CREDENTIALS
exports.pymtgatewayuri = "https://test.instamojo.com/api/1.1/payments/";
exports.pymtgatewayApiKey = "fa85d3cf3fa291bfee0a77bbe5250553";
exports.pymtgatewayAuthToken = "b883990135d499a178c8f2d554be4ea2";

//EXPORT FILE
exports.CONFIG_PATH = "http://27.250.1.52:2001/api/files/export/download/";
exports.REDIRECTURL = "http://27.250.1.52/freshorder-admin/#paymentstatus?pymtid=";

//SMS TEMPLATES
exports.OTP_MSG = "Your registration with FreshOrders is successful. Your OTP is";
exports.PWD_MSG = "Your password is :";
exports.PYMNT_NOTIFI = "Your password is :";
exports.EXP_PYMT = "Your subscription ends on %s. Please make payment of %s to avoid service disruption. FreshOrders.in"

// Serial Number Generation
exports.ORDCODE = "BILL_NO";
exports.CUSCODE = "CUS_CODE";
exports.PARTYCODE = "PARTY_CODE";
exports.WRKTYPFILE = logpath + "/server/storage/worktype.json";