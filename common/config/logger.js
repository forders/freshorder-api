/**
 * @Filename : logger.js
 * @Description : To write a logger instance.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var logger = require('winston');// used npm package winston
var path=require('path');
var logpath=path.dirname(path.dirname(__dirname));
logger.add(logger.transports.File, { filename: logpath+'/log/freshorder.log' });	// set up a Logfile
exports.logger=logger;