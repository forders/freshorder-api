/**
 * @Filename : messages.js
 * @Description : To write server side messages.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 * 0.3		Nov 07	Preethi			Messages changed for list not found command
 * 0.4		Nov 08	Preethi			Comments added
 * 0.5		Nov 08	Nithya			Messages constants added
 */
exports.USER_SAVE_SUCCESS = 'Thank you for registering with us';
exports.UPDATE_SUCCESSFULLY = 'Updated Successfully';
exports.SAVE_SUCCESSFULLY = 'Saved Successfully';
exports.SAVE_FAILED = 'Failed to save your details';
exports.PROFILE_FAILED = 'Your details not saved. Please try again';
exports.PROFILE_SUCCESS = 'Your details updated successfully';
exports.PROD_SAVE_FAILED = 'Your product details not saved. Please try again';
exports.PROD_SAVE_SUCCESS = 'Your product details saved successfully';
exports.PROD_UPDATE_FAILED = 'Your product details not updated. Please try again';
exports.PROD_UPDATED_SUCCESS = 'Product Updating Successful';
exports.PROD_UPDATE_SUCCESS = 'Your product details updated successfully';
exports.ORDER_SAVE_FAILED = 'Order details not saved. Please try again';
exports.ORDER_SAVE_SUCCESS = 'Your order successfully placed';
exports.ORDER_UPDATE_FAILED = 'Your order details not updated. Please try again';
exports.ORDER_UPDATE_SUCCESS = 'Your order details updated successfully';
exports.COMPLAINTS_SAVE_SUCCESS = 'Your query raised to the dealer';
exports.COMPLAINTS_SAVE_FAILED = 'Your query raised to the dealer,Failed. Please try again';
exports.COMPLAINTS_DELETE_FAILED = 'Your complaint not deleted, Please try again';
exports.COMPLAINTS_DELETE_SUCCESS = 'Your complaint deleted successfully';
exports.DEALER_SAVE_SUCCESS = 'Dealer details added succesfully';
exports.DEALER_SAVE_FAILED = 'Dealer details save failed';
exports.DEALER_UPDATE_FAILED = 'Dealer details update failed';
exports.DEALER_UPDATE_SUCCESS = 'Dealer details updated succesfully';
exports.PYMT_SAVE_SUCCESS = 'Payment saved successfully';
exports.PYMT_SAVE_FAILED = 'Payment Failed';
exports.PYMT_UPDATE_SUCCESS = 'Payment details updated successfully';
exports.PYMT_UPDATE_FAILED = 'Payment details updated failed,Please try again';
exports.LOGIN_SUCCESS = 'Logged in Successfully';
exports.LOGIN_FAILED = 'Please provide valid user credentials';
exports.EXISTING_USER = 'Already registered with us, Please Login';
exports.OTP_PENDING = 'Account verification pending, Please activate your account';
exports.NOT_USER = 'Not an existing user, Please register with us';
exports.INACTIVE_USER = 'Inactive User, Please contact support';
exports.OTP_SENT = 'OTP sent, Please verify your account';
exports.OTP_VERIFIED = 'OTP verified successfully, Please login';
exports.OTP_NT_VERIFIED = 'Invalid OTP, Please try again';
exports.INTERNAL_ERR = "Network unavailable, Please try again later";
exports.LIST_NT_FOUND = "List not Found";
exports.MLIST_NT_FOUND = "No Merchant(s)";
exports.SLIST_NT_FOUND = "No Salesman(s)";
exports.DLIST_NT_FOUND = "No Dealer(s)";
exports.DEALERS_EMPTY = "No more dealers to add";
exports.LIST_FOUND = "List Found";
exports.DATA_NT_FOUND = "Data not Found";
exports.DATA_FOUND = "Data Found";
exports.RECORD_DELETED = "Record deleted Successfully";
exports.RECORD_NT_FOUND = "Record not Found";
exports.TBL_COUNT = "Table Count";
exports.MAXLENGTH_EXCEED = "Product code/name exceeds it maxlength";
exports.PWD_SENT_SUCCESS = "Password sent to your mobile";
exports.PWD_SENT_FAIL = "Request to your password is failed, please try again";
exports.PWD_CHANGED = "Your password has been changed";
exports.INVALID_PWD = "Invalid old password, please provide correct credentials";
exports.INCORRECT_PWD = "The password you entered is incorrect. Please try again.";
exports.STATUS_PENDING = "Pending";
exports.STATUS_ACTIVE = "Active";
exports.STATUS_INACTIVE = "Inactive";
exports.STATUS_INPROGRESS = "Inprogress";
exports.STATUS_DELIVERED = "Delivered";
exports.STATUS_CANCELLED = "Cancelled";
exports.STATUS_VERIFIED = "Verified";
exports.STATUS_DELETED = "Deleted";
exports.PYMT_MONTHLY = "Monthly";
exports.PYMT_YEARLY = "Yearly";
exports.PYMT_QUARTERLY = "Quarterly";
exports.PYMT_STATUS = "Credit";
exports.COMPLAINTS_LIST_NTFOUND = "Not received any complaints yet";
exports.PRODUCTS_LIST_NTFOUND = "No product(s) found for this dealer";
exports.ORDER_LIST_NTFOUND = "No Order(s) placed, Please place your first order";
exports.MERCHANT_NT_FOUND = "No Merchant(s) found to place order";
exports.DORDER_NT_FOUND = "No Order(s) requested from salesman/outlet";
exports.ID_CREATE_SUCCESS = "Created Successfully";
exports.ID_CREATE_FAILED = "Creating Failed";
exports.UPDATE_FAILED = 'Updation Failed';
exports.UPDATED = 'All Updated';
exports.USERIMP_SAVE_SUCCESS = 'Your customer details added successfully';
exports.USERIMP_SAVE_FAILED = 'Failed to save customer details, Please try again';
exports.PYMT_PENDING = "Please make payment before placing this order";
exports.UPLOAD_SUCCESS = "Uploaded successfully";
exports.UPLOAD_FAILED = "Uploaded failed";
exports.APPSETTING_SAVE_SUCCESS = "Your settings saved successfully";
exports.APPSETTING_SAVE_FAILED = "Failed to save your settings";
exports.APPSETTING_UPDATE_SUCCESS = "Your settings updated successfully";
exports.APPSETTING_UPDATE_FAILED = "Failed to update your settings";
exports.INDUS_SAVE_SUCCESS = "Industry added successfully";
exports.INDUS_SAVE_FAILED = "Failed to add industry";
exports.INDUS_UPDATE_SUCCESS = "Industry details updated successfully";
exports.INDUS_UPDATE_FAILED = "Failed to update industry details";
exports.ROUTE_SAVE_SUCCESS = "Successfully saved the route plans of the salesman";
exports.ROUTE_SAVE_FAILED = "Failed to save this route assignment, Please try again later";
exports.ROUTE_NT_AVAIL = "No appointment(s) assigned";
exports.ROUTE_UPDATE_SUCCESS = "Successfully updated the route plans of the salesmans";
exports.ROUTE_UPDATE_FAILED = "Failed to update your changes, Please try again later";
exports.PROSETTING_SAVE_SUCCESS = "Your product settings saved successfully";
exports.PROSETTING_UPDATE_SUCCESS = "Your product settings updated successfully";
exports.LOGS_FOUND = "Found track logs";
exports.LOGS_NT_FOUND = "No data found";
exports.PACK_SAVE_SUCCESS = "Your packinglist saved successfully";
exports.PACK_SAVE_FAILED = "Failed to save your details";
exports.PACK_UPDATE_SUCCESS = "Your packing details updated successfully";
exports.PACK_UPDATE_FAILED = "Failed to update your packing details";
exports.SALEBILDTL_SAVE_SUCCESS = "Sales Bill Details saved successfully";
exports.SALEBILDTL_SAVE_FAILED = "Failed to Save Sales Bill Details";
exports.SALEBILDTL_UPDATE_SUCCESS = "Sales Bill Details updated successfully";
exports.SALEBILDTL_UPDATE_FAILED = "Failed to update Sales Bill Details";
exports.SALEBIL_SAVE_SUCCESS = "Sales bill saved successfully";
exports.SALEBIL_SAVE_FAILED = "Failed to save Sales Bill";
exports.SALEBIL_UPDATE_SUCCESS = "Sales bill updated successfully";
exports.SALEBIL_UPDATE_FAILED = "Failed to update Sales Bill";
exports.BILL_SAVE_SUCCESS = "Bill saved successfully";
exports.SLNO_SAVE_SUCCESS = "Serial number saved successfully";
exports.SLNO_SAVE_FAILED = "Serial number save failed";
exports.SLNO_UPDATE_SUCCESS = "Serial number updated successfully";
exports.SLNO_UPDATE_FAILED = "Failed to serial number generation, please try again";
exports.STOCK_SAVE_SUCCESS = "Stock entry saved successfully";
exports.STOCK_SAVE_FAILED = "Stock entry save failed";
exports.STOCK_UPDATE_SUCCESS = "Stock entry updated successfully";
exports.STOCK_UPDATE_FAILED = "Failed to stock entry, please try again ";
exports.SM_SAVE_SUCCESS = "Successfully added the salesman details";
exports.M_SAVE_SUCCESS = "Successfully added the merchant details";
exports.PROD_AVAIL = "Product with same code and name already exists";
exports.C_SAVE_SUCCESS = "Successfully added the customer details";
exports.SCHEME_SAVE_SUCCESS = "Scheme details saved successfully";
exports.SCHEME_SAVE_FAILED = "Scheme details save failed";
exports.SCHEME_UPDATE_SUCCESS = "Scheme details updated successfully";
exports.SCHEME_UPDATE_FAILED = "Failed to update Scheme details";
exports.APPSETTING_DELETE_SUCCESS = "Successfully updated settings";
exports.EXIST = "Already existing scheme, please try again";
exports.BEAT_AVAIL = "Beat details exists";
exports.DATA_REQUIRED = "Please provide required details";
exports.USERDATA_REQUIRED = "Please enter name";
exports.USER_AVAIL = "Already existing details";
exports.INVALID_ORDID = "Valid ordid required";
exports.INVALID_ORDLIST = "Valid order details required";
exports.INVALID_PRODID = "Valid product details required";
exports.VALID_USERID = "Valid user id required";
exports.VALID_DEALERID = "Valid dealer details required";
exports.PWD_CHANGE_FAILED = "Failed to update password";
exports.EXISTING_SALESMAN = "Salesman details already exists";
exports.EXISTING_MERCHANT = "Merchant details already exists";