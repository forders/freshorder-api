'use strict';
/**
 * @Filename : tbl-beat.js
 * @Description : To write hooks for the beat related operation.
 * @Author : Gomathi
 * @Date : Feb 07,2017
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Feb 07 	Gomathi 			
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var config = require('../config/constants.js');
module.exports = function(Tblbeat) {
// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		log.info(ctx.args.data);
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				data : model.beatid,
				message : appmsg.SAVE_SUCCESSFULLY
			}
			log.info(filename + ">>modifySaveResponse>>"+response.message);
		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifySaveResponse>>"+response.message);
		}
		
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	}// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			if (model.length === 0) {
				var response = {
					status : false,
					message : appmsg.LIST_NT_FOUND
				}
				log.info(filename + ">>modifyFindResponse>>"+response.message);
			} else {
				var response = {
					status : true,
					data : model,
					message : appmsg.LIST_FOUND
				}
				log.info(filename + ">>modifyFindResponse>>"+response.message);
			}

		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifyFindResponse>>"+response.message);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	} 
	
	// Bulk update/save beat
	Tblbeat.batchPut = function(ctx, cb) {
		try{
			log.info(ctx.args.data);
			var beatlist = ctx.args.data.beatlist;
			var len = beatlist.length;
			for (var i = 0; i < beatlist.length; i++) {
				Tblbeat.upsert(beatlist[i], function(err, beat) {
					if (err) {
						var response = {
							status : false,
							message : appmsg.INTERNAL_ERR
						}
						log.error(filename + ">>batchPut>>");
						ctx.res.status(500);
						ctx.res.send(response);
					} else {
						len--;
						if (len == 0) {
							try{
								if(ctx.args.data.removelist){
									if(ctx.args.data.removelist.length != 0){
										for(var j=0;j<ctx.args.data.removelist.length;j++){
											Tblbeat.deleteById(ctx.args.data.removelist[j], function (err, beats) {
												if(err){
													log.error(err);
												}else{
													log.info(beats);
												}
											});
										}
									}
								}
							}catch(ex){
								var response = {
										status : false,
										message : appmsg.INTERNAL_ERR
									}
								log.error(filename + ">>batchPut>>");
								ctx.res.status(500);
								ctx.res.send(response);
							}
							var response = {
								status : true,
								message : appmsg.SAVE_SUCCESSFULLY
							}
							log.info(filename + ">>batchPut>>");
							ctx.res.status(200);
							ctx.res.send(response);
						}
					}
				});
			}
		}catch(e){
			console.log(e);
			var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
				log.error(filename + ">>batchPut>>");
				ctx.res.status(500);
				ctx.res.send(response);
		}
		
	};
	Tblbeat.remoteMethod('batchPut', {
		description : ' Bulk update or save beat details',
		accepts : [{
			arg : 'ctx',
			type : 'object',
			http : {
				source : 'context'
			}
		}, {
			arg : 'data',
			type : 'object',
			http : {
				source : 'body'
			}
		}],
		returns : {
			arg : 'message',
			type : 'object',
			root : true
		},
		http : {
			verb : 'post'
		}
	});
	
	
	Tblbeat.merchantlist = function (ctx, cb) {
		var ds = Tblbeat.dataSource;
		var response = {};
		console.log(ctx.args.data);
		if (ctx.args.data.status != null && ctx.args.data.status != undefined) {
			var status = ctx.args.data.status;
		} else {
			var status = appmsg.STATUS_ACTIVE;
		}
		if (ctx.args.data.usertype == "D" && ctx.args.data.suserid) {
            var query = "select u . *, ud.usrdlrid,b.* from tbl_user_dealer ud, tbl_user u left join tbl_beat b on b.muserid=u.userid where ud.muserid = u.userid and u.usertype = 'M' and ud.duserid = ? and u.userstatus = ? and ud.dstatus = ?";                   
            var qryArr = [ctx.args.data.duserid, status, status];
		} 

		ds.connector.execute(query, qryArr, function (err, list) {
			if (err) {
				console.error(err);
				response.status = false;
				response.message = appmsg.INTERNAL_ERR;
				log.error(filename + ">>merchantlist>>" + response.message);
				ctx.res.send(response);
			} else {
				if (list.length != 0) {
					response.status = true;
					response.message = appmsg.LIST_FOUND;
					response.data = list;
                                        response.len= list.length;
				} else {
					response.status = false;
					response.message = appmsg.LIST_NT_FOUND;

				}
				log.info(filename + ">>merchantlist>>" + response.message);
				ctx.res.send(response);
			}
		});
	};
	Tblbeat
		.remoteMethod(
		'merchantlist',
		{
			description: 'Get the merchant list based on the query triggered',
			accepts: [{
				arg: 'ctx',
				type: 'object',
				http: {
					source: 'context'
				}
			}, {
				arg: 'data',
				type: 'object',
				http: {
					source: 'body'
				}
			}],
			returns: {
				arg: 'message',
				type: 'object',
				root: true
			},
			http: {
				verb: 'post'
			}
		}); 

		function modifySavecheck(ctx, model, next){
			if(ctx.args.data){

			}
			console.log(ctx.args.data);
			next();
		}

    Tblbeat.beforeRemote('create', modifySavecheck);
    Tblbeat.afterRemote('create', modifySaveResponse);
    Tblbeat.afterRemote('find', modifyFindResponse);
};
