/**
 * @Filename : tbl-complaints.js
 * @Description : To write hooks for the complaints related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(TblComplaints) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				data : model.complid,
				message : appmsg.COMPLAINTS_SAVE_SUCCESS
			}			
		} else {
			var response = {
				status : false,
				message : appmsg.COMPLAINTS_SAVE_FAILED
			}
		}
		log.info(filename+">>modifySaveResponse>>"+response.message);
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	}
	// Response Message after UPDATE
	function modifyUpsertResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				data : model.complid,
				message : appmsg.UPDATE_SUCCESSFULLY
			}
		log.info(filename+">>modifyUpsertResponse>>"+response.message);
		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
		log.error(filename+">>modifyUpsertResponse>>"+response.message);
		}		
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	}

	// Response Message after UPDATEALL
	function modifyUpdateAllResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				message: appmsg.COMPLAINTS_DELETE_SUCCESS
			}
			log.info(filename+">>modifyUpdateAllResponse>>"+response.message);
		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename+">>modifyUpdateAllResponse>>"+response.message);
		} else {
			var response = {
				status : false,
				message : appmsg.COMPLAINTS_SAVE_FAILED
			}
			log.error(filename+">>modifyUpdateAllResponse>>"+response.message);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	}
	// Request Alteration before UpdateAll
	function modifyUpdateAllObject(ctx, model, next) {
		ctx.req.query.where = {
			complid : ctx.args.data.complid
		};
		log.info(filename+">>modifyUpdateAllObject>>");
		next();
	}
	// Request Alteration before UpdateAll
	function modifySaveObject(ctx, model, next) {
		log.info(filename+">>modifySaveObject>>");
		try{
			console.log(ctx.req.query);
			if(JSON.stringify(ctx.req.query)!='{}'){
				ctx.args.data=ctx.req.query;
		app.dataSources.filestorage.connector.getFilename = function(file, req,
				res) {
			var origFilename = file.name;
			var parts = origFilename.split('.'), extension = parts[parts.length - 1];
			var newFilename = common.generateOTP(4) + '-'
					+ common.generateOTP(2) + '.' + extension;
			return newFilename;
		};
		ctx.req.params.container = 'complaints';
		var options = {};
		app.models.files.upload(ctx.req, ctx.result, options, function(
				err, fileObj) {
			if (err) {
				cb(err);
				log.error(filename + ">>upload>>");
			} else {
				if(JSON.stringify(fileObj.files)!='{}'){
					var fileInfo = fileObj.files.file[0];
					ctx.args.data.status=appmsg.STATUS_PENDING;	
					ctx.args.data.attchmnt = fileInfo.name;
					next();
				}else{
					ctx.args.data.status=appmsg.STATUS_PENDING;	
					next();
				}					
			}
		});
			}else{
				ctx.args.data.status=appmsg.STATUS_PENDING;	
				next();
			}
		}catch(e){
			console.log(e);
		}
	}
	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			if (model.length === 0) {
				var response = {
					status : false,
					message : appmsg.LIST_NT_FOUND
				}
				log.info(filename + ">>modifyFindResponse>>"+response.message);
			} else {
				var response = {
					status : true,
					data : model,
					message : appmsg.COMPLAINTS_LIST_NTFOUND
				}
				log.info(filename + ">>modifyFindResponse>>"+response.message);
			}

		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifyFindResponse>>"+response.message);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	}
	// Request Alteration before Find data
	function modifyFindRequest(ctx, model, next) {
		log.info(filename + ">>modifyFindRequest>>");
		if(ctx.req.query.filter.where.duserid){
			ctx.req.query.filter.include=[{relation:"muser", 
				scope: {
				  	fields:["fullname","companyname","address","mobilno"]
						}
			  }];
		}
	next();
	}
	TblComplaints.afterRemote('create', modifySaveResponse);
	TblComplaints.afterRemote('find', modifyFindResponse);
	TblComplaints.afterRemote('updateAll', modifyUpdateAllResponse);
	TblComplaints.beforeRemote('updateAll', modifyUpdateAllObject);
	TblComplaints.beforeRemote('create', modifySaveObject);
	TblComplaints.beforeRemote('find', modifyFindRequest);
};
