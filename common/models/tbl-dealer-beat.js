'use strict';
/**
 * @Filename : tbl-app-settings.js
 * @Description : To write hooks app-settings.
 * @Author : Nithya
 * @Date : Oct 06, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version Date Modified By Remarks 0.1 Oct 06 Nithya Remote hooks added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(Tbldealerbeat) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.id,
					message : appmsg.SAVE_SUCCESSFULLY
				}
			} else {
				var response = {
					status : false,
					message : appmsg.SAVE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after UPDATE/UPSERT
	function modifyUpdateResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.id,
					message : appmsg.SAVE_SUCCESSFULLY
				}
			} else {
				var response = {
					status : false,
					message : appmsg.SAVE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				if (model.length === 0) {
					var response = {
						status : false,
						message : appmsg.LIST_NT_FOUND
					}
				} else {
					var response = {
						status : true,
						data : model,
						message : appmsg.LIST_FOUND
					}
				}

			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
			}
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	function modifySaveRequest(ctx, model, next) {
		try{
			log.info(ctx.args.data);
			var query = 'SELECT * FROM tbl_dealer_beat where beatnm=? and duserid=? and status=?';
			var qryArr = [ctx.args.data.beatnm,ctx.args.data.duserid,ctx.args.data.status]
			common.executeQuery(query,qryArr,function(result){
				if(result.data==0){
					next();
				}else{
					var response = {
							status : false,
							message : appmsg.BEAT_AVAIL
						}
					ctx.res.send(response);
				}
			});
		}catch(E){
			log.error(E);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	};
	
	/**
	 * To get the salesman distributor list of the logged in user
	 * @method smdistributorlist
	 * @param  {Object} ctx - Request object 
	 * @param  {Object} cb - Callback object
	 * @return {Object} response - Object contains status and message
	 */
	Tbldealerbeat.smdistributorlist = function (ctx, cb) {
		try{
			var ds = Tbldealerbeat.dataSource;
			var response = {};
			log.info(ctx.args.data);
			if(ctx.args.data.suserid){
				var query = "select u.*,us.fullname,us.usertype,us.companyname from tbl_beat as u JOIN tbl_user as us ON us.userid = u.duserid where u.duserid in (select muserid from tbl_user_dealer where duserid = ? and dstatus='Active') and u.muserid IS NULL and u.suserid IS NULL";
				var qryArr = [ctx.args.data.suserid]
				ds.connector.execute(query, qryArr, function (err, list) {
					if (err) {
						console.error(err);
						response.status = true;
						response.message = appmsg.INTERNAL_ERR;
						log.error(filename + ">>smdistributorlist>>");
						ctx.res.send(response);
					} else {
						if (list.length != 0) {
							response.status = true;
							response.message = appmsg.LIST_FOUND;
							response.data = list;
						} else {
							response.status = false;
							response.message = appmsg.LIST_NT_FOUND;
						}
						log.info(filename + ">>smdistributorlist>>");
						ctx.res.send(response);
					}
				});
			}else{
				response.status = false;
				response.message = appmsg.LIST_NT_FOUND;
				ctx.res.send(response);
			}
		}catch(e){
			console.error(err);
			response.status = true;
			response.message = appmsg.INTERNAL_ERR;
			log.error(filename + ">>smdistributorlist>>");
			ctx.res.send(response);
		}
	};
	Tbldealerbeat
		.remoteMethod(
		'smdistributorlist',
		{
			description: 'Get the distrubutor list based on the query triggered',
			accepts: [{
				arg: 'ctx',
				type: 'object',
				http: {
					source: 'context'
				}
			}, {
				arg: 'data',
				type: 'object',
				http: {
					source: 'body'
				}
			}],
			returns: {
				arg: 'message',
				type: 'object',
				root: true
			},
			http: {
				verb: 'post'
			}
		});		
	Tbldealerbeat.afterRemote('create', modifySaveResponse);
	Tbldealerbeat.afterRemote('updateAttributes', modifyUpdateResponse);
	Tbldealerbeat.afterRemote('updateAll', modifyUpdateResponse);
	Tbldealerbeat.afterRemote('upsert', modifyUpdateResponse);
	Tbldealerbeat.afterRemote('find', modifyFindResponse);
	Tbldealerbeat.beforeRemote('upsert', modifySaveRequest);
};