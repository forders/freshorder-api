/**
 * @Filename : tbl-industry.js
 * @Description : To write hooks industry.
 * @Author : Nithya
 * @Date : Oct 06, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version Date Modified By Remarks 0.1 Oct 06 Nithya Remote hooks added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(Tblindustry) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.industryid,
					message : appmsg.INDUS_SAVE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.INDUS_SAVE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after UPDATE/UPSERT
	function modifyUpdateResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.industryid,
					message : appmsg.INDUS_UPDATE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.INDUS_UPDATE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				if (model.length === 0) {
					var response = {
						status : false,
						message : appmsg.LIST_NT_FOUND
					}
				} else {
					var response = {
						status : true,
						data : model,
						message : appmsg.LIST_FOUND
					}
				}

			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
			}
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	Tblindustry.afterRemote('create', modifySaveResponse);
	Tblindustry.afterRemote('updateAttributes', modifyUpdateResponse);
	Tblindustry.afterRemote('updateAll', modifyUpdateResponse);
	Tblindustry.afterRemote('upsert', modifyUpdateResponse);
	Tblindustry.afterRemote('find', modifyFindResponse);
};
