/**
 * @Filename : tbl-orders-dtl.js
 * @Description : To write hooks for the order details related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	   Modified By 	    Remarks 
 * 0.1 		Jun 08 	   Nithya 			
 * 0.2		Nov 05     Nithya			Comments added
 * 0.3      Jul 22,17  Nithya           Query Param added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var constants = require('../config/constants.js');
var config = require('../config/constants.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
var validation = require('../validation/custom-validation');
module.exports = function(TblOrderDtl) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.orddtlid,
                    message: appmsg.ORDER_SAVE_SUCCESS
                }
                log.info(filename + ">>modifySaveResponse>>" + response.message);
                ctx.res.status(status);
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifySaveResponse>>" + response.message);
                ctx.res.status(status);
            } else {
                var response = {
                    status: false,
                    message: appmsg.ORDER_SAVE_FAILED
                }
                log.error(filename + ">>modifySaveResponse>>" + response.message);
            }
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.send(response);
        } catch (E) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    message: appmsg.ORDER_UPDATE_SUCCESS
                }
                log.info(filename + ">>modifyUpsertResponse>>" + response.message);
                ctx.res.status(status);
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            } else {
                var response = {
                    status: false,
                    message: appmsg.ORDER_UPDATE_FAILED
                }
                log.info(filename + ">>modifyUpsertResponse>>" + response.message);
                ctx.res.status(status);
            }
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.send(response);
        } catch (E) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                if (model.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.DORDER_NT_FOUND
                    }
                } else {
                    var response = {
                        status: true,
                        data: model,
                        message: appmsg.LIST_FOUND
                    }
                }
                log.info(filename + ">>modifyFindResponse>>" + response.message);
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifyFindResponse>>" + response.message);
            }
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (E) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    /**
     * To Bulk Update Order details
     * @method bulkorderupdate
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrderDtl.bulkorderupdate = function(ctx, cb) {
        try {
            log.info(ctx.args.data);
            var orderDtls = ctx.args.data.orderdtls;
            var len = orderDtls.length;
            for (var i = 0; i < orderDtls.length; i++) {
                if (orderDtls[i].fqty != undefined && orderDtls[i].fqty > 0) {
                    var obj = {
                        orddtlid: orderDtls[i].orddtlid,
                        ordid: orderDtls[i].ordid,
                        duserid: orderDtls[i].duserid,
                        prodid: orderDtls[i].prodid,
                        qty: orderDtls[i].fqty,
                        isfree: 'F',
                        ordslno: orderDtls[i].ordslno,
                        stock: orderDtls[i].stock,
                        deliverydt: orderDtls[i].deliverydt,
                        ordstatus: appmsg.STATUS_PENDING,
                        ord_uom: orderDtls[i].ord_uom,
                        default_uom: orderDtls[i].default_uom,
                        updateddt: new Date()
                    }
                    if (orderDtls[i].prate != null && orderDtls[i].prate != undefined && orderDtls[i].prate != 'null') {
                        obj.prate = orderDtls[i].prate;
                    }
                    if (orderDtls[i].ptax != null && orderDtls[i].ptax != undefined && orderDtls[i].ptax != 'null') {
                        obj.ptax = orderDtls[i].ptax;
                    }
                    if (orderDtls[i].pvalue != null && orderDtls[i].pvalue != undefined && orderDtls[i].pvalue != 'null') {
                        obj.pvalue = orderDtls[i].pvalue;
                    }
                    if (orderDtls[i].manufdt != null && orderDtls[i].manufdt != undefined && orderDtls[i].manufdt != 'null') {
                        obj.manufdt = orderDtls[i].manufdt;
                    }
                    if (orderDtls[i].expirydt != null && orderDtls[i].expirydt != undefined && orderDtls[i].expirydt != 'null') {
                        obj.expirydt = orderDtls[i].expirydt;
                    }
                    if (orderDtls[i].batchno != null && orderDtls[i].batchno != undefined && orderDtls[i].batchno != 'null') {
                        obj.batchno = orderDtls[i].batchno;
                    }
                    if (orderDtls[i].deliverytype != null &&
                        orderDtls[i].deliverytype != undefined) {
                        obj.deliverytype = orderDtls[i].deliverytype;
                    }
                    if (orderDtls[i].deliverytime != null &&
                        orderDtls[i].deliverytime != undefined) {
                        obj.deliverytime = orderDtls[i].deliverytime;
                    }
                    if (orderDtls[i].cqty != null &&
                        orderDtls[i].cqty != undefined) {
                        obj.cqty = orderDtls[i].cqty;
                    }
                    TblOrderDtl.upsert(obj);
                }
                if (orderDtls[i].prodid != 0) {
                    var insertObj = {
                        orddtlid: orderDtls[i].orddtlid,
                        ordid: orderDtls[i].ordid,
                        prodid: orderDtls[i].prodid,
                        isfree: orderDtls[i].isfree,
                        ordslno: orderDtls[i].ordslno,
                        stock: orderDtls[i].stock,
                        deliverydt: orderDtls[i].deliverydt,
                        ord_uom: orderDtls[i].ord_uom,
                        default_uom: orderDtls[i].default_uom,
                        ordstatus: orderDtls[i].ordstatus,
                        updateddt: new Date()
                    };
                    if (orderDtls[i].manufdt != null && orderDtls[i].manufdt != undefined && orderDtls[i].manufdt != 'null' && orderDtls[i].manufdt != '') {
                        insertObj.manufdt = orderDtls[i].manufdt;
                    }
                    if (orderDtls[i].expirydt != null && orderDtls[i].expirydt != undefined && orderDtls[i].expirydt != 'null' && orderDtls[i].expirydt != '') {
                        insertObj.expirydt = orderDtls[i].expirydt;
                    }
                    if (orderDtls[i].batchno != null && orderDtls[i].batchno != undefined && orderDtls[i].batchno != 'null' && orderDtls[i].batchno != '') {
                        insertObj.batchno = orderDtls[i].batchno;
                    }
                    if (orderDtls[i].deliverytype != null &&
                        orderDtls[i].deliverytype != undefined) {
                        insertObj.deliverytype = orderDtls[i].deliverytype;
                    }
                    if (orderDtls[i].deliverytime != null &&
                        orderDtls[i].deliverytime != undefined) {
                        insertObj.deliverytime = orderDtls[i].deliverytime;
                    }
                    if (orderDtls[i].cqty != null &&
                        orderDtls[i].cqty != undefined) {
                        insertObj.cqty = orderDtls[i].cqty;
                    }
                    if (orderDtls[i].prate != null && orderDtls[i].prate != undefined && orderDtls[i].prate != 'null') {
                        insertObj.prate = orderDtls[i].prate;
                    }
                    if (orderDtls[i].ptax != null && orderDtls[i].ptax != undefined && orderDtls[i].ptax != 'null') {
                        insertObj.ptax = orderDtls[i].ptax;
                    }
                    if (orderDtls[i].pvalue != null && orderDtls[i].pvalue != undefined && orderDtls[i].pvalue != 'null') {
                        insertObj.pvalue = orderDtls[i].pvalue;
                    }
                } else if (orderDtls[i].image == 'Y') {
                    var insertObj = {
                        orddtlid: orderDtls[i].orddtlid,
                        ordimage: orderDtls[i].ordimage,
                        updateddt: new Date()
                    }
                    if (ctx.args.data.deletelist != null && ctx.args.data.deletelist != undefined) {
                        for (var j = 0; j < ctx.args.data.deletelist.length; j++) {
                            var container = 'orders';
                            var file = ctx.args.data.deletelist[j];
                            app.models.files.removeFile(container, file, function(err) {
                                log.info(filename + ">>file removed>>" + file);
                            });
                        }
                    }
                }

                if (orderDtls[i].qty != null && orderDtls[i].qty != undefined && orderDtls[i].qty > 0) {
                    insertObj.qty = orderDtls[i].qty;
                }
                if (orderDtls[i].duserid != null && orderDtls[i].duserid != undefined) {
                    insertObj.duserid = orderDtls[i].duserid;
                }
                if (orderDtls[i].ordstatus == appmsg.STATUS_CANCELLED) {
                    var msg = appmsg.ORDER_MSG;
                    app.models.TblOrders.findOne({
                        where: {
                            ordid: orderDtls[i].ordid
                        },
                        include: ['muser', 'suser']
                    }, function(err, response) {
                        log.info(response.toJSON());
                        var i = response.toJSON();
                        if (i.suserid != null) {
                            var Smobileno = i.suser.mobileno;
                            var Mmobileno = i.muser.mobileno;
                            if (Mmobileno.length == 10) {
                                var smsResult = common.sendSMS(constants.smsgatewayuri,
                                    constants.smsgatewayuser, constants.smsgatewaypassword,
                                    constants.smsgatewaysenderID, Mmobileno, msg);
                            }
                            if (Smobileno.length == 10) {
                                var smsResult = common.sendSMS(constants.smsgatewayuri,
                                    constants.smsgatewayuser, constants.smsgatewaypassword,
                                    constants.smsgatewaysenderID, Smobileno, msg);
                            }
                        } else {
                            var smsResult = common.sendSMS(constants.smsgatewayuri,
                                constants.smsgatewayuser, constants.smsgatewaypassword,
                                constants.smsgatewaysenderID, Mmobileno, msg);
                        }

                    });
                }
                TblOrderDtl.upsert(insertObj, function(err, user) {
                    if (err) {
                        var response = {
                            status: false,
                            message: appmsg.INTERNAL_ERR
                        }
                        log.error(filename + ">>bulkorderupdate>>" +
                            response.message);
                        ctx.res.status(500);
                        ctx.res.send(response);
                    } else {
                        len--;
                        if (len == 0) {
                            var response = {
                                status: true,
                                message: appmsg.ORDER_UPDATE_SUCCESS
                            }
                            log.info(filename + ">>bulkorderupdate>>" +
                                response.message);
                            ctx.res.status(200);
                            ctx.res.send(response);
                        }
                    }
                });
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            ctx.res.send(response);
        }
    };
    TblOrderDtl.remoteMethod('bulkorderupdate', {
        description: 'Bulk update order details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    // Request Alteration before Find data
    function modifyFindRequest(ctx, model, next) {
        try {
            log.info(filename + ">>modifyFindRequest>>");
            log.info(ctx.req.query);
            if (ctx.req.query.filter.where != undefined &&
                ctx.req.query.filter.where != "undefined") {
                if (ctx.req.query.filter.where.ordid != undefined &&
                    ctx.req.query.filter.where.ordid != "undefined") {
                    ctx.req.query.filter.include = [{
                        relation: "product",
                        scope: {
                            include: [{
                                relation: "u",
                                scope: { where: { status: 'Active' } }
                            }]
                        }
                    }]
                    ctx.req.query.filter.order = ['ordslno ASC', 'isfree DESC'];
                } else if (ctx.req.query.filter.where.duserid != undefined &&
                    ctx.req.query.filter.where.duserid != "undefined") {
                    ctx.req.query.filter.order = ['ordslno ASC', 'orddtlid ASC', 'isfree DESC'];
                    ctx.req.query.filter.include = [{
                            relation: "product",
                            scope: {
                                fields: ["userid", "prodcode", "prodname",
                                    "prodstatus"
                                ],
                                //include: [{relation: "u",scope: {where:{status:'Active'}}}]
                            }
                        },
                        {
                            relation: "orders",
                            scope: {
                                include: [{
                                    relation: "muser",
                                    scope: {
                                        fields: ["fullname", "companyname",
                                            "address", "mobileno"
                                        ]
                                    }
                                }]
                            }
                        }
                    ];
                }
            }
            next();
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            ctx.res.send(response);
        }

    }
    /**
     * To upload order images 
     * @method upload
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrderDtl.upload = function(ctx, options, data, cb) {
        if (!options)
            options = {};
        try {
            ctx.req.params.container = 'orders';
            var orddtlObj = {
                ordid: ctx.req.query.ordid,
                orddtlid: ctx.req.query.orddtlid
            }
            TblOrderDtl.app.models.files.upload(ctx.req, ctx.result, options,
                function(err, fileObj) {
                    if (err) {
                        cb(err);
                        log.error(filename + ">>upload>>");
                    } else {
                        var fileArr = [];
                        fileArr = fileObj.files;
                        var firstProp;
                        var nameArr = [];
                        for (var key in fileArr) {
                            if (fileArr.hasOwnProperty(key)) {
                                var i = 0;
                                firstProp = fileArr[key];
                                nameArr.push(firstProp[0]);
                                i++;
                            }
                        }
                        for (var i = 0; i < nameArr.length; i++) {
                            var fileInfo = nameArr[i].name;
                            if (orddtlObj.ordimage != undefined ||
                                orddtlObj.ordimage != null) {
                                orddtlObj.ordimage = orddtlObj.ordimage +
                                    ',' + fileInfo;
                            } else {
                                orddtlObj.ordimage = fileInfo;
                            }
                        }

                        TblOrderDtl.upsert(orddtlObj, function(err, obj) {
                            if (err !== null) {
                                var response = {
                                    status: false,
                                    message: appmsg.UPLOAD_FAILED
                                }
                                log.error(filename + ">>upload>>" +
                                    response.message);
                                cb(null, response.message);
                            } else {
                                var response = {
                                    data: obj.id,
                                    message: appmsg.UPLOAD_SUCCESS,
                                    status: true
                                }
                                log.info(filename + ">>upload>>" +
                                    response.message);
                                cb(null, response.message);
                            }
                        });
                    }
                });
        } catch (e) {
            log.info(e);
        }

    };
    TblOrderDtl.remoteMethod('upload', {
        description: 'Uploads a order image',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'options',
            type: 'string',
            http: {
                source: 'query'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'fileObject',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    // Request Alteration before File upload
    function modifyFileObject(ctx, model, next) {
        try {
            app.dataSources.filestorage.connector.getFilename = function(file, req,
                res) {
                var origFilename = file.name;
                var parts = origFilename.split('.'),
                    extension = parts[parts.length - 1];
                var newFilename = common.generateOTP(4) + '-' +
                    common.generateOTP(2) + '.' + extension;
                return newFilename;
            };
            log.info(filename + ">>modifyFileObject>>");
            try {
                var data = (ctx.req.query.ordid);
                var query = "SELECT * FROM tbl_order_dtl where ordid=?";
                var qryArr = [data];
                var ds = TblOrderDtl.dataSource;
                ds.connector.execute(query, qryArr, function(err, orders) {
                    if (err) {
                        console.error(err);
                        response.status = true;
                        response.message = appmsg.INTERNAL_ERR;
                        ctx.res.send(response);
                    } else {
                        if (orders.length != 0) {
                            ctx.req.query.orddtlid = orders[0].orddtlid;
                            next();
                        } else {
                            var response = {
                                status: false,
                                message: appmsg.UPLOAD_FAILED
                            }
                            ctx.res.send(response.message);
                        }
                    }
                });
            } catch (ex) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(ex);
                ctx.res.send(response);
            }
        } catch (ex) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(ex);
            ctx.res.send(response);
        }
    }

    /**
     * To Bulk Update Order discount details
     * @method updateorders
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrderDtl.updateorders = function(ctx, cb) {
        try {
            var orderDtls = ctx.args.data.orderdtls;
            log.info(orderDtls);
            var len = orderDtls.length;
            for (var i = 0; i < orderDtls.length; i++) {
                orderDtls[i].updateddt = new Date();
                TblOrderDtl.upsert(orderDtls[i], function(err, order) {
                    if (err) {
                        var response = {
                            status: false,
                            message: appmsg.INTERNAL_ERR
                        }
                        log.error(err);
                        ctx.res.status(500);
                        ctx.res.send(response);
                    } else {
                        len--;
                        if (len == 0) {
                            var response = {
                                status: true,
                                message: appmsg.ORDER_UPDATE_SUCCESS
                            }
                            log.info(order);
                            ctx.res.status(200);
                            ctx.res.send(response);
                        }
                    }
                });
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            ctx.res.send(response);
        }
    };
    TblOrderDtl.remoteMethod('updateorders', {
        description: 'Bulk update of order discount details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    /**
     * To get Report I data
     * @method reports
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrderDtl.report = function(ctx, cb) {
        try {
            var response = {};
            var ds = TblOrderDtl.dataSource;
            log.info(ctx.args.data)
            if (ctx.args.data.usertype) {
                if (ctx.args.data.usertype === 'D') {
                    //Report 4 
                    if (ctx.args.data.reporttype === 'F') {
                        var query = 'select p.prodcode,p.prodtax,h.aprxordval,p.unitperuom,dt.ptax,dt.qty,dt.cqty,p.unitprice as prodprice,dt.ordid,dt.prodid,h.orderdt,dt.duserid,p.prodname,u.companyname as mcompanyname,u.tin as mtin,u.fullname as mname,h.muserid,SUM(dt.ptax) as ptaxtotal,SUM(dt.cqty) as kcqty FROM tbl_order_dtl as dt left join  tbl_user_prod as p on p.prodid=dt.prodid left join tbl_orders as h on h.ordid=dt.ordid left join tbl_user as u on u.userid=h.muserid where dt.duserid=? AND dt.ordstatus != "Cancelled" AND dt.ordstatus != "No Order" AND dt.ordstatus != "Outlet" AND dt.isfree = "NF" and substring(h.orderdt, 1, 10) between ? and ? group by h.muserid,dt.prodid;';
                        var qryStg = [ctx.args.data.duserid, ctx.args.data.fromdt, ctx.args.data.todt];
                    }
                    // Stock Report
                    else if (ctx.args.data.reporttype === 'STOCK') {
                        var query = "select s.*,p.prodcode,p.prodname,u.fullname as sname,m.companyname as partyname FROM tbl_stock_entry as s left join tbl_user_prod as p on p.prodid=s.prodid join tbl_user as u on u.userid=s.suserid  join tbl_user as m on m.userid=s.muserid where s.userid=0 and s.muserid in (?) and s.prodid in (?) and substring(s.updateddt, 1, 10) between ? and ? ;";
                        var qryStg = [ctx.args.data.muserid, ctx.args.data.prodid, ctx.args.data.fromdt, ctx.args.data.todt];
                    } else {
                        var query = 'SELECT tbl_user_prod.*,tbl_order_dtl.*,tbl_orders.*,sm.fullname as sname,m.companyname as mcname FROM tbl_orders join tbl_user as sm on sm.userid=tbl_orders.suserid join tbl_user as m on m.userid=tbl_orders.muserid join tbl_order_dtl on tbl_order_dtl.ordid=tbl_orders.ordid join tbl_user_prod on tbl_user_prod.prodid=tbl_order_dtl.prodid where tbl_orders.muserid in (?) and tbl_orders.suserid in (?) and tbl_order_dtl.isfree = "NF" and tbl_order_dtl.ordstatus != "Cancelled" AND tbl_order_dtl.ordstatus != "No Order" AND tbl_order_dtl.ordstatus != "Outlet"  and  tbl_order_dtl.prodid in (?) and substring(tbl_orders.orderdt,1,10) BETWEEN ? AND ?'
                        var qryStg = [ctx.args.data.muserid, ctx.args.data.suserid, ctx.args.data.prodid, ctx.args.data.fromdt, ctx.args.data.todt];
                    }
                }
                //My sales report
                else if (ctx.args.data.usertype === 'S' && ctx.args.data.reporttype === 'SALES') {
                    var query = "select p.prodcode,p.prodname,sum(round(d.cqty,2)) as qtotal ,p.uom FROM tbl_orders as h left join tbl_order_dtl as d on d.ordid=h.ordid join tbl_user_prod as p on p.prodid=d.prodid where h.suserid=? and substring(h.orderdt, 1, 10) between ? and ? and d.ordstatus != 'Cancelled' AND d.ordstatus != 'No Order' AND d.ordstatus != 'Outlet' and d.isfree='NF' group by p.prodid;";
                    var qryStg = [ctx.args.data.suserid, ctx.args.data.fromdt, ctx.args.data.todt];
                }
                //Stock entry report
                else if (ctx.args.data.usertype === 'S' && ctx.args.data.reporttype === 'STOCK') {
                    var query = "select s.*,p.prodcode,p.prodname FROM tbl_stock_entry as s left join tbl_user_prod as p on p.prodid=s.prodid where s.userid=0 and s.suserid=? and s.muserid in (?) and s.prodid in (?) and substring(s.updateddt, 1, 10) between ? and ?;";
                    var qryStg = [ctx.args.data.suserid, ctx.args.data.muserid, ctx.args.data.prodid, ctx.args.data.fromdt, ctx.args.data.todt];
                }
                ds.connector.execute(query, qryStg, function(err, orders) {
                    if (err) {
                        log.error(err);
                        response.status = false;
                        response.message = appmsg.INTERNAL_ERR;
                        ctx.res.send(response);
                    } else {
                        if (orders.length != 0) {
                            response.status = true;
                            response.message = appmsg.LIST_FOUND;
                            response.data = orders;
                        } else {
                            response.status = false;
                            response.message = appmsg.DORDER_NT_FOUND;
                        }
                        ctx.res.send(response);
                    }
                });
            } else {
                response.status = false;
                response.message = appmsg.LIST_NTFOUND;
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblOrderDtl.remoteMethod('report', {
        description: 'Report I',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    /**
     * To get Report I data
     * @method eodreport
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrderDtl.eodreport = function(ctx, cb) {
        try {
            var response = {};
            var ds = TblOrderDtl.dataSource;
            log.info(ctx.args.data)
            validation.isMandatoryString(ctx.args.data.duserid, 'Dealer id', ctx.res, 1, 15);
            validation.isMandatoryString(ctx.args.data.suserid, 'Salesman id', ctx.res, 1, 15);
            validation.isMandatoryDate(ctx.args.data.fromdt, 'From date', 'YYYY-MM-DD', ctx.res);
            validation.isMandatoryDate(ctx.args.data.todt, 'To date', 'YYYY-MM-DD', ctx.res);
            //My sales report
            var query = "select p.prodcode,p.prodname,sum(round(d.cqty,2)) as qtotal ,p.uom FROM tbl_orders as h left join tbl_order_dtl as d on d.ordid=h.ordid join tbl_user_prod as p on p.prodid=d.prodid where h.suserid=? and substring(h.orderdt, 1, 10) between ? and ? and d.ordstatus != 'Cancelled' AND d.ordstatus != 'No Order' AND d.ordstatus != 'Outlet' and d.isfree='NF' group by p.prodid;";
            var qryStg = [ctx.args.data.suserid, ctx.args.data.fromdt, ctx.args.data.todt];
            ds.connector.execute(query, qryStg, function(err, orders) {
                if (err) {
                    log.error(err);
                    response.status = false;
                    response.message = appmsg.INTERNAL_ERR;
                    ctx.res.send(response);
                } else {
                    if (orders.length != 0) {
                        response.status = true;
                        response.message = appmsg.LIST_FOUND;
                        response.data = orders;
                        var ordquery = "SELECT (SELECT count(ordid) FROM tbl_orders where suserid=?  AND SUBSTRING(orderdt, 1, 10) BETWEEN ? AND ?) AS totalcount,(SELECT count(distinct h.ordid) FROM tbl_orders h,tbl_order_dtl d where h.ordid=d.ordid and h.suserid=?  AND SUBSTRING(h.orderdt, 1, 10) BETWEEN ? AND ? AND d.ordstatus NOT IN ('Cancelled' , 'No Order', 'Outlet')) AS pccount;";
                        var ordqryStg = [ctx.args.data.suserid, ctx.args.data.fromdt, ctx.args.data.todt, ctx.args.data.suserid, ctx.args.data.fromdt, ctx.args.data.todt];
                        ds.connector.execute(ordquery, ordqryStg, function(e, ordercount) {
                            // var mquery = "select count(*) as count from tbl_user where usertype='M' and userstatus='Active' and userid in(select muserid from tbl_user_dealer where dstatus='Active' and duserid=?)";
                            // var mqryStg = [ctx.args.data.duserid];
                            console.log(ordercount);
                            response.totalcount = ordercount[0].totalcount;
                            response.pccount = ordercount[0].pccount;
                            ctx.res.send(response);
                            // ds.connector.execute(mquery, mqryStg, function(er, mcount) {
                            //     console.log(mcount);
                            //     response.merchantcount = mcount[0].count;
                            //     ctx.res.send(response);
                            // });
                        });
                    } else {
                        response.status = false;
                        response.totalcount = 0;
                        response.pccount = 0;
                        response.message = appmsg.DORDER_NT_FOUND;
                        ctx.res.send(response);
                    }
                }
            });
        } catch (e) {
            validation.generateAppError(e, response, ctx.res);
        }
    };
    TblOrderDtl.remoteMethod('eodreport', {
        description: 'End of the report for whatsapp share details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    TblOrderDtl.afterRemote('create', modifySaveResponse);
    TblOrderDtl.afterRemote('upsert', modifyUpsertResponse);
    TblOrderDtl.afterRemote('find', modifyFindResponse);
    TblOrderDtl.beforeRemote('find', modifyFindRequest);
    TblOrderDtl.afterRemote('bulkorderupdate', modifySaveResponse);
    TblOrderDtl.afterRemote('updateAll', modifyUpsertResponse);
    TblOrderDtl.beforeRemote('upload', modifyFileObject);
};