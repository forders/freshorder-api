/**
 * @Filename : tbl-orders.js
 * @Description : To write hooks for the order header details related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var app = require('../../server/server');
var path = require('path');
var _ = require('underscore');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var constants = require('../config/constants.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
var schedule = require('node-schedule');
var json2csv = require('json2csv');
var fs = require('fs');
var async = require("async");
module.exports = function (TblOrders) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                data: {
                    ordid: model.ordid,
                    muserid: model.muserid
                },
                message: appmsg.ORDER_SAVE_SUCCESS
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            // console.log(ctx.req.body.orderdtls);
            try {
                if (ctx.req.body.orderdtls.length != 0) {
                    for (var i = 0; i < ctx.req.body.orderdtls.length; i++) {
                        console.log(orderdetails);
                        var orderdetails = ctx.req.body.orderdtls[i];
                        var QtyOrder = {
                            ordid: model.ordid,
                            duserid: orderdetails.duserid,
                            updateddt: new Date(),
                        }
                        if (orderdetails.prodid != null &&
                            orderdetails.prodid != undefined) {
                            QtyOrder.prodid = orderdetails.prodid;
                        }
                        if (orderdetails.qty != null &&
                            orderdetails.qty != undefined) {
                            QtyOrder.qty = orderdetails.qty;
                        }
                        if (orderdetails.isfree != null &&
                            orderdetails.isfree != undefined) {
                            QtyOrder.isfree = orderdetails.isfree;
                        }
                        if (orderdetails.ordstatus != null &&
                            orderdetails.ordstatus != undefined) {
                            QtyOrder.ordstatus = orderdetails.ordstatus;
                        }
                        if (orderdetails.deliverydt != null &&
                            orderdetails.deliverydt != undefined) {
                            QtyOrder.deliverydt = orderdetails.deliverydt;
                        }
                        if (orderdetails.deliverytype != null &&
                            orderdetails.deliverytype != undefined) {
                            QtyOrder.deliverytype = orderdetails.deliverytype;
                        }
                        if (orderdetails.deliverytime != null &&
                            orderdetails.deliverytime != undefined) {
                            QtyOrder.deliverytime = orderdetails.deliverytime;
                        }
                        if (orderdetails.updatedby != null &&
                            orderdetails.updatedby != undefined) {
                            QtyOrder.updatedby = orderdetails.updatedby;
                        }
                        if (orderdetails.fqty != null &&
                            orderdetails.fqty != undefined) {
                            QtyOrder.fqty = orderdetails.fqty;
                        }
                        if (orderdetails.ordslno != null &&
                            orderdetails.ordslno != undefined) {
                            QtyOrder.ordslno = orderdetails.ordslno;
                        }
                        if (orderdetails.prate != null &&
                            orderdetails.prate != undefined) {
                            QtyOrder.prate = orderdetails.prate;
                        }
                        if (orderdetails.ptax != null &&
                            orderdetails.ptax != undefined) {
                            QtyOrder.ptax = orderdetails.ptax;
                        }
                        if (orderdetails.pvalue != null &&
                            orderdetails.pvalue != undefined) {
                            QtyOrder.pvalue = orderdetails.pvalue;
                        }
                        if (orderdetails.isread != null &&
                            orderdetails.isread != undefined) {
                            QtyOrder.isread = orderdetails.isread;
                        }
                        if (orderdetails.batchno != null && orderdetails.batchno != 'null' &&
                            orderdetails.batchno != undefined && orderdetails.batchno != '') {
                            QtyOrder.batchno = orderdetails.batchno;
                        }
                        if (orderdetails.manufdt != null && orderdetails.manufdt != 'null' &&
                            orderdetails.manufdt != undefined && orderdetails.manufdt != '') {
                            QtyOrder.manufdt = orderdetails.manufdt;
                        }
                        if (orderdetails.expirydt != null && orderdetails.expirydt != 'null' &&
                            orderdetails.expirydt != undefined && orderdetails.expirydt != '') {
                            QtyOrder.expirydt = orderdetails.expirydt;
                        }
                        if (orderdetails.stock != null &&
                            orderdetails.stock != undefined) {
                            QtyOrder.stock = orderdetails.stock;
                        }
                        if (orderdetails.ord_uom != null &&
                            orderdetails.ord_uom != undefined) {
                            QtyOrder.ord_uom = orderdetails.ord_uom;
                        }
                        if (orderdetails.default_uom != null &&
                            orderdetails.default_uom != undefined) {
                            QtyOrder.default_uom = orderdetails.default_uom;
                        }
                        if (orderdetails.cqty != null &&
                            orderdetails.cqty != undefined) {
                            QtyOrder.cqty = orderdetails.cqty;
                        }
                        if (QtyOrder.qty > 0) {
                            console.log(QtyOrder);
                            insertIntoOrderDtls(QtyOrder);
                        }
                        if (orderdetails.image == 'Y') {
                            insertIntoOrderDtls(QtyOrder);
                        }
                        if (QtyOrder.fqty > 0) {
                            var fQtyOrder = QtyOrder;
                            fQtyOrder.qty = QtyOrder.fqty;
                            fQtyOrder.isfree = 'F';
                            insertIntoOrderDtls(fQtyOrder);
                        }
                        log.info(filename + ">>orderdtls>>ADDED");
                    }
                }
            } catch (ex) {
                console.log(ex);
            }
            ctx.res.status(status);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.status(status);
            complaintsave();
        } else {
            var response = {
                status: false,
                message: appmsg.ORDER_SAVE_FAILED
            }
            log.error(filename + ">>modifySaveResponse>>" + response.message);
            complaintsave();
        }

        function complaintsave() {
            try {
                log.info(filename + ">>complaintsave>>");
                var complaint = {
                    // suserid:ctx.req.body.suserid,
                    particular: ctx.req.body.particular,
                    cmplnttype: 'S',
                    status: appmsg.STATUS_PENDING
                }
                if (ctx.req.body.suserid != null &&
                    ctx.req.body.suserid != undefined) {
                    complaint.muserid = ctx.req.body.suserid;
                }
                if (ctx.req.body.muserid != null &&
                    ctx.req.body.muserid != undefined) {
                    complaint.muserid = ctx.req.body.muserid;
                }
                app.models.TblComplaints.create(complaint,
                    function (err, result) {
                        log.info(result);
                    });
            } catch (e) {
                log.error(filename + ">>complaintsave>>");
                console.log(e);
            }
        }
        ctx.res.status(status);
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.send(response);
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                data: model.ordid,
                message: appmsg.ORDER_UPDATE_SUCCESS
            }
            log.info(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
        } else {
            var response = {
                status: false,
                message: appmsg.ORDER_UPDATE_FAILED
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.send(response);
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            if (model.length === 0) {
                var response = {
                    status: false,
                    message: appmsg.ORDER_LIST_NTFOUND
                }
            } else {
                var response = {
                    status: true,
                    data: model,
                    message: appmsg.LIST_FOUND
                }
            }
            log.info(filename + ">>modifyFindResponse>>" + response.message);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyFindResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Request Alteration before Find data
    function modifyFindRequest(ctx, model, next) {
        log.info(filename + ">>modifyFindRequest>>");
        if (ctx.req.query.mode != 'WEB') {
            if (_.has(ctx.req.query.filter.where, 'muserid') == true && ctx.req.query.filter.where.muserid != undefined && ctx.req.query.filter.where.muserid != null) {
                ctx.req.query.filter.include = [{
                    relation: "orderdetail",
                    scope: {
                        include: [{
                            relation: "duser",
                            scope: {
                                fields: ["fullname", "companyname",
                                    "address", "mobilno"
                                ]
                            }
                        },
                        {
                            relation: "product",
                            scope: {
                                fields: ["userid", "prodcode",
                                    "prodname", "prodstatus"
                                ]
                            }
                        }
                        ]
                    }
                }];
                next();
            } else {
                next();
            }
        } else {
            next();
        }
    }
    /**
     * To get the order list based on the usertype
     * @method orderlist
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrders.orderlist = function (ctx, cb) {
        try {
            var response = {};
            var ds = TblOrders.dataSource;
            var dt = new Date();
            var dd = dt.getDate();
            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);
            var mm = dt.getMonth() + 1;
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            yd = yesterday.getDate();
            if (yesterday.getDate() < 10) {
                yd = '0' + yesterday.getDate();
            }
            ym = yesterday.getMonth() + 1;
            if (ym < 10) {
                ym = '0' + ym;
            }
            var yr = dt.getFullYear();
            var cdt = yr + '-' + mm + '-' + dd;
            var ydt = yesterday.getFullYear() + '-' + ym + '-' + yd;
            var qryArr = [ctx.args.data.muserid];
            if (ctx.args.data.usertype === 'S' && ctx.args.data.orders != 'M') {
                var qryArr = [ctx.args.data.muserid, cdt];
                var query = 'SELECT DISTINCT tbl_orders.ordid,tbl_orders.muserid,tbl_order_dtl.isread,tbl_orders.orderdt,tbl_order_dtl.duserid,duser.companyname as dcompany,duser.address as daddress,duser.fullname as dfname,duser.mobileno as dmobileno,muser.companyname as mcompany,muser.address as maddress,muser.fullname as mfname,muser.mobileno as mmobileno FROM tbl_order_dtl join tbl_orders on tbl_orders.ordid=tbl_order_dtl.ordid join tbl_user as duser on duser.userid=tbl_order_dtl.duserid join tbl_user as muser on muser.userid=tbl_orders.muserid where tbl_orders.suserid=? AND tbl_order_dtl.ordstatus != "Cancelled" AND tbl_order_dtl.ordstatus != "No Order" AND SUBSTRING(tbl_orders.orderdt,1,10)=? order by tbl_orders.ordid DESC';
            } else if (ctx.args.data.usertype === 'M') {
                var qryArr = [ctx.args.data.muserid];
                var query = 'SELECT DISTINCT tbl_orders.ordid,tbl_orders.orderdt,tbl_order_dtl.isread,tbl_order_dtl.duserid,tbl_user.companyname,tbl_user.address,tbl_user.fullname,tbl_user.mobileno FROM tbl_order_dtl join tbl_orders on tbl_orders.ordid=tbl_order_dtl.ordid join tbl_user on tbl_user.userid=tbl_order_dtl.duserid where tbl_orders.muserid=? AND tbl_order_dtl.ordstatus != "Cancelled" AND tbl_order_dtl.ordstatus != "No Order" order by tbl_orders.ordid DESC';
            } else if (ctx.args.data.usertype === 'S' && ctx.args.data.orders === 'M') {
                var qryArr = [ctx.args.data.muserid, ctx.args.data.duserid];
                var query = 'SELECT DISTINCT tbl_orders.ordid,tbl_orders.orderdt,tbl_order_dtl.duserid,tbl_orders.muserid,tbl_user.companyname,tbl_user.fullname FROM tbl_orders JOIN tbl_order_dtl ON tbl_order_dtl.ordid = tbl_orders.ordid JOIN tbl_user ON tbl_user.userid = tbl_orders.muserid WHERE tbl_orders.muserid = ? AND tbl_order_dtl.duserid = ? AND tbl_order_dtl.ordstatus != "Cancelled" AND tbl_order_dtl.ordstatus != "No Order" ORDER BY tbl_orders.ordid DESC';
            } else if (ctx.args.data.usertype === 'D' && ctx.args.data.map == 'Y') {
                var query = 'SELECT DISTINCT tbl_orders.suserid,tbl_order_dtl.ordid,tbl_orders.orderdt,tbl_orders.start_time,tbl_orders.end_time,tbl_orders.geolat,tbl_orders.geolong,tbl_order_dtl.duserid,tbl_user.companyname as mcompanyname,tbl_user.address as maddress,tbl_user.fullname as mname,tbl_user.mobileno as mmobileno,tbl_orders.muserid FROM tbl_order_dtl join tbl_orders on tbl_orders.ordid=tbl_order_dtl.ordid join tbl_user on tbl_user.userid=tbl_orders.muserid where tbl_order_dtl.duserid=? AND tbl_order_dtl.ordstatus != "Outlet" AND tbl_orders.suserid in (?) AND SUBSTRING(tbl_orders.orderdt, 1, 10) =? order by tbl_orders.ordid ASC';
                var qryArr = [ctx.args.data.duserid, ctx.args.data.suserid, ctx.args.data.reportdt];
            } else if (ctx.args.data.usertype === 'D' && ctx.args.data.map != 'Y') {
                var query = 'SELECT DISTINCT tbl_order_dtl.deliverytype,sman.userid,sman.fullname as sname,tbl_order_dtl.ordid,tbl_orders.orderdt,tbl_orders.start_time,tbl_orders.end_time,tbl_order_dtl.isread,tbl_order_dtl.duserid,tbl_user.companyname as mcompanyname,tbl_user.address as maddress,tbl_user.fullname as mname,tbl_user.mobileno as mmobileno,tbl_orders.muserid,tbl_orders.suserid,tbl_order_dtl.ordstatus FROM tbl_order_dtl join tbl_orders on tbl_orders.ordid=tbl_order_dtl.ordid join tbl_user as sman on tbl_orders.suserid=sman.userid join tbl_user on tbl_user.userid=tbl_orders.muserid where tbl_order_dtl.duserid=? AND tbl_order_dtl.ordstatus != "Outlet" AND tbl_order_dtl.ordstatus != "Cancelled" AND tbl_order_dtl.ordstatus != "No Order" AND SUBSTRING(tbl_orders.orderdt, 1, 10) BETWEEN ? AND ? order by tbl_orders.ordid DESC';
                ctx.args.data.muserid = ctx.args.data.duserid;
                if (ctx.args.data.mode === 'WEB') {
                    var qryArr = [ctx.args.data.duserid, ctx.args.data.fromdt, ctx.args.data.todt];
                } else {
                    var qryArr = [ctx.args.data.duserid, ydt, cdt];
                }
            }
            ds.connector.execute(query, qryArr, function (err, orders) {
                if (err) {
                    console.error(err);
                    response.status = true;
                    response.message = appmsg.INTERNAL_ERR;
                    ctx.res.send(response);
                } else {
                    if (orders.length != 0) {
                        response.status = true;
                        response.message = appmsg.LIST_FOUND;
                        if (ctx.args.data.usertype === 'D') {
                            //response.data = orders;
                            result = [];
                            for (var i = 0; i < orders.length; i++) {
                                var exist = _.findWhere(result, { ordid: orders[i].ordid });
                                if (exist != undefined) {
                                    //exist.deliverytype != orders[i].deliverytype
                                    if (exist.deliverytype != orders[i].deliverytype) {
                                        if (orders[i].deliverytype == 'Scheduled') {
                                            var index = _.indexOf(result, exist);
                                            result[index] = orders[i];
                                        } else {
                                            var index = _.indexOf(result, exist);
                                            result[index] = exist;
                                        }
                                    } else {
                                        var index = _.indexOf(result, exist);
                                        result[index] = orders[i];
                                    }
                                } else {
                                    result.push(orders[i])
                                }
                                if (i == (orders.length - 1)) {
                                    response.data = result;
                                    ctx.res.send(response);
                                }
                            }
                        } else {
                            response.data = orders;
                            ctx.res.send(response);
                        }

                    } else {
                        response.status = false;
                        if (ctx.args.data.usertype === 'D') {
                            response.message = appmsg.DORDER_NT_FOUND;
                        } else {
                            response.message = appmsg.ORDER_LIST_NTFOUND;
                        }
                        ctx.res.send(response);
                    }
                }
            });
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblOrders.remoteMethod('orderlist', {
        description: 'Get orderdetails for requested user id',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    /**
     * To generate csv from the order details based on the usertype
     * @method exportcsv
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrders.exportcsv = function (ctx, cb) {
        var response = {};
        var ds = TblOrders.dataSource;
        log.info(ctx.args.data);
        if (ctx.args.data.usertype === 'S') {
            var query = 'SELECT DISTINCT tbl_orders.ordid,tbl_orders.muserid,tbl_orders.orderdt,tbl_order_dtl.qty,duserid,duser.companyname as dcompany,duser.address as daddress,duser.fullname as dfname,duser.mobileno as dmobileno,muser.companyname as mcompany,muser.address as maddress,muser.fullname as mfname,muser.mobileno as mmobileno,tbl_order_dtl.ordstatus FROM tbl_order_dtl join tbl_orders on tbl_orders.ordid=tbl_order_dtl.ordid join tbl_user as duser on duser.userid=tbl_order_dtl.duserid join tbl_user as muser on muser.userid=tbl_orders.muserid where tbl_orders.suserid=? and substring(tbl_orders.orderdt,1,10) between ? and ? order by tbl_orders.ordid DESC ';
        } else if (ctx.args.data.usertype === 'M') {
            var query = 'SELECT DISTINCT tbl_orders.ordid,tbl_orders.orderdt,duserid,tbl_order_dtl.qty,tbl_user.companyname,tbl_user.address,tbl_user.fullname,tbl_user.mobileno,tbl_order_dtl.ordstatus FROM tbl_order_dtl join tbl_orders on tbl_orders.ordid=tbl_order_dtl.ordid join tbl_user on tbl_user.userid=tbl_order_dtl.duserid where tbl_orders.muserid=? and substring(tbl_orders.orderdt,1,10) between ? and ? order by tbl_orders.ordid DESC';
        } else if (ctx.args.data.usertype === 'D' &&
            ctx.args.data.suseridlist == undefined &&
            ctx.args.data.suseridlist == null) {
            var query = 'SELECT * FROM tbl_order_dtl JOIN tbl_orders ON tbl_orders.ordid = tbl_order_dtl.ordid JOIN tbl_user ON tbl_user.userid = tbl_orders.muserid JOIN tbl_user_prod ON tbl_user_prod.prodid = tbl_order_dtl.prodid WHERE tbl_order_dtl.duserid = ? AND tbl_order_dtl.ordstatus != "Cancelled" AND SUBSTRING(tbl_orders.orderdt, 1, 10) BETWEEN ? AND ? ORDER BY ifnull(tbl_orders.suserid,tbl_orders.muserid)ASC, tbl_order_dtl.ordid ASC,tbl_order_dtl.ordslno ASC ,tbl_order_dtl.isfree DESC;'
            var qArr = [ctx.args.data.userid, ctx.args.data.fromdt,
            ctx.args.data.todt
            ];
        } else if (ctx.args.data.usertype === 'D' &&
            ctx.args.data.suseridlist != undefined &&
            ctx.args.data.suseridlist != null) {
            var query = 'SELECT * FROM tbl_order_dtl JOIN tbl_orders ON tbl_orders.ordid = tbl_order_dtl.ordid JOIN tbl_user ON tbl_user.userid = tbl_orders.muserid JOIN tbl_user_prod ON tbl_user_prod.prodid = tbl_order_dtl.prodid WHERE tbl_orders.suserid in (?) AND tbl_order_dtl.duserid = ? AND tbl_order_dtl.ordstatus != "Cancelled" AND  SUBSTRING(tbl_orders.orderdt, 1, 10) BETWEEN ? AND ?  ORDER BY ifnull(tbl_orders.suserid,tbl_orders.muserid)ASC, tbl_order_dtl.ordid ASC,tbl_order_dtl.ordslno ASC ,tbl_order_dtl.isfree DESC;'
            var qStr = ctx.args.data.suseridlist;
            var concatlist = qStr;
            console.log(concatlist);
            var qArr = [concatlist, ctx.args.data.userid, ctx.args.data.fromdt,
                ctx.args.data.todt
            ];
        }
        ds.connector.execute(query, qArr, function (err, orders) {
            if (err) {
                console.error(err);
                response.status = false;
                response.message = appmsg.INTERNAL_ERR;
                ctx.res.send(response);
            } else {
                if (orders.length != 0) {
                    var query = 'SELECT * FROM tbl_app_setting where userid=? and refkey in ("EXPORT","CUSFIELD1","CUSFIELD2","INVNO");';
                    var qStr = [ctx.args.data.userid];
                    ds.connector.execute(query, qStr, function (e, refvalue) {
                        if (e) {
                            console.error(e);
                            response.status = false;
                            response.message = appmsg.INTERNAL_ERR;
                            ctx.res.send(response);
                        } else {
                            response.status = true;
                            response.message = appmsg.LIST_FOUND;
                            var list = [];
                            var qry = [];
                            var initno = ctx.args.data.invoiceno;
                            var newno = Number(initno);
                            for (var i = 0; i < orders.length; i++) {
                                if (refvalue.length != 0) {
                                    var expo = _.findWhere(refvalue, { refkey: 'EXPORT' });
                                    var cus_fld1 = _.findWhere(refvalue, { refkey: 'CUSFIELD1' });
                                    if (cus_fld1 != undefined) {
                                        var obj1 = JSON.parse(cus_fld1.refvalue);
                                        orders[i].custfield1 = obj1.value;
                                        var value = _.values(JSON.parse(cus_fld1.refvalue));
                                        qry[parseInt(obj1.index) - 1] = 'custfield1';
                                    }
                                    var cus_fld2 = _.findWhere(refvalue, { refkey: 'CUSFIELD2' });
                                    if (cus_fld2 != undefined) {
                                        var obj2 = JSON.parse(cus_fld2.refvalue);
                                        orders[i].custfield2 = obj2.value;
                                        var value = _.values(JSON.parse(cus_fld2.refvalue));
                                        qry[parseInt(obj2.index) - 1] = 'custfield2';
                                    }
                                    var invno = _.findWhere(refvalue, { refkey: 'INVNO' });
                                    if (invno != undefined) {
                                        qry[parseInt(invno.refvalue) - 1] = 'invoiceno';
                                        /*	orders[i].invoiceno=newno;
                                        	newno=newno+1;*/
                                    }
                                    if (expo != undefined) {
                                        var e_ref_value = new Array(expo.refvalue);
                                        var ln = e_ref_value[0].split(',').length;
                                        _.map(e_ref_value[0].split(','), function (item, key) {
                                            var key = _.allKeys(JSON.parse(item));
                                            var value = _.values(JSON.parse(item));
                                            var newkey = key[0];
                                            var newvalue = value[0];
                                            qry[newvalue - 1] = newkey;
                                            if (newkey == 'fqty') {
                                                if (orders[i].isfree == 'F') {
                                                    orders[i].fqty = orders[i].cqty;
                                                    orders[i].qty = 0;
                                                } else if (orders[i].isfree == 'NF') {
                                                    orders[i].fqty = 0;
                                                    orders[i].qty = orders[i].cqty;
                                                }
                                            } else {
                                                orders[i].qty = parseFloat(orders[i].cqty).toFixed(2);
                                            }
                                            ln--;
                                            if (ln == 0) {
                                                ctx.req.fields = qry;
                                                var result = _.pick(orders[i], qry);
                                                if (ctx.args.data.invoice == 'Y') {
                                                    if (_.findWhere(list, { partycd: orders[i].partycd }) != undefined) {
                                                        orders[i].invoiceno = _.findWhere(list, { partycd: orders[i].partycd }).invoiceno;
                                                    }
                                                    if (_.findWhere(list, { companyname: orders[i].companyname }) != undefined) {
                                                        orders[i].invoiceno = _.findWhere(list, { companyname: orders[i].companyname }).invoiceno;
                                                    } else {
                                                        orders[i].invoiceno = newno;
                                                        newno = newno + 1;
                                                    }
                                                }
                                                var d_result = orders[i];
                                                if (_.has(d_result, 'orderdt') == true) {
                                                    var date = new Date(d_result.orderdt);
                                                    console.log(date);
                                                    var dd = date.getDate();
                                                    var mm = date.getMonth() + 1;
                                                    if (dd < 10) {
                                                        dd = '0' + dd;
                                                    }
                                                    if (mm < 10) {
                                                        mm = '0' + mm;
                                                    }
                                                    d_result.orderdt = dd + '/' +
                                                        mm + '/' +
                                                        date.getFullYear()
                                                }
                                                list.push(d_result);
                                            }
                                        });
                                    } else {
                                        ctx.req.fields = qry;
                                        var result = _.pick(orders[i], qry);
                                        if (ctx.args.data.invoice == 'Y') {
                                            if (_.findWhere(list, { partycd: orders[i].partycd }) != undefined) {
                                                orders[i].invoiceno = _.findWhere(list, { partycd: orders[i].partycd }).invoiceno;
                                            }
                                            if (_.findWhere(list, { companyname: orders[i].companyname }) != undefined) {
                                                orders[i].invoiceno = _.findWhere(list, { companyname: orders[i].companyname }).invoiceno;
                                            } else {
                                                orders[i].invoiceno = newno;
                                                newno = newno + 1;
                                            }
                                        }
                                        var d_result = orders[i];
                                        console.log(d_result);
                                        list.push(d_result);
                                    }
                                } else {
                                    //var fields = ['Party_Code', 'Product_Code', 'Product_Quantity', 'Free_Status', 'Payment_Mode'];
                                    var result = {
                                        partycd: orders[i].partycd,
                                        prodcode: orders[i].prodcode,
                                        qty: orders[i].cqty,
                                        isfree: orders[i].isfree,
                                        iscash: orders[i].iscash
                                    }
                                    list.push(result);
                                }
                            }
                            var myfilename = ctx.args.data.userid + '.csv';
                            if (list.length == orders.length) {
                                arf = [], ara = [], are = [], arm = [], art = [];
                                others = [];
                                for (var j = 0; j < list.length; j++) {
                                    fileArr = [];
                                    if (list[j].prodcode && ctx.req.fields) {
                                        if (list[j].prodcode.substr(0, 3) == 'ARF') {
                                            var obj = _.pick(list[j], ctx.req.fields);
                                            arf.push(obj);
                                        } else if (list[j].prodcode.substr(0, 3) == 'ARA') {
                                            var obj = _.pick(list[j], ctx.req.fields);
                                            ara.push(obj);
                                        } else if (list[j].prodcode.substr(0, 3) == 'ARE') {
                                            var obj = _.pick(list[j], ctx.req.fields);
                                            are.push(obj);
                                        } else if (list[j].prodcode.substr(0, 3) == 'ARM') {
                                            var obj = _.pick(list[j], ctx.req.fields);
                                            arm.push(obj);
                                        } else if (list[j].prodcode.substr(0, 3) == 'ART') {
                                            var obj = _.pick(list[j], ctx.req.fields);
                                            art.push(obj);
                                        } else {
                                            if (ctx.req.fields) {
                                                var obj = _.pick(list[j], ctx.req.fields);
                                            } else {
                                                var obj = list[j];
                                            }
                                            others.push(obj);
                                        }
                                    } else if (ctx.req.fields) {
                                        var obj = _.pick(list[j], ctx.req.fields);
                                        others.push(obj);
                                    } else {
                                        others.push(list[j]);
                                    }
                                    if (j == (list.length - 1)) {
                                        response.orders = arf.concat(ara);
                                        response.orders = response.orders.concat(are);
                                        response.orders = response.orders.concat(arm);
                                        response.orders = response.orders.concat(art);
                                        response.orders = response.orders.concat(others);
                                        if (arf.length != 0) {
                                            name = 'AFT' + myfilename;
                                            fileArr.push(name);
                                            writeData(arf, name)
                                        }
                                        if (ara.length != 0) {
                                            name = 'ARA' + myfilename;
                                            fileArr.push(name);
                                            writeData(ara, name)
                                        }
                                        if (are.length != 0) {
                                            name = 'ARE' + myfilename;
                                            fileArr.push(name);
                                            writeData(are, name)
                                        }
                                        if (arm.length != 0) {
                                            name = 'ARM' + myfilename;
                                            fileArr.push(name);
                                            writeData(arm, name)
                                        }
                                        if (art.length != 0) {
                                            name = 'ART' + myfilename;
                                            fileArr.push(name);
                                            writeData(art, name)
                                        }
                                        if (others.length != 0) {
                                            name = myfilename;
                                            fileArr.push(name);
                                            writeData(others, name)
                                        }
                                    }
                                }

                                function writeData(array, name) {
                                    // console.log(array)
                                    json2csv({
                                        data: array,
                                        //fields: ctx.req.fields,
                                        hasCSVColumnTitle: false
                                    }, function (err, csv) {
                                        if (err) {
                                            throw err;
                                        } else {
                                            var path = constants.EXPORT_PATH + name;
                                            fs.writeFile(path, csv, function (err) {
                                                if (err)
                                                    throw err;
                                            });
                                        }
                                    });
                                }
                                response.url = '/api/files/export/download/' + myfilename;
                                response.files = fileArr;
                                ctx.res.send(response);
                            }
                            if (ctx.args.data.email != null &&
                                ctx.args.data.email != undefined) {
                                common.sendMail(ctx.args.data.email, myfilename);
                            }
                        }
                    });
                } else {
                    response.status = false;
                    if (ctx.args.data.usertype === 'D') {
                        response.message = appmsg.DORDER_NT_FOUND;
                    } else {
                        response.message = appmsg.ORDER_LIST_NTFOUND;
                    }
                    ctx.res.send(response);
                }
            }
        });
    };
    TblOrders
        .remoteMethod(
        'exportcsv', {
            description: 'Get orderdetails for requested user id and export data to csv ',
            accepts: [{
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            }, {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'body'
                }
            }],
            returns: {
                arg: 'message',
                type: 'object',
                root: true
            },
            http: {
                verb: 'post'
            }
        });

    // Request Alteration before Save
    function modifySaveRequest(ctx, model, next) {
        try {
            log.info(filename + ">>modifySaveRequest>>");
            console.error(ctx.args.data);
            if (ctx.args.data.muserid && ctx.args.data.suserid && (ctx.args.data.merchant == undefined || ctx.args.data.merchant == null)) {
                var userid = ctx.args.data.suserid;
                if (ctx.args.data.oflnordno != null && ctx.args.data.oflnordno != undefined) {
                    ctx.args.data.oflnordid = ctx.args.data.oflnordno;
                }
                checkPayment(userid);
            } else if (ctx.args.data.muserid && (ctx.args.data.merchant == undefined || ctx.args.data.merchant == null)) {
                var userid = ctx.args.data.muserid;
                if (ctx.args.data.oflnordno != null && ctx.args.data.oflnordno != undefined) {
                    ctx.args.data.oflnordid = ctx.args.data.oflnordno;
                }
                checkPayment(userid);
            } else if (ctx.args.data.muserid != null && ctx.args.data.merchant) {
                var merchant = ctx.args.data.merchant;
                app.models.TblUser.upsert({
                    userid: ctx.args.data.muserid,
                    pymtstatus: 'Credit',
                    userstatus: 'Active',
                    cflag: merchant.cflag
                });
                if (ctx.args.data.oflnordno != null && ctx.args.data.oflnordno != undefined) {
                    ctx.args.data.oflnordid = ctx.args.data.oflnordno;
                }
                next();
            } else if (ctx.args.data.muserid == null && ctx.args.data.merchant) {
                console.log("expected")
                var merchant = ctx.args.data.merchant;
                console.log(merchant)
                app.models.tbl_user.findOne({where:{mobileno:merchant.mobileno}}, function (err, obj) {
                    if (obj != null) {
                        ctx.args.data.muserid = obj.userid;
                        next();
                    } else {
                        app.models.TblUser.create({
                            partycd: merchant.partycd,
                            mobileno: merchant.mobileno,
                            usertype: merchant.usertype,
                            fullname: merchant.fullname,
                            emailid: merchant.emailid,
                            companyname: merchant.companyname,
                            address: merchant.address,
                            pymtstatus: 'Credit',
                            userstatus: 'Active',
                            cflag: merchant.cflag,
                            tin: merchant.tin,
                            duserid: merchant.duserid,
                        }, function (err, obj) {
                            
                                app.models.tbl_slno_gen.upsert(ctx.args.data.slobj, function (err, data) {
                                    if (err) {
                                        log.error(err);
                                    } else {
                                        log.info(data);
                                        log.info("serial number updated successfully");
                                    }
                                });
                            if (err) {
                                var response = {
                                    status: false,
                                    message: appmsg.INTERNAL_ERR
                                }
                                log.error(filename + ">>addmerchant>>" +
                                    response.message);

                                ctx.res.send(response);
                            } else if (obj) {
                                ctx.args.data.muserid = obj.userid;
                                if (ctx.args.data.oflnordno != null && ctx.args.data.oflnordno != undefined) {
                                    ctx.args.data.oflnordid = ctx.args.data.oflnordno;
                                }
                            app.models.TblUserDealer.create({
                                muserid: obj.userid,
                                duserid: merchant.duserid,
                                dstatus: 'Active'
                            }, function (err, obj) {
                                if (err) {
                                    var response = {
                                        status: false,
                                        message: appmsg.INTERNAL_ERR
                                    }
                                    log.error(filename + ">>mapmerchant>>" +
                                        response.message);

                                    ctx.res.send(response);
                                }
                            });
                                next();
                            }
                        });
                    }
                });

            }

            function checkPayment(userid) {
                app.models.TblUser.find({
                    where: {
                        pymtstatus: appmsg.PYMT_STATUS,
                        userid: userid
                    }
                }, function (err, muser) {
                    if (muser.length != 0) {
                        var condition = {
                            oflnordid: ctx.args.data.oflnordid,
                            muserid: ctx.args.data.muserid
                        }
                        if (ctx.args.data.suserid != null &&
                            ctx.args.data.suserid != undefined) {
                            condition.suserid = ctx.args.data.suserid;
                        }
                        if (ctx.req.body.usertype == 'M') {
                            next();
                        } else {
                            TblOrders.find({ where: condition }, function (err, orders) {
                                if (orders.length != 0) {
                                    try {
                                        if (ctx.req.body.orderdtls.length != 0) {
                                            for (var i = 0; i < ctx.req.body.orderdtls.length; i++) {
                                                var orderdetails = ctx.req.body.orderdtls[i];
                                                var QtyOrder = {
                                                    ordid: orders[0].ordid,
                                                    duserid: orderdetails.duserid,
                                                    prodid: orderdetails.prodid,
                                                    qty: orderdetails.qty,
                                                    fqty: orderdetails.fqty,
                                                    ordslno: orderdetails.ordslno,
                                                    prate: orderdetails.prate,
                                                    ptax: orderdetails.ptax,
                                                    pvalue: orderdetails.pvalue,
                                                    isfree: orderdetails.isfree,
                                                    isread: orderdetails.isread,
                                                    /*batchno : orderdetails.batchno,
                                                    expirydt : orderdetails.expirydt,
                                                    manufdt: orderdetails.manufdt,*/
                                                    stock: orderdetails.stock,
                                                    ordstatus: orderdetails.ordstatus,
                                                    deliverydt: orderdetails.deliverydt,
                                                    default_uom: orderdetails.default_uom,
                                                    ord_uom: orderdetails.ord_uom,
                                                    updateddt: new Date(),
                                                }
                                                if (orderdetails.manufdt != null && orderdetails.manufdt != 'null' && orderdetails.manufdt != '') {
                                                    QtyOrder.manufdt = orderdetails.manufdt;
                                                }
                                                if (orderdetails.expirydt != null && orderdetails.expirydt != 'null' && orderdetails.expirydt != '') {
                                                    QtyOrder.expirydt = orderdetails.expirydt;
                                                }
                                                if (orderdetails.batchno != null && orderdetails.batchno != 'null' && orderdetails.batchno != '') {
                                                    QtyOrder.batchno = orderdetails.batchno;
                                                }
                                                if (orderdetails.deliverytype != null &&
                                                    orderdetails.deliverytype != undefined) {
                                                    QtyOrder.deliverytype = orderdetails.deliverytype;
                                                }
                                                if (orderdetails.deliverytime != null &&
                                                    orderdetails.deliverytime != undefined) {
                                                    QtyOrder.deliverytime = orderdetails.deliverytime;
                                                }
                                                insertIntoOrderDtls(QtyOrder);
                                                var response = {
                                                    status: true,
                                                    data: {
                                                        ordid: orders[0].ordid,
                                                        muserid: orders[0].muserid
                                                    },
                                                    message: appmsg.ORDER_SAVE_SUCCESS
                                                }
                                            }
                                            ctx.res.send(response);
                                        }
                                    } catch (ex) {
                                        console.log(ex);
                                    }
                                } else {
                                    if (ctx.args.data.oflnordno != null && ctx.args.data.oflnordno != undefined) {
                                        ctx.args.data.oflnordid = ctx.args.data.oflnordno;
                                    }
                                    next();
                                }
                            });
                        }
                    } else {
                        console.error(err);
                        var response = {
                            status: false,
                            message: appmsg.PYMT_PENDING
                        }
                        ctx.res.send(response);
                    }
                });
            }

        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            };
            console.error(e);
            ctx.res.send(response);

        }
    }

    // Insert into order details
    function insertIntoOrderDtls(orderdtl) {
        try {
            log.info(orderdtl);
            //Duplicate check
            app.models.TblOrderDtl.find({
                where: {
                    ordid: orderdtl.ordid,
                    prodid: orderdtl.prodid,
                }
            }, function (err, order) {
                if (order.length == 0) {
                    app.models.TblOrderDtl.create(orderdtl, function (err, result) {
                        console.log(result);
                    });
                } else {
                    app.models.TblOrderDtl.upsert(orderdtl, function (err, result) {
                        console.log(result);
                    });
                }
            })
        } catch (e) {
            console.error(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    }
    var rule = new schedule.RecurrenceRule();
    rule.hour = 6;
    rule.minute = 02;
    var j = schedule.scheduleJob(rule, function () {
        sendnotification();
        log.info(filename + ">>sendtriggered>>");
    });
    // Send Notification Due in Order Payment
    function sendnotification() {
        try {
            TblOrders.find({
                include: ['muser']
            }, function (err, result) {
                if (err) {
                    log.error(err);
                    response.status = true;
                    response.message = appmsg.INTERNAL_ERR;
                    //ctx.res.send(response);
                } else {
                    var orders = result;
                    for (var i = 0; i < orders.length; i++) {
                        if (orders[i].ordertotal != null &&
                            orders[i].pymttotal != null) {
                            if (orders[i].ordertotal > orders[i].pymttotal) {
                                if (orders[i].muserid != null) {
                                    var orderslist = orders[i].toJSON();
                                    var Mmobileno = orderslist.muser.mobileno;
                                    if (Mmobileno.length == 10) {
                                        var amt = orders[i].ordertotal - orders[i].pymttotal;
                                        var msg = appmsg.PYMNT_NOTIFI + " " + amt;
                                        var smsResult = common.sendSMS(constants.smsgatewayuri, constants.smsgatewayuser, constants.smsgatewaypassword, constants.smsgatewaysenderID, Mmobileno, msg);
                                    }
                                }
                            }
                        }
                    }
                }
            });
        } catch (err) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    }


    /**
     * To get the order list based on the usertype
     * @method orders
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrders.report = function (ctx, cb) {
        try {
            var response = {};
            log.info(ctx.args.data);
            var ds = TblOrders.dataSource;
            datalist = [];
            if (ctx.args.data.usertype === 'D') {
                var query = "select tbl_orders.* ,tbl_user.fullname from tbl_orders join tbl_user on tbl_user.userid=tbl_orders.suserid where tbl_orders.suserid in (?) and substring(tbl_orders.orderdt,1,10) between ? and ?";
                var qryArr = [ctx.args.data.suserarr, ctx.args.data.fromdt, ctx.args.data.todt];
                ds.connector.execute(query, qryArr, function (err, orders) {
                    if (err) {
                        console.error(err);
                        response.status = true;
                        response.message = appmsg.INTERNAL_ERR;
                        ctx.res.send(response);
                    } else {
                        if (orders.length != 0) {
                            response.status = true;
                            response.message = appmsg.LIST_FOUND;
                            var suserarr = [];
                            suserarr = ctx.args.data.suserarr;
                            var slen = suserarr.length;
                            async.map(suserarr, function iteratee(item, callback) {
                                var data = _.where(orders, { suserid: item });
                                common.calculateDistance(data, item, function (result) {
                                    //console.log(result);									
                                    if (result.status == true) {
                                        var obj = {
                                            data: _.where(orders, { suserid: result.key }),
                                            distance: (result.distance * 0.001),
                                            amount: (result.distance * 0.001) * 1.5,
                                            fullname: result.fullname
                                        }
                                        datalist.push(obj);
                                    }
                                    slen = slen - 1;
                                    console.log(slen);
                                    if (slen == 0) {
                                        response.data = datalist;
                                        ctx.res.send(response);
                                    }
                                });
                            }, function done() {
                                response.data = datalist;
                                ctx.res.send(response);
                            });
                        } else {
                            response.status = false;
                            response.message = appmsg.DORDER_NT_FOUND;
                            ctx.res.send(response);
                        }
                    }
                });
            } else {
                response.status = false;
                response.message = appmsg.LIST_NTFOUND;
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblOrders.remoteMethod('report', {
        description: 'Get report for requested user id',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }

    });
    /**
     * To update the order list
     * @method updateorder
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblOrders.updateorder = function (ctx, cb) {
        try {
            reply = {}
            if (_.has(ctx.args.data, 'orderdetails') == true && ctx.args.data.orderdetails.length != 0) {
                var orderdetails = ctx.args.data.orderdetails;
                var data = ctx.args.data;
                TblOrders.findById(data.ordid, [], function (err, order) {
                    length = 0;
                    if (order != null) {
                        for (var i = 0; i < orderdetails.length; i++) {
                            if (orderdetails[i].product.prodid != undefined && orderdetails[i].product.prodid != null) {
                                var orderupdate = {
                                    qty: orderdetails[i].qty,
                                    cqty: orderdetails[i].cqty,
                                    pvalue: orderdetails[i].pvalue,
                                    discntprcnt: orderdetails[i].discntprcnt,
                                    discntval: orderdetails[i].discntval,
                                    ord_uom: orderdetails[i].ord_uom
                                }
                                log.info(orderupdate);
                                app.models.TblOrderDtl.updateAll({ orddtlid: orderdetails[i].orddtlid }, orderupdate, function (err, result) {
                                    log.info(err);
                                    log.info(result);
                                    length = length + 1;
                                    if (length == i) {
                                        var reply = {
                                            status: true,
                                            message: appmsg.UPDATE_SUCCESSFULLY,
                                            data: data.ordid
                                        }
                                        updateHeader(order, function (status, data, id) {
                                            if (status) {
                                                data.ordid = id;
                                                TblOrders.updateAll({ ordid: id }, data, function (err, result) {
                                                    log.info(err);
                                                    if (result) {
                                                        log.info(result);
                                                    }
                                                    ctx.res.send(reply);
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                reply = {};
                                reply.message = appmsg.INVALID_PRODID;
                                ctx.res.send(reply);
                                length = length + 1;
                                break;
                            }
                        }
                    } else {
                        var reply = {
                            status: false,
                            message: appmsg.INVALID_ORDID
                        }
                        ctx.res.send(reply);
                    }
                });
            }
        } catch (e) { }
    };
    TblOrders.remoteMethod('updateorder', {
        description: 'Update order headers and details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }

    });

    function updateHeader(data, callback) {
        try {
            app.models.TblOrderDtl.find({ where: { ordid: data.ordid } }, function (err, orders) {
                if (err) throw err;
                else {
                    if (orders.length != 0) {
                        var update = { discntval: 0.0, discntprcnt: 0.0, aprxordval: 0.0 };
                        for (var i = 0; i < orders.length; i++) {
                            update.aprxordval = update.aprxordval + orders[i].pvalue;
                            length = length - 1;
                            if (length == 0) {
                                callback(true, update, data.ordid);
                            }
                        }
                    } else {
                        callback(true, data);
                    }
                }

            });
        } catch (e) {
            callback(false, e);
        }
    }

    function validateOrders(ctx, model, next) {
        try {
            reply = {};
            reply.status = false;
            if (_.isUndefined(ctx.args.data.ordid) || _.isNull(ctx.args.data.ordid)) {
                reply.message = appmsg.INVALID_ORDID;
                ctx.res.send(reply);
            } else if (_.isUndefined(ctx.args.data.orderdetails) || _.isNull(ctx.args.data.orderdetails) || ctx.args.data.orderdetails.length == 0) {
                reply.message = appmsg.INVALID_ORDLIST;
                ctx.res.send(reply);
            } else {
                next();
            }

        } catch (e) {
            console.log(e);
        }
    }
    function modifySaveCheck(ctx, model, next) {

        try {
            condition = {};
            if (ctx.args.data.merchant!=null && ctx.args.data.merchant!=undefined) {
                condition.mobileno = ctx.args.data.mobileno;
                app.models.tbl_user.findOne({ where: condition }, function (err, obj) {
                    if (obj == null) {
                        var response = {
                            status: false,
                            exist: 'Y',
                            data: obj,
                            message: appmsg.EXISTING_USER
                        }
                        ctx.res.send(response);
                        console.log(response);
                    } else if (err) {
                        var response = {
                            status: false,
                            message: appmsg.INTERNAL_ERR
                        }
                        log.error(filename + ">>modifySaveObject>>" + response.message);
                        ctx.res.send(response);
                    } else if(obj != null){
                        console.log("expected");
                        ctx.args.data.otpstatus = appmsg.STATUS_PENDING;
                        ctx.args.data.pymtstatus = appmsg.STATUS_PENDING;
                        // if (ctx.args.data.usertype == 'C' || ctx.args.data.usertype == 'M') {
                        ctx.args.data.userstatus = appmsg.STATUS_ACTIVE;
                        var serialnum = {
                            duserid: ctx.args.data.duserid,
                            ref_key: constants.PARTYCODE,
                            status: appmsg.STATUS_ACTIVE
                        };
                        log.info(serialnum);
                        app.models.tbl_slno_gen.findOne({
                            where: serialnum
                        }, function (err, result) {
                            log.info(result);
                            if (err) {
                                log.error(err);
                                next();
                            } else if (result != null) {
                                var sno = result.prefix_key + "" + result.prefix_cncat +
                                    "" + result.suffix_key + "" +
                                    result.suffix_cncat + "" + result.curr_seqno;
                                log.info(sno);
                                ctx.args.data.merchant.partycd = sno;
                                var slobj = {
                                    slno_id: result.slno_id,
                                    curr_seqno: result.curr_seqno + 1,
                                    last_seqno: result.curr_seqno,
                                    last_updated_dt: new Date(),
                                    last_updated_by: ctx.args.data.fullname
                                }
                                ctx.args.data.slobj = slobj;
                                log.info(slobj);
                                log.info(ctx.args.data.slobj);
                                next();
                            } else {
                                // if (ctx.args.data.usertype == 'M' || ctx.args.data.usertype == 'C') {

                                var slobj = {
                                    duserid: ctx.args.data.duserid,
                                    ref_key: serialnum.ref_key,
                                    curr_seqno: 1,
                                    last_seqno: 0,
                                    status: appmsg.STATUS_ACTIVE,
                                    last_updated_dt: new Date(),
                                    last_updated_by: ctx.args.data.fullname
                                }
                                slobj.prefix_key = 'PARTY';
                                slobj.prefix_cncat = '-';
                                slobj.suffix_key = 'CODE';
                                slobj.suffix_cncat = '-';
                                slobj.ref_key = constants.PARTYCODE;
                                app.models.tbl_slno_gen.create(slobj, function (err, serialnumber) {
                                    if (err) {
                                        next();
                                    } else if (serialnumber) {
                                        var sno = serialnumber.prefix_key + "" + serialnumber.prefix_cncat +
                                            "" + serialnumber.suffix_key + "" +
                                            serialnumber.suffix_cncat + "" + serialnumber.curr_seqno;
                                        log.info(sno);
                                        ctx.args.data.merchant.partycd = sno;
                                        var updateobj = {
                                            slno_id: serialnumber.slno_id,
                                            curr_seqno: serialnumber.curr_seqno + 1,
                                            last_seqno: serialnumber.curr_seqno + 1,
                                            last_updated_dt: new Date(),
                                            last_updated_by: ctx.args.data.fullname
                                        }
                                        ctx.args.data.slobj = updateobj;

                                        log.info(ctx.args.data.slobj);
                                        app.models.tbl_slno_gen.upsert(ctx.args.data.slobj, function (err, data) {
                                            if (err) {
                                                log.error(err);
                                            } else {
                                                log.info(data);
                                                log.info("serial number updated successfully");
                                            }
                                        });
                                    }
                                });

                                // } else {
                                //     next();
                                // }
                            }
                        });
                        // } else {
                        //     next();
                        // }
                    }
                });
            } else {
                next();
            }

        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            ctx.res.send(response);
        }
    }
    TblOrders.beforeRemote('updateorder', validateOrders);
    TblOrders.afterRemote('create', modifySaveResponse);
    TblOrders.beforeRemote('create', modifySaveCheck);
    TblOrders.beforeRemote('create', modifySaveRequest);
    TblOrders.afterRemote('upsert', modifyUpsertResponse);
    TblOrders.afterRemote('updateAll', modifyUpsertResponse);
    TblOrders.beforeRemote('find', modifyFindRequest);
    TblOrders.afterRemote('find', modifyFindResponse);
};