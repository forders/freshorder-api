/**
 * @Filename : tbl-prod-settings.js
 * @Description : To write hooks product-settings.
 * @Author : Nithya
 * @Date : Oct 07, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version Date Modified By Remarks 0.1 Oct 06 Nithya Remote hooks added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(Tblprodsetting) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.settingid,
					message : appmsg.PROSETTING_SAVE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.APPSETTING_UPDATE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after UPDATE/UPSERT
	function modifyUpdateResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.settingid,
					message : appmsg.PROSETTING_UPDATE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.APPSETTING_UPDATE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				if (model.length === 0) {
					var response = {
						status : false,
						message : appmsg.LIST_NT_FOUND
					}
				} else {
					var response = {
						status : true,
						data : model,
						message : appmsg.LIST_FOUND
					}
				}

			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
			}
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Bulk Update
	Tblprodsetting.bulkupdate = function(ctx, cb) {
		try{
			log.info(ctx.args.data.prodlist);
			var prodlist = ctx.args.data.prodlist;
			var len = prodlist.length;
			for (var i = 0; i < prodlist.length; i++) {
				Tblprodsetting.upsert(prodlist[i], function(err, settings) {
					if (err) {
						var response = {
							status : false,
							message : appmsg.INTERNAL_ERR
						}
						log.error(filename + ">>bulkupdate>>"
								+ response.message);
						ctx.res.status(500);
						ctx.res.send(response);
					} else {
						len--;
						if (len == 0) {
							var response = {
								status : true,
								message : appmsg.PROSETTING_SAVE_SUCCESS
							}
							log.info(filename + ">>bulkupdate>>"
									+ response.message);
							ctx.res.status(200);
							ctx.res.send(response);
						}
					}
				});
			}
		}catch(e){
			log.error(e);
			var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
				log.error(filename + ">>bulkupdate>>"
						+ response.message);
				ctx.res.status(500);
				ctx.res.send(response);
		}
		
	};
	Tblprodsetting.remoteMethod('bulkupdate', {
		description : 'Bulk update of settings details',
		accepts : [{
			arg : 'ctx',
			type : 'object',
			http : {
				source : 'context'
			}
		}, {
			arg : 'data',
			type : 'object',
			http : {
				source : 'body'
			}
		}],
		returns : {
			arg : 'message',
			type : 'object',
			root : true
		},
		http : {
			verb : 'post'
		}
	});
	Tblprodsetting.afterRemote('create', modifySaveResponse);
	Tblprodsetting.afterRemote('updateAttributes', modifyUpdateResponse);
	Tblprodsetting.afterRemote('updateAll', modifyUpdateResponse);
	Tblprodsetting.afterRemote('upsert', modifyUpdateResponse);
	Tblprodsetting.afterRemote('find', modifyFindResponse);
};
