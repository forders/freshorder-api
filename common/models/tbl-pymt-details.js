/**
 * @Filename : tbl-pymt-details.js
 * @Description : To write hooks for the payment details related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var constants = require('../config/constants.js');
var common = require('../services/commonServices.js');
module.exports = function(TblPymtDetails) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                message: appmsg.PYMT_SAVE_SUCCESS,
                data: model.pymntid
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
        } else {
            var response = {
                status: false,
                message: appmsg.PYMT_SAVE_FAILED
            }
            log.error(filename + ">>modifySaveResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                data: model.pymntid,
                message: appmsg.PYMT_UPDATE_SUCCESS
            }
            log.info(filename + ">>modifyUpsertResponse>>" + response.message);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }

    // Response Message after UPDATEALL
    function modifyUpdateAllResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        log.info(ctx.args.data);
        if (status && status === 200) {
            var len = ctx.req.body.saleslist.length;
            for (var i = 0; i < ctx.req.body.saleslist.length; i++) {
                if (ctx.req.body.saleslist[i] != null) {
                    app.models.TblUser.findOne({
                            where: {
                                userid: ctx.req.body.saleslist[i],
                            }
                        },
                        function(err, user) {
                            len--;
                            if (user) {
                                if (ctx.args.data.pymtstatus == appmsg.PYMT_STATUS) {
                                    console.log(user.nextpymtdt);
                                    if (user.nextpymtdt != '0000-00-00 00:00:00') {
                                        var pymtdt = new Date(user.nextpymtdt);
                                    } else {
                                        var pymtdt = new Date();
                                    }
                                    if (ctx.args.data.pymttenure == appmsg.PYMT_MONTHLY) {
                                        var nextpymtdt = new Date(pymtdt.getFullYear(), pymtdt.getMonth(), pymtdt.getDate() + 30)
                                    } else if (ctx.args.data.pymttenure == appmsg.PYMT_YEARLY) {
                                        var nextpymtdt = new Date(pymtdt.getFullYear() + 1, pymtdt.getMonth(), pymtdt.getDate())
                                    } else {
                                        var nextpymtdt = new Date(pymtdt.getFullYear(), pymtdt.getMonth(), pymtdt.getDate() + 90)
                                    }
                                    app.models.TblUser.upsert({
                                        userid: user.userid,
                                        nextpymtdt: nextpymtdt,
                                        lastpymtdt: pymtdt,
                                        pymtstatus: ctx.args.data.pymtstatus
                                    });
                                }
                                if (len == 0) {
                                    var response = {
                                        status: true,
                                        data: user.pymntid,
                                        pymtstatus: ctx.args.data.pymtstatus,
                                        message: appmsg.PYMT_UPDATE_SUCCESS
                                    }
                                    ctx.res.set('Content-Location', 'the internet');
                                    ctx.res.status(status);
                                    ctx.res.send(response);
                                }
                            } else {
                                var response = {
                                    status: false,
                                    message: appmsg.INTERNAL_ERR
                                }
                                ctx.res.set('Content-Location', 'the internet');
                                ctx.res.status(status);
                                ctx.res.send(response);
                            }
                        });
                }
            }
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.info(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } else {
            var response = {
                status: false,
                message: appmsg.PYMT_UPDATE_FAILED
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        }
    }
    // Request Alteration before Find data
    function modifyFindRequest(ctx, model, next) {
        log.info(filename + ">>modifyFindRequest>>");
        next();
    }

    /**
     * To Update payment details of the app subscription 
     * @method updatepayment
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblPymtDetails.updatepayment = function(ctx, cb) {
        console.log(ctx.args.data);
        try {
            common.getPaymentdetails(ctx.args.data.payment_request_id, ctx.args.data.payment_id, function(result) {
                if (result == false) {
                    ctx.res.send(result);
                } else {
                    var data = result.data;
                    TblPymtDetails.findOne({
                        where: {
                            pymntid: ctx.args.data.pymtid
                        }
                    }, function(err, paymentdtls) {
                        if (err) {
                            var response = {
                                status: false,
                                message: appmsg.INTERNAL_ERR
                            }
                            log.error(filename + ">>updatepayment>>" + response.message);
                            ctx.res.send(response);
                        } else {
                            var pymtdetails = {
                                userid: ctx.args.data.userid,
                                pymtref: ctx.args.data.payment_id,
                                pymtstatus: data.payment_request.payment.status,
                                pymntid: ctx.args.data.pymtid,
                                pymtdate: new Date()
                            }
                            TblPymtDetails.upsert(pymtdetails, function(err, pymt) {
                                if (err) {
                                    var response = {
                                        status: false,
                                        message: appmsg.INTERNAL_ERR
                                    }
                                    log.error(filename + ">>updatepayment>>" + response.message);
                                    ctx.res.send(response);
                                } else {
                                    var pymtdt = pymt.pymtdate;
                                    if (ctx.args.data.pymttenure == appmsg.PYMT_MONTHLY) {
                                        var nextpymtdt = new Date(pymtdt.getFullYear(), pymtdt.getMonth(), pymtdt.getDate() + 30)
                                    } else if (ctx.args.data.pymttenure == appmsg.PYMT_YEARLY) {
                                        var nextpymtdt = new Date(pymtdt.getFullYear() + 1, pymtdt.getMonth(), pymtdt.getDate())
                                    } else {
                                        var nextpymtdt = new Date(pymtdt.getFullYear(), pymtdt.getMonth(), pymtdt.getDate() + 90)
                                    }
                                    try {
                                        var saleslist = (paymentdtls.remarks).split(',');
                                        console.log(saleslist);
                                        var len = saleslist.length;
                                        if (saleslist.length != 0) {
                                            for (var i = 0; i < saleslist.length; i++) {
                                                app.models.TblUser.upsert({
                                                    userid: saleslist[i],
                                                    lastpymtdt: pymt.pymtdate,
                                                    nextpymtdt: nextpymtdt,
                                                    pymtstatus: data.payment_request.payment.status,
                                                    updateddt: pymt.pymtdate
                                                }, function(err, upUser) {
                                                    len--;
                                                    if (len == 0) {
                                                        var response = {
                                                            status: true,
                                                            message: appmsg.PYMT_SAVE_SUCCESS
                                                        }
                                                        ctx.res.status(200);
                                                        ctx.res.send(response);
                                                    }
                                                });
                                            }
                                        }
                                    } catch (ex) {
                                        console.log(ex);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            console.log(e);
            ctx.res.send(response);
        }
    };
    TblPymtDetails
        .remoteMethod(
            'updatepayment', {
                description: 'Get Payment details using the transaction ID,from the instamojo api',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });

    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            if (model.length === 0) {
                var response = {
                    status: false,
                    message: appmsg.LIST_NT_FOUND
                }
                log.info(filename + ">>modifyFindResponse>>" + response.message);
            } else {
                var response = {
                    status: true,
                    data: model,
                    message: appmsg.LIST_FOUND
                }
                log.info(filename + ">>modifyFindResponse>>" + response.message);
            }

        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyFindResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Payment-Request
    TblPymtDetails.paymentrequest = function(ctx, cb) {
        try {
            var data = ctx.args.data;
            console.log(data);
            common.makePayment(data, function(result) {
                ctx.res.send(result);
            });
        } catch (e) {
            console.log(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblPymtDetails
        .remoteMethod(
            'paymentrequest', {
                description: 'Generate payment-link with dynamic amount',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });

    function modifySaveRequest(ctx, model, next) {
        ctx.args.data.suserid = ctx.args.data.saleslist;
        next();
    }
    TblPymtDetails.afterRemote('create', modifySaveResponse);
    TblPymtDetails.beforeRemote('create', modifySaveRequest);
    TblPymtDetails.afterRemote('updateAll', modifyUpdateAllResponse);
    TblPymtDetails.beforeRemote('find', modifyFindRequest);
    TblPymtDetails.afterRemote('find', modifyFindResponse);
};