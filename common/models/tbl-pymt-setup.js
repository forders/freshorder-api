/**
 * @Filename : tbl-pymt-setup.js
 * @Description : To write hooks for the payment related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
module.exports = function(TblPymtSetup) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				data:model.psetupid,
				message : appmsg.PYMT_SAVE_SUCCESS
			}
			log.info(filename + ">>modifySaveResponse>>"+response.message);
			ctx.res.status(status);
		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifySaveResponse>>"+response.message);
			ctx.res.status(status);
		} else {
			var response = {
				status : false,
				message : appmsg.PYMT_SAVE_FAILED
			}
			log.error(filename + ">>modifySaveResponse>>"+response.message);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.send(response);
	}
	// Response Message after UPDATE
	function modifyUpsertResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				data:model.psetupid,
				message : appmsg.UPDATE_SUCCESSFULLY
			}
			log.info(filename + ">>modifyUpsertResponse>>"+response.message);
			ctx.res.status(status);
		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifyUpsertResponse>>"+response.message);
		} else {
			var response = {
				status : false,
				message : appmsg.PYMT_SAVE_FAILED
			}
			log.error(filename + ">>modifyUpsertResponse>>"+response.message);
			ctx.res.status(status);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.send(response);
	}
	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			if (model.length === 0) {
				var response = {
					status : false,
					message : appmsg.LIST_NT_FOUND
				}
				log.info(filename + ">>modifyFindResponse>>"+response.message);
			} else {
				var response = {
					status : true,
					data : model,
					message : appmsg.LIST_FOUND
				}
				log.info(filename + ">>modifyFindResponse>>"+response.message);
			}

		} else if (status && status === 500) {
			var response = {
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifyFindResponse>>"+response.message);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.status(status);
		ctx.res.send(response);
	}
	TblPymtSetup.afterRemote('create', modifySaveResponse);
	TblPymtSetup.afterRemote('upsert', modifyUpsertResponse);
	TblPymtSetup.afterRemote('find', modifyFindResponse);
};
