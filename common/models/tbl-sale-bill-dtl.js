/**
 * @Filename : tbl-sale-bill-dtl.js
 * @Description : To write hooks sale-bill-dtl.
 * @Author : Bhuvaneshwari B
 * @Date : Oct 22, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 		Date 			Modified By 	Remarks
 * 0.1			Oct 24, 2016	Bhuvaneshwari	
 */
var path = require('path');
var app = require('../../server/server');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
module.exports = function(Tblsalebilldtl) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.billdtlid,
                    message: appmsg.SALEBILDTL_SAVE_SUCCESS
                }
                log.info(ctx.req.body.slobj);
                app.models.Tblslnogen.upsert(ctx.req.body.slobj, function(err, data) {
                    if (err) {
                        log.error(err);
                    } else {
                        log.info(data);
                        log.info("serial number updated successfully");
                    }
                });
            } else {
                var response = {
                    status: false,
                    message: appmsg.SALEBILDTL_SAVE_FAILED
                }
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    }

    // Response Message after UPDATE/UPSERT
    function modifyUpdateResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.billdtlid,
                    message: appmsg.SALEBILDTL_UPDATE_SUCCESS
                }
            } else {
                var response = {
                    status: false,
                    message: appmsg.SALEBILDTL_UPDATE_FAILED
                }
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                if (model.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.LIST_NT_FOUND
                    }
                } else {
                    var response = {
                        status: true,
                        data: model,
                        message: appmsg.LIST_FOUND
                    }
                }

            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
            }
            log.info(filename + ">>modifyFindResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    }
    // Bulk Update Sales Details details
    Tblsalebilldtl.bulksalesdetailsupdate = function(ctx, cb) {
        log.info(ctx.args.data.billdtls);
        try {
            var bills = ctx.args.data.billdtls;
            var length = bills.length;
            for (var i = 0; i < length; i++) {
                var billdetails = ctx.args.data.billdtls[i];
                var salebilldtl = {
                    billid: billdetails.billid,
                    ordslno: billdetails.ordslno,
                    prodid: billdetails.prodid,
                    qty: billdetails.qty,
                    cqty: billdetails.cqty,
                    ord_uom: billdetails.ord_uom,
                    default_uom: billdetails.default_uom,
                    mrp: billdetails.mrp,
                    rate: billdetails.rate,
                    discount: billdetails.discount,
                    discntprcnt: billdetails.discntprcnt,
                    amount: billdetails.amount,
                    taxprcnt: billdetails.taxprcnt,
                    taxval: billdetails.taxval,
                    basicvalue: billdetails.basicvalue,
                    margin_percent: billdetails.margin_percent,
                    margin_value: billdetails.margin_value,
                    packverify: billdetails.packverify,
                    verifieddt: billdetails.verifieddt,
                    verifiedby: billdetails.verifiedby,
                }
                Tblsalebilldtl.upsert(salebilldtl, function(err, salebill) {
                    if (err) {
                        var response = {
                            status: false,
                            message: appmsg.INTERNAL_ERR
                        }
                        log.error(filename + ">>bulkupdate>>" +
                            response.message);
                        ctx.res.status(500);
                        ctx.res.send(response);
                    } else {
                        len--;
                        if (len == 0) {
                            var response = {
                                status: true,
                                message: appmsg.SALEBILDTL_UPDATE_SUCCESS
                            }
                            log.info(filename + ">>bulkupdate>>" +
                                response.message);
                            ctx.res.status(200);
                            ctx.res.send(response);
                        }
                    }
                });
            }
        } catch (e) {
            log.error(e);
            log.error(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    Tblsalebilldtl.remoteMethod('bulksalesdetailsupdate', {
        description: 'Bulk update sale bill details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    Tblsalebilldtl.afterRemote('create', modifySaveResponse);
    Tblsalebilldtl.afterRemote('updateAttributes', modifyUpdateResponse);
    Tblsalebilldtl.afterRemote('updateAll', modifyUpdateResponse);
    Tblsalebilldtl.afterRemote('upsert', modifyUpdateResponse);
    Tblsalebilldtl.afterRemote('find', modifyFindResponse);
};