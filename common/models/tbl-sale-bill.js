/**
 * @Filename : tbl-sale-bill.js
 * @Description : To write hooks sale-bill.
 * @Author : Bhuvaneshwari B
 * @Date : Oct 22, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 		Date 			Modified By 	Remarks
 * 0.1			Oct 24, 2016	Bhuvaneshwari	Serial number generated
 * 0.2 			Nov 04, 2016	Nithya  		comments added	
 */
var path = require('path');
var _ = require('underscore');
var app = require('../../server/server');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var constants = require('../config/constants.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
var process = require('child_process');
module.exports = function(Tblsalebill) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: {
                        billid: model.billid,
                        billno: model.billno
                    },
                    message: appmsg.SALEBIL_SAVE_SUCCESS
                };
                log.info(ctx.args.data.slobj);
                app.models.tbl_slno_gen.upsert(ctx.args.data.slobj, function(err, data) {
                    if (err) {
                        log.error(err);
                    } else {
                        log.info(data);
                        log.info("serial number updated successfully");
                    }
                });
                var duserid = model.duserid;
                if (ctx.args.data.billdtls.length !== 0) {
                    for (var i = 0; i < ctx.args.data.billdtls.length; i++) {
                        var billdetails = ctx.args.data.billdtls[i];
                        var salebilldtl = {
                            billid: model.billid,
                            ordslno: billdetails.ordslno,
                            prodid: billdetails.prodid,
                            qty: billdetails.qty,
                            cqty: billdetails.cqty,
                            ord_uom: billdetails.ord_uom,
                            default_uom: billdetails.default_uom,
                            mrp: billdetails.mrp,
                            rate: billdetails.rate,
                            discount: billdetails.discount,
                            discntprcnt: billdetails.discntprcnt,
                            amount: billdetails.amount,
                            taxprcnt: billdetails.taxprcnt,
                            taxval: billdetails.taxval,
                            basicvalue: billdetails.basicvalue,
                            packverify: billdetails.packverify,
                            margin_percent: billdetails.margin_percent,
                            margin_value: billdetails.margin_value,
                            verifieddt: billdetails.verifieddt,
                            verifiedby: billdetails.verifiedby,
                        };
                        app.models.tbl_sale_bill_dtl.create(salebilldtl, function(err, result) {
                            log.info(model);
                            common.updateStock(ctx.args.data.billdtls, model.duserid, function(result) {
                                log.info(result);
                            });
                        });
                    }
                    if (model.ordid !== null && model.ordid !== undefined && model.ordid !== '') {
                        app.models.tbl_order_dtl.updateAll({ ordid: model.ordid, duserid: model.duserid }, { ordstatus: 'Invoiced' }, function(err, updated) {
                            log.info(updated);
                        });
                    }
                } else {
                    var response = {
                        status: false,
                        message: appmsg.SALEBILDTL_SAVE_FAILED
                    };
                }
            } else {
                var response = {
                    status: false,
                    message: appmsg.SALEBIL_SAVE_FAILED
                };
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    // Request change before create
    function modifySaveRequest(ctx, model, next) {
        log.info(filename + ">>modifySaveRequest>>");
        try {
            var duserid = '';
            log.info(ctx.args.data);
            log.info(ctx.args.data.length);
            var refkey = constants.ORDCODE;
            if (ctx.args.data.length > 0) {
                duserid = ctx.args.data[0].duserid;
            } else {
                duserid = ctx.args.data.duserid;
            }
            var serialnum = {
                duserid: duserid,
                ref_key: refkey,
                status: appmsg.STATUS_ACTIVE
            };
            log.info(serialnum);
            app.models.tbl_slno_gen.findOne({
                where: serialnum
            }, function(err, result) {
                log.info(result);
                if (err) {
                    log.error(err);
                } else if (result != null) {
                    var sno = result.prefix_key + "" + result.prefix_cncat +
                        "" + result.suffix_key + "" +
                        result.suffix_cncat + "" + result.curr_seqno;
                    log.info("Order bill serial Number");
                    if (_.isArray(ctx.req.body) === false) {
                        log.info(sno);
                        ctx.req.body.billno = sno;
                        var slobj = {
                            slno_id: result.slno_id,
                            curr_seqno: result.curr_seqno + 1,
                            last_seqno: result.curr_seqno,
                            last_updated_dt: ctx.req.body.lastupdateddt,
                            last_updated_by: ctx.req.body.lastupdatedby
                        };
                        ctx.req.body.slobj = slobj;
                        log.info(slobj);
                        next();
                    } else {
                        ctx.req.body.slobj = result;
                        next();
                    }
                } else {
                    var slobj = {
                        prefix_key: 'BILL',
                        prefix_cncat: '-',
                        suffix_key: 'NO',
                        suffix_cncat: '-',
                        curr_seqno: 1,
                        last_seqno: 0,
                        ref_key: refkey,
                        status: appmsg.STATUS_ACTIVE,
                        duserid: ctx.args.data.duserid,
                        last_updated_dt: ctx.req.body.lastupdateddt,
                        last_updated_by: ctx.req.body.lastupdatedby
                    };
                    app.models.tbl_slno_gen.create(slobj, function(err, data) {
                        updateObj = {};
                        if (data) {
                            if (_.isArray(ctx.req.body) === false) {
                                var sno = data.prefix_key + "" + data.prefix_cncat +
                                    "" + data.suffix_key + "" +
                                    data.suffix_cncat + "" + data.curr_seqno;
                                ctx.req.body.billno = sno;
                                updateObj = {
                                    slno_id: data.slno_id,
                                    curr_seqno: data.curr_seqno + 1,
                                    last_seqno: data.curr_seqno,
                                    last_updated_dt: ctx.req.body.lastupdateddt,
                                    last_updated_by: ctx.req.body.lastupdatedby
                                };
                                ctx.req.body.slobj = updateObj;
                                next();
                            } else {
                                ctx.req.body.slobj = result;
                                next();
                            }
                        } else {
                            ctx.req.body.slobj = '';
                            next();
                        }
                        // log.info(slobj);
                    });
                }
            });
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                message: appmsg.INTERNALERR
            };
            ctx.res.send(response);
        }
    }
    // Response Message after UPDATE/UPSERT
    function modifyUpdateResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.billid,
                    message: appmsg.SALEBIL_UPDATE_SUCCESS
                };
            } else {
                var response = {
                    status: false,
                    message: appmsg.SALEBIL_UPDATE_FAILED
                };
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        response = {};
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                if (model.length === 0) {
                    response = {
                        status: false,
                        message: appmsg.LIST_NT_FOUND
                    };
                } else {
                    response = {
                        status: true,
                        data: model,
                        message: appmsg.LIST_FOUND
                    };
                }
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                };
            }
            log.info(filename + ">>modifyFindResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    /**
     * To get the packing or payment collection list based the user param
     * @method list
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    Tblsalebill.list = function(ctx, cb, next) {
        try {
            log.info(ctx.args.data);
            var response = {};
            var ordlist = [];
            if (ctx.args.data.mode !== undefined && ctx.args.data.mode !== null) {
                var mode = ctx.args.data.mode;
            } else {
                var mode = 'MOB';
            }
            if (ctx.args.data.usertype == 'D') {
                var query = 'SELECT DISTINCT tbl_order_dtl.ordid FROM tbl_order_dtl where tbl_order_dtl.duserid=? AND tbl_order_dtl.ordstatus = "Invoiced"';
                var qryArr = [ctx.args.data.duserid];
            } else if (ctx.args.data.usertype == 'S' && mode != 'WEB') {
                var query = 'SELECT DISTINCT od.ordid FROM tbl_orders as od join tbl_order_dtl as dtl on od.ordid = dtl.ordid where od.muserid=? AND od.suserid=? AND dtl.ordstatus = "Invoiced"';
                var qryArr = [ctx.args.data.muserid, ctx.args.data.suserid];
            }
            if (mode != 'WEB') {
                common.executeQuery(query, qryArr, function(result) {
                    console.log(result);
                    var len = result.data.length;
                    if (result.status == true) {
                        for (var i = 0; i < result.data.length; i++) {
                            ordlist.push(result.data[i].ordid);
                            len--;
                            if (len == 0) {
                                if (ctx.args.data.paylist == 'Y' && ctx.args.data.muser == 'Y') {
                                    var qry = 'SELECT DISTINCT tbl_sale_bill.billid, billno,grant_total, billtotal, paidamount, balamount, muser.companyname AS mcompany FROM tbl_order_dtl JOIN tbl_orders ON tbl_orders.ordid = tbl_order_dtl.ordid JOIN tbl_user AS muser ON muser.userid = tbl_orders.muserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_order_dtl.ordid WHERE tbl_sale_bill.billtotal > tbl_sale_bill.paidamount AND tbl_sale_bill.ordid IN (SELECT DISTINCT od.ordid FROM tbl_orders as od join tbl_order_dtl as dtl ON od.ordid = dtl.ordid where od.suserid in (?) AND dtl.ordstatus = "Invoiced") AND tbl_sale_bill.status = "Billed" AND SUBSTRING(tbl_sale_bill.billdt, 1, 10) BETWEEN ? AND ?';
                                    var qryarr = [ctx.args.data.slist, ctx.args.data.fromdt,
                                        ctx.args.data.todt
                                    ];
                                } else if (ctx.args.data.paylist == 'Y' && ctx.args.data.muser != 'Y') {
                                    var qry = 'SELECT * FROM tbl_sale_bill  WHERE tbl_sale_bill.billtotal > tbl_sale_bill.paidamount AND tbl_sale_bill.ordid IN (?) AND tbl_sale_bill.status = "Billed"';
                                    var qryarr = [ordlist];
                                } else {
                                    var qry = 'SELECT * FROM tbl_sale_bill WHERE ordid in (?) AND status="Billed"';
                                    var qryarr = [ordlist];
                                }
                                common.executeQuery(qry, qryarr, function(res) {
                                    log.info(res);
                                    ctx.res.send(res);
                                });
                            }
                        }
                    } else {
                        resp = {
                            status: false,
                            message: appmsg.LIST_NT_FOUND
                        }
                        ctx.res.send(resp);
                    }
                });
            } else {
                common.executeQuery(query, qryArr, function(res) {
                    var qry = 'SELECT bd . *, p .prodname,p .prodcode,p .hsncode, SUM(bd.qty) as unit,SUM(bd.basicvalue) as value FROM tbl_sale_bill as b join tbl_sale_bill_dtl as bd ON b.billid = bd.billid join tbl_user_prod as p ON bd.prodid = p.prodid where substring(b.billdt,1,10) =? and b.status="Billed" and b.ordid in (?) GROUP BY bd.prodid';
                    console.log(res);
                    if (res.status === true) {
                        var len = res.data.length;
                        for (var i = 0; i < res.data.length; i++) {
                            ordlist.push(res.data[i].ordid);
                            len--;
                            var qryarr = [ctx.args.data.fromdt, ordlist];
                            if (len === 0) {
                                common.executeQuery(qry, qryarr, function(res) {
                                    log.info(res);
                                    ctx.res.send(res);
                                });
                            }
                        }
                    } else {
                        resp = {
                            status: false,
                            message: appmsg.LIST_NT_FOUND
                        };
                        ctx.res.send(resp);
                    }
                });
            }
        } catch (e) {
            log.info(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    };


    Tblsalebill.remoteMethod('list', {
        description: 'Get payment collection or packing list',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    /**
     * To get the packing or payment collection list based the user param
     * @method list
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    Tblsalebill.smlist = function(ctx, cb, next) {
        try {
            var responseArr = [];
            var len = 0;
            log.info(ctx.args.data);
            if (ctx.args.data.slist) {
                len = ctx.args.data.slist.length;
            }
            if (len != 0) {
                // for (var i = 0; i < ctx.args.data.slist.length; i++) {
                // var slist = ctx.args.data.slist[i];
                if (ctx.args.data.usertype == 'D' && ctx.args.data.muser == 'Y') {
                    var query = 'SELECT fullname,userid FROM tbl_user WHERE userid in (?)';
                    var queryarr = [ctx.args.data.slist];
                    var smlist = ctx.args.data.slist;
                    rdata = [];
                    common.executeQuery(query, queryarr, function(data) {
                        var qry = 'SELECT DISTINCT tbl_sale_bill.billid, billno, billtotal, paidamount, balamount,grant_total, muser.companyname AS mcompany,tbl_orders.suserid FROM tbl_order_dtl JOIN tbl_orders ON tbl_orders.ordid = tbl_order_dtl.ordid JOIN tbl_user AS muser ON muser.userid = tbl_orders.muserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_order_dtl.ordid WHERE tbl_sale_bill.billtotal > tbl_sale_bill.paidamount AND tbl_sale_bill.ordid IN (SELECT DISTINCT od.ordid FROM tbl_orders as od join tbl_order_dtl as dtl ON od.ordid = dtl.ordid where od.suserid in (?) AND dtl.ordstatus = "Invoiced") AND tbl_sale_bill.status = "Billed" AND SUBSTRING(tbl_sale_bill.billdt, 1, 10) BETWEEN ? AND ? order by mcompany';
                        var qryarr = [ctx.args.data.slist, ctx.args.data.fromdt, ctx.args.data.todt];
                        log.info(qryarr);
                        common.executeQuery(qry, qryarr, function(res) {
                            log.info(res);
                            _.filter(smlist, function(item, index) {
                                var resultant = _.where(res.data, { suserid: item });
                                if (resultant.length !== 0) {
                                    var obj = {
                                        sname: _.findWhere(data.data, { userid: item }).fullname,
                                        suserid: item,
                                        data: resultant,
                                    };
                                    rdata.push(obj);
                                }
                            });
                            response = {
                                data: rdata,
                                status: true,
                                code: 'S'
                            };
                            ctx.res.send(response);
                        });
                    });
                    // getdata(slist, ctx.args.data.fromdt, ctx.args.data.todt, function(result) {
                    //     len--;
                    //     if (result.data != 0) {
                    //         responseArr.push(result);
                    //     }
                    //     if (len == 0) {
                    //         response = {
                    //             data: responseArr,
                    //             status: true,
                    //             code: 'S'
                    //         }
                    //         ctx.res.send(response);
                    //     }
                    // });
                }
                // }
            } else {
                var qry = 'SELECT DISTINCT tbl_sale_bill.billid, billno, billtotal, paidamount, balamount, muser.companyname AS mcompany FROM tbl_order_dtl JOIN tbl_orders ON tbl_orders.ordid = tbl_order_dtl.ordid JOIN tbl_user AS muser ON muser.userid = tbl_orders.muserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_order_dtl.ordid WHERE tbl_sale_bill.billtotal > tbl_sale_bill.paidamount AND tbl_sale_bill.ordid IN (SELECT DISTINCT dtl.ordid FROM tbl_order_dtl as dtl where dtl.duserid=? AND dtl.ordstatus = "Invoiced") AND tbl_sale_bill.status = "Billed" AND SUBSTRING(tbl_sale_bill.billdt, 1, 10) BETWEEN ? AND ? order by mcompany';
                var qryarr = [ctx.args.data.duserid, ctx.args.data.fromdt, ctx.args.data.todt];
                common.executeQuery(qry, qryarr, function(res) {
                    res.code = 'A';
                    ctx.res.send(res);
                });
            }
        } catch (e) {
            log.info(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    };

    function getdata(slist, fromdt, todt, cb) {
        var responseArr = [];
        var query = 'SELECT fullname FROM tbl_user WHERE userid=?';
        var queryarr = [slist];
        common.executeQuery(query, queryarr, function(data) {
            var obj = {
                sname: data.data,
                suserid: slist
            };
            var qry = 'SELECT DISTINCT tbl_sale_bill.billid, billno, billtotal, paidamount, balamount, muser.companyname AS mcompany FROM tbl_order_dtl JOIN tbl_orders ON tbl_orders.ordid = tbl_order_dtl.ordid JOIN tbl_user AS muser ON muser.userid = tbl_orders.muserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_order_dtl.ordid WHERE tbl_sale_bill.billtotal > tbl_sale_bill.paidamount AND tbl_sale_bill.ordid IN (SELECT DISTINCT od.ordid FROM tbl_orders as od join tbl_order_dtl as dtl ON od.ordid = dtl.ordid where od.suserid =? AND dtl.ordstatus = "Invoiced") AND tbl_sale_bill.status = "Billed" AND SUBSTRING(tbl_sale_bill.billdt, 1, 10) BETWEEN ? AND ? order by mcompany';
            var qryarr = [slist, fromdt, todt];
            log.info(qryarr);
            common.executeQuery(qry, qryarr, function(res) {
                log.info(res);
                obj.data = res.data;
                cb(obj);
            });
        });
    }

    Tblsalebill.remoteMethod('smlist', {
        description: 'Get payment collection or based on salesman',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    /**
     * To update the payment amount based on the given amount
     * @method updatepayment
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    Tblsalebill.updatepayment = function(ctx, cb) {
        try {
            var response = {};
            log.info(ctx.args.data);
            Tblsalebill.find({
                where: {
                    billid: {
                        inq: ctx.args.data.bill_list
                    },
                },
                order: 'billid ASC',
            }, function(err, result) {
                if (err) {
                    log.error(err);
                } else {
                    var payamt = ctx.args.data.payamt;
                    var len = result.length;
                    for (var i = 0; i < result.length; i++) {
                        len--;
                        if (payamt > 0) {
                            var list = result[i];
                            if (list.billtotal > list.paidamount && list.balamount != 0) {
                                if (list.balamount != 0) {
                                    var balamount = list.balamount;
                                    if (balamount == payamt) {
                                        list.paidamount = list.paidamount + payamt;
                                        payamt = 0;
                                        list.balamount = list.billtotal - list.paidamount;
                                    } else if (payamt < balamount) {
                                        list.paidamount = list.paidamount + payamt;
                                        list.balamount = (list.billtotal - list.paidamount);
                                        payamt = 0;
                                    } else if (payamt > balamount) {
                                        payamt = payamt - (list.billtotal - list.paidamount);
                                        list.paidamount = list.billtotal;
                                        list.balamount = (list.billtotal - list.paidamount);
                                    }
                                    var updatelist = {
                                        balamount: list.balamount,
                                        paidamount: list.paidamount
                                    };
                                    if (list.balamount == 0) {
                                        updatelist.status = 'Billed';
                                    }
                                    Tblsalebill.update({ billid: list.billid }, updatelist, function(er, bill) {
                                        if (er) {
                                            log.error(er);
                                        } else {
                                            log.info(bill);
                                            console.log("up" + payamt);
                                            app.models.tbl_pymt_collectin.create({
                                                pymt_dt: new Date(),
                                                suserid: ctx.args.data.suserid,
                                                billid: list.billid,
                                                status: appmsg.STATUS_ACTIVE,
                                                last_updated_dt: new Date(),
                                                last_updated_by: ctx.args.data.suserid,
                                            }, function(err, result) {
                                                Tblsalebill.update({ billid: list.billid }, { pymtid: result.pymtid }, function(er, upbill) {
                                                    if (er) {
                                                        log.error(er);
                                                    } else {
                                                        log.info(upbill);
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            }
                        }
                        if (len == 0) {
                            response.status = true;
                            var msg = 'Thanks for making the payment of Rs ' + ctx.args.data.payamt + ' . Regards ' + ctx.args.data.dname;
                            response.message = "Payment updated successfully";
                            var smsResult = common.sendSMS(constants.smsgatewayuri,
                                constants.smsgatewayuser, constants.smsgatewaypassword,
                                constants.smsgatewaysenderID, ctx.args.data.mmobileno, msg);
                            console.log(response);
                            ctx.res.send(response);
                        }

                    }
                }

            })
        } catch (e) {
            log.info(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    };

    Tblsalebill.remoteMethod('updatepayment', {
        description: 'Update payment details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    // Response Message after FIND
    function modifyFindRequest(ctx, model, next) {
        try {
            console.log(ctx.req.query);
            //ctx.req.query.filter = JSON.parse(ctx.req.query.filter);
            console.log(ctx.req.query);
            if (ctx.req.query.mode == 'WEB') {
                ctx.req.query.filter.include = [{
                    relation: 'billdetails',
                    scope: {
                        include: [{
                            relation: "product",
                            scope: {
                                group: ['prodid']
                            },
                        }]
                    },
                }];
                //ctx.req.query.filter.include =['billdetails'];
                console.log(ctx.req.query);
                next();
            } else {
                next();
            }
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    Tblsalebill.afterRemote('create', modifySaveResponse);
    Tblsalebill.beforeRemote('create', modifySaveRequest);
    Tblsalebill.afterRemote('updateAttributes', modifyUpdateResponse);
    Tblsalebill.afterRemote('updateAll', modifyUpdateResponse);
    Tblsalebill.afterRemote('upsert', modifyUpdateResponse);
    Tblsalebill.afterRemote('find', modifyFindResponse);
    Tblsalebill.beforeRemote('find', modifyFindRequest);
    /**
     * To generate bills from the orders list
     * @method generatebills
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    Tblsalebill.generatebills = function(ctx, cb) {
        try {
            var serialNum = ctx.args.data.slobj;
            var len = ctx.args.data.length;
            var curr_seqno = serialNum.curr_seqno;
            var reply = { status: true, message: appmsg.SAVE_SUCCESSFULLY, data: [] };
            for (var i = 0; i < ctx.args.data.length; i++) {
                var hdr = ctx.args.data[i];
                var sno = serialNum.prefix_key + "" + serialNum.prefix_cncat +
                    "" + serialNum.suffix_key + "" +
                    serialNum.suffix_cncat + "" + curr_seqno;
                hdr.billno = sno;
                curr_seqno = curr_seqno + 1;
                Tblsalebill.create(hdr, function(err, result) {
                    reply.data.push(result.billid);
                    var salebilldtls = _.map(result.billdtls, function(item, index) {
                        item.billid = result.billid;
                        return item;
                    });
                    app.models.tbl_sale_bill_dtl.create(salebilldtls, function(err, response) {
                        log.info(response);
                        if (response != null && response != undefined) {
                            common.updateStock(salebilldtls, serialNum.duserid, function(result) {
                                log.info(result);
                            });
                        }
                        app.models.tbl_order_dtl.updateAll({ ordid: result.ordid, duserid: result.duserid }, { ordstatus: 'Invoiced' }, function(err, updated) {
                            log.info(updated);
                        });
                        len = len - 1;
                        if (len == 0) {
                            updateObj = {
                                slno_id: serialNum.slno_id,
                                curr_seqno: curr_seqno,
                                last_seqno: curr_seqno - 1,
                                last_updated_dt: hdr.lastupdateddt,
                                last_updated_by: hdr.lastupdatedby
                            }
                            app.models.tbl_slno_gen.upsert(updateObj, function(err, data) {
                                if (err) {
                                    log.error(err);
                                } else {
                                    log.info(data);
                                    log.info("serial number updated successfully");
                                }
                            });
                            ctx.res.send(reply);
                        }
                    });
                });
            }
        } catch (e) {
            log.info(e);
        }
    };

    Tblsalebill.remoteMethod('generatebills', {
        description: 'Update payment details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    Tblsalebill.beforeRemote('generatebills', modifySaveRequest);
};