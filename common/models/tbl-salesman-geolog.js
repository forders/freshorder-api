/**
 * @Filename : tbl-salesman-geolog.js
 * @Description : To write hooks salesman-geolog.
 * @Author : Nithya
 * @Date : Oct 06, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version Date 	Modified By 	Remarks 
 * 0.1 	   Oct 15 	Nithya 			Services added for date search
 */
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
module.exports = function(Tblsalesmangeolog) {
	//Response Message After Creating UlogID	
	function modifySaveResponse(ctx, model, next) {
		var status = ctx.res.statusCode;
		if (status && status === 200) {
			var response = {
				status : true,
				data:model.ulogid,
				message : appmsg.ID_CREATE_SUCCESS
			}
			log.info(filename + ">>modifySaveResponse>>"+response.message);
			ctx.res.status(status);
		} else if (status && status === 500) {
			var response = { 
				status : false,
				message : appmsg.INTERNAL_ERR
			}
			log.error(filename + ">>modifySaveResponse>>"+response.message);
			ctx.res.status(status);
		} else {
			var response = {
				status : false,
				message : appmsg.ID_CREATE_FAILED
			}
			log.error(filename + ">>modifySaveResponse>>"+response.message);
		}
		ctx.res.set('Content-Location', 'the internet');
		ctx.res.send(response);
		}
	// Response Message after UPDATE
		function modifyUpsertResponse(ctx, model, next) {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data:model.ulogid,
					message : appmsg.UPDATE_SUCCESSFULLY
				}
				log.info(filename + ">>modifyUpsertResponse>>"+response.message);
				ctx.res.status(status);
			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
				log.error(filename + ">>modifyUpsertResponse>>"+response.message);
			} else {
				var response = {
					status : false,
					message : appmsg.UPDATE_FAILED
				}
				log.error(filename + ">>modifyUpsertResponse>>"+response.message);
				ctx.res.status(status);
			}
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.send(response);
		}
		// Response Message after FIND
		function modifyFindResponse(ctx, model, next) {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				if (model.length === 0) {
					var response = {
						status : false,
						message : appmsg.LIST_NT_FOUND
					}
					log.info(filename + ">>modifyFindResponse>>"+response.message);
				} else {
					var response = {
						status : true,
						data : model,
						message : appmsg.LIST_FOUND
					}
					log.info(filename + ">>modifyFindResponse>>"+response.message);
				}

			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
				log.error(filename + ">>modifyFindResponse>>"+response.message);
			}
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		}
		// Find all instances by filtering the plan date .
		Tblsalesmangeolog.logs=function (ctx,cb){
			try{
				var response = {};
				log.info(filename+ ">>geologs>>");
				log.info(ctx.args.data);
				if (ctx.args.data.usertype === 'D' && ctx.args.data.orderplndt!=null && ctx.args.data.orderplndt!=undefined) {
					var query = 'SELECT g.geolat as lat,g.geolong as lng,g.suserid as gsuserid,g.updateddt as gupdateddt,g.logtime as glogtime,u.* FROM tbl_salesman_geolog as g JOIN tbl_user as u ON u.userid=g.suserid WHERE g.suserid=? AND SUBSTRING(g.updateddt,1,10)=? AND g.status= ? ORDER BY ulogid';
					var qryArr = [ctx.args.data.suserid,ctx.args.data.orderplndt,ctx.args.data.status];					
				}else {
					var qryArr = [ctx.args.data.suserid,ctx.args.data.status];
					var query = 'SELECT g.*,u.* FROM tbl_salesman_geolog as g JOIN tbl_user as u ON u.userid=g.suserid WHERE suserid=? ORDER BY ulogid;';
				}
				common.executeQuery(query,qryArr,function(result){
					if(result.data != 0){
						response.status = true;
						response.data = result.data;
						response.message = appmsg.LOGS_FOUND;
					}else if(result.data==0){
						response.status = false;
						response.message = appmsg.LOGS_NT_FOUND;
					}
					log.info(result);
					ctx.res.send(response);
				});
			}catch(e){
				console.log(e);
				response.status = false;
				response.message = appmsg.INTERNAL_ERR;
				ctx.res.send(response);
			}
			
		}
		Tblsalesmangeolog
				.remoteMethod(
						'logs',
						{
							description : "Find all instances by filtering the logs",
							accepts : [ {
								arg : 'ctx',
								type : 'object',
								http : {
									source : 'context'
								}
							}, {
								arg : 'data',
								type : 'object',
								http : {
									source : 'body'
								}
							} ],
							returns : {
								arg : 'message',
								type : 'object',
								root : true
							},
							http : {
								verb : 'post'
							}
						});
		Tblsalesmangeolog.afterRemote('create', modifySaveResponse);
		Tblsalesmangeolog.afterRemote('upsert', modifyUpsertResponse);
		Tblsalesmangeolog.afterRemote('find', modifyFindResponse);
		Tblsalesmangeolog.afterRemote('updateAll', modifyUpsertResponse);
};
