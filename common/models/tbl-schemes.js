/**
 * @Filename : tbl-schemes.js
 * @Description : To write hooks schemes.
 * @Author : Monisha
 * @Date : Dec 27, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version Date Modified By Remarks 0.1 Dec 27 Monisha Remote hooks added
 */

var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var config = require('../config/constants.js');
var common = require('../services/commonServices.js');

module.exports = function(Tblschemes) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.id,
					message : appmsg.SCHEME_SAVE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.SCHEME_SAVE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}

	// Response Message after UPDATE/UPSERT
	function modifyUpdateResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model,
					message : appmsg.SCHEME_UPDATE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.SCHEME_UPDATE_FAILED
				}
			}
			log.info(filename + ">>modifyUpdateResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}

	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				if (model.length === 0) {
					var response = {
						status : false,
						message : appmsg.LIST_NT_FOUND
					}
				} else {
					var response = {
						status : true,
						data : model,
						message : appmsg.LIST_FOUND
					}
				}
			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
			}
			log.info(filename + ">>modifyFindResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}

	function modifySaveRequest(ctx, model, next) {
		try{
			log.info(ctx.args.data);
			if(ctx.args.data.schemetype=='P'){
				var query = 'SELECT * FROM tbl_schemes where start_date=? and end_date=? and prodid=? and free_prodid=?';
				var qryArr = [ctx.args.data.start_date, ctx.args.data.end_date,
						ctx.args.data.prodid,ctx.args.data.discount];
			}else if(ctx.args.data.schemetype=='R'){
				var query = 'SELECT * FROM tbl_schemes where start_date=? and end_date=? and prodid=? and discount=?';
				var qryArr = [ctx.args.data.start_date, ctx.args.data.end_date,
						ctx.args.data.prodid,ctx.args.data.discount];
			}
			common.executeQuery(query, qryArr, function(result) {
				if (result.data == 0) {
					next();
				} else {
					var response = {
						status : false,
						message : appmsg.EXIST
					}
					ctx.res.send(response);
				}
			});
		}catch(e){
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	};
	
	/**
	 * To get the current schemes available for dealer
	 * @method schemes
	 * @param  {Object} ctx - Request object 
	 * @param  {Object} cb - Callback object
	 * @return {Object} response - Object contains status and message
	 */
	Tblschemes.schemes = function(ctx, cb) {
		try{
			var ds = Tblschemes.dataSource;
			response = {};  
			var dt = new Date();
			var dd = dt.getDate();  
			var yr = dt.getFullYear();
			var mm = dt.getMonth() + 1;
	        if (dd < 10) {
	            dd = '0' + dd;
	        }
	        if (mm < 10) {
	            mm = '0' + mm
	        }
			var cdt = yr + '-' + mm + '-' + dd;
			var qryArr = [ctx.args.data.duserid,cdt,cdt];
			var query = "SELECT * FROM tbl_schemes where duserid=? and substring(start_date,1,10)<=? and substring(end_date,1,10)>=? and status='Active'";

			ds.connector.execute(query, qryArr, function(err,list) {
				if (err) {
					console.error(err);
					response.status = true;
					response.message = appmsg.INTERNAL_ERR;
					log.error(filename + ">>schemes>>" + response.message);
					ctx.res.send(response);
				} else {
					if (list.length != 0) {
						response.status = true;
						response.message = appmsg.LIST_FOUND;
						response.data = list;
					} else{
						response.status = false;
						response.message = appmsg.LIST_NT_FOUND;						
					}
					log.info(filename + ">>schemes>>" + response.message);
					ctx.res.send(response);
				}
			});
		}catch(e){
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
		
	};
	Tblschemes
			.remoteMethod(
					'schemes',
					{
						description : 'Get the schemes data based on the query triggered',
						accepts : [{
							arg : 'ctx',
							type : 'object',
							http : {
								source : 'context'
							}
						}, {
							arg : 'data',
							type : 'object',
							http : {
								source : 'body'
							}
						}],
						returns : {
							arg : 'message',
							type : 'object',
							root : true
						},
						http : {
							verb : 'post'
						}
					});
	Tblschemes.beforeRemote('create', modifySaveRequest);
	Tblschemes.beforeRemote('create', modifySaveRequest);
	Tblschemes.afterRemote('create', modifySaveResponse);
	Tblschemes.afterRemote('updateAttributes', modifyUpdateResponse);
	Tblschemes.afterRemote('updateAll', modifyUpdateResponse);
	Tblschemes.afterRemote('upsert', modifyUpdateResponse);
	Tblschemes.afterRemote('find', modifyFindResponse);
};
