/**
 * @Filename : tbl-slno-gen.js
 * @Description : To write hooks slno-generation.
 * @Author : Monisha
 * @Date : Oct 22, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version  Date   Modified By   Remarks 
 * 0.1     Oct 22    Monisha    Remote hooks added
 */

var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(Tblslnogen) {
	// Response Message after CREATE
	function modifySaveResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model.slno_id,
					message : appmsg.SLNO_SAVE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.SLNO_SAVE_FAILED
				}
			}
			log.info(filename + ">>modifySaveResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}	
	
	// Response Message after UPDATE/UPSERT
	function modifyUpdateResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				var response = {
					status : true,
					data : model,
					message : appmsg.SLNO_UPDATE_SUCCESS
				}
			} else {
				var response = {
					status : false,
					message : appmsg.SLNO_UPDATE_FAILED
				}
			}
			log.info(filename + ">>modifyUpdateResponse>>" + response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
	// Response Message after FIND
	function modifyFindResponse(ctx, model, next) {
		try {
			var status = ctx.res.statusCode;
			if (status && status === 200) {
				if (model.length === 0) {
					var response = {
						status : false,
						message : appmsg.LIST_NT_FOUND
					}
				} else {
					var response = {
						status : true,
						data : model,
						message : appmsg.LIST_FOUND
					}
				}
			} else if (status && status === 500) {
				var response = {
					status : false,
					message : appmsg.INTERNAL_ERR
				}
			}
			log.info(filename+">>modifyFindResponse>>"+response.message);
			ctx.res.set('Content-Location', 'the internet');
			ctx.res.status(status);
			ctx.res.send(response);
		} catch (e) {
			log.error(e);
			var response = {
				status : false,
				data : e,
				message : appmsg.INTERNAL_ERR
			}
			ctx.res.send(response);
		}
	}
Tblslnogen.afterRemote('create', modifySaveResponse);
Tblslnogen.afterRemote('updateAttributes', modifyUpdateResponse);
Tblslnogen.afterRemote('updateAll', modifyUpdateResponse);
Tblslnogen.afterRemote('upsert', modifyUpdateResponse);
Tblslnogen.afterRemote('find', modifyFindResponse);	
};
