/**
 * @Filename : tbl-smroute-plan.js
 * @Description : To write hooks saleman route assignment.
 * @Author : Nithya
 * @Date : Oct 06, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version Date Modified By Remarks 
 * 0.1 Oct 06 Nithya Message change done
 * 0.2 Oct 13 Nithya RawQuery added for obtaining smrouteplan
 */
var app = require('../../server/server');
var path = require('path');
var fs = require('fs');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var constants = require('../config/constants.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(Tblsmrouteplan) {
    //Response Message After Creating Route Plan
    function modifySaveResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                data: model.planid,
                message: appmsg.SAVE_SUCCESSFULLY
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.status(status);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            console.log(model)
            log.error(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.status(status);
        } else {
            var response = {
                status: false,
                message: appmsg.SAVE_FAILED
            }
            log.error(filename + ">>modifySaveResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.send(response);
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                data: model.planid,
                message: appmsg.ROUTE_UPDATE_SUCCESS
            }
            log.info(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
        } else {
            var response = {
                status: false,
                message: appmsg.ROUTE_UPDATE_FAILED
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.send(response);
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            if (model.length === 0) {
                var response = {
                    status: false,
                    message: appmsg.ROUTE_NT_AVAIL
                }
                log.info(filename + ">>modifyFindResponse>>" + response.message);
            } else {
                var response = {
                    status: true,
                    data: model,
                    message: appmsg.LIST_FOUND
                }
                log.info(filename + ">>modifyFindResponse>>" + response.message);
            }

        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyFindResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }

    function modifyFindRequest(ctx, model, next) {
        console.log(ctx.req.query);
        if (ctx.req.query.mode == 'WEB') {
            ctx.req.query.filter = JSON.parse(ctx.req.query.filter);
        } else {
            next();
        }
    }
    // Find all instances by filtering the plan date .
    Tblsmrouteplan.smplans = function(ctx, cb) {
        try {
            var response = {};
            log.info(filename + ">>smplans>>");
            log.info(ctx.args.data);
            if (ctx.args.data.usertype === 'D' && ctx.args.data.mode == 'WEB' && ctx.args.data.orderplndt != undefined && ctx.args.data.orderplndt != null) {
                var query = 'SELECT sm.*,u.* FROM tbl_smroute_plan as sm JOIN tbl_user as u ON u.userid=sm.muserid WHERE sm.suserid=? AND SUBSTRING(sm.orderplndt,1,10)=? ORDER BY sm.planid';
                var qryArr = [ctx.args.data.suserid, ctx.args.data.orderplndt];
            } else if (ctx.args.data.usertype === 'D' && ctx.args.data.mode == 'MOB' && ctx.args.data.saleslist != undefined && ctx.args.data.saleslist != null) {
                var query = 'SELECT sm.*,m.fullname as mname,s.fullname as sname,m.companyname as mcompanyname,m.address as maddress,s.companyname as scompanyname FROM tbl_smroute_plan as sm JOIN tbl_user as m ON m.userid=sm.muserid JOIN tbl_user as s ON s.userid=sm.suserid WHERE sm.suserid in (?) AND sm.duserid=? AND SUBSTRING(sm.orderplndt,1,10) BETWEEN ? AND ? ORDER BY sm.planid';
                var qryArr = [ctx.args.data.saleslist, ctx.args.data.duserid, ctx.args.data.fromplndt, ctx.args.data.toplndt];
            } else if (ctx.args.data.usertype === 'D' && ctx.args.data.mode == 'MOB') {
                var qryArr = [ctx.args.data.duserid, ctx.args.data.fromplndt, ctx.args.data.toplndt];
                var query = 'SELECT sm.*,m.fullname as mname,s.fullname as sname,m.companyname as mcompanyname,m.address as maddress,s.companyname as scompanyname FROM tbl_smroute_plan as sm JOIN tbl_user as m ON m.userid=sm.muserid JOIN tbl_user as s ON s.userid=sm.suserid WHERE sm.duserid=? AND SUBSTRING(sm.orderplndt,1,10) BETWEEN ? AND ? ORDER BY sm.planid;';
            } else if (ctx.args.data.usertype === 'D' && ctx.args.data.mode == 'WEB' && ctx.args.data.eod == 'Y') {
                var qryArr = [ctx.args.data.duserid, ctx.args.data.fromredt, ctx.args.data.toredt];
                var query = 'SELECT tbl_smroute_plan.*, tbl_user.fullname, tbl_user.companyname FROM tbl_smroute_plan JOIN tbl_user ON tbl_user.userid = tbl_smroute_plan.suserid WHERE tbl_smroute_plan.duserid = ? AND status = "Submitted" AND SUBSTRING(tbl_smroute_plan.orderplndt,1,10) BETWEEN ? AND ? ORDER BY tbl_smroute_plan.planid ';
            } else if (ctx.args.data.suserid && ctx.args.data.usertype === 'S' && ctx.args.data.eoddt) {
                var qryArr = [ctx.args.data.suserid, ctx.args.data.eoddt];
                var query = 'SELECT * FROM tbl_smroute_plan WHERE tbl_smroute_plan.suserid = ? AND status = "Submitted" AND SUBSTRING(tbl_smroute_plan.orderplndt,1,10) =? ORDER BY tbl_smroute_plan.planid ';
            }
            console.log(qryArr);
            common.executeQuery(query, qryArr, function(result) {
                if (result.data != 0) {
                    response.status = true
                    response.data = result.data;
                    response.message = appmsg.LIST_FOUND;
                } else {
                    response.status = false;
                    response.message = appmsg.ROUTE_NT_AVAIL;
                }
                ctx.res.send(response);
            });
        } catch (e) {
            console.log(e);
            response.status = false;
            response.message = appmsg.INTERNAL_ERR;
            ctx.res.send(response);
        }

    }
    Tblsmrouteplan
        .remoteMethod(
            'smplans', {
                description: "Find all instances by filtering the plan date",
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });
    // Request Alteration before Save/Create
    function modifySaveRequest(ctx, model, next) {
        log.info(filename + ">>modifySaveObject>>");
        try {
            log.info(ctx.req.query);
            if (JSON.stringify(ctx.req.query) != '{}') {
                ctx.args.data = ctx.req.query;
                log.info(ctx.args.data);
                ctx.req.params.container = 'reports';
                app.dataSources.filestorage.connector.getFilename = function(file, req,
                    res) {
                    var origFilename = file.name;
                    var parts = origFilename.split('.'),
                        extension = parts[parts.length - 1];
                    var newFilename = common.generateOTP(4) + '-' +
                        common.generateOTP(2) + '.' + extension;
                    return newFilename;
                };
                var options = {};
                var response = {};
                app.models.files.upload(ctx.req, ctx.result, options, function(
                    err, fileObj) {
                    if (err) {
                        log.error(err);
                        response.status = false;
                        response.message = appmsg.INTERNAL_ERR;
                        ctx.res.send(response);
                    } else {
                        if (JSON.stringify(fileObj.files) != '{}') {
                            var fileInfo = fileObj.files.file[0];
                            ctx.args.data.exportfilenm = fileInfo.name;
                            next();
                        } else {
                            next();
                        }
                    }
                });
            } else {
                next();
            }
        } catch (e) {
            log.error(e);
            response.status = false;
            response.message = appmsg.INTERNAL_ERR;
            ctx.res.send(response);
        }
    }

    // Find all instances by filtering the plan date .
    Tblsmrouteplan.worktype = function(ctx, cb) {
        try {
            var response = {};
            log.info(filename + ">>worktype>>");
            fs.readFile(constants.WRKTYPFILE, 'utf8', function(err, data) {
                if (err) {
                    response.status = false;
                    response.message = appmsg.INTERNAL_ERR;
                    ctx.res.send(response);
                } else {
                    obj = JSON.parse(data);
                    response.status = true;
                    response.message = appmsg.LIST_FOUND;
                    response.data = obj;
                    ctx.res.send(response);
                }
            });
        } catch (e) {
            console.log(e);
            response.status = false;
            response.message = appmsg.INTERNAL_ERR;
            ctx.res.send(response);
        }

    }
    Tblsmrouteplan
        .remoteMethod(
            'worktype', {
                description: "Find all worktypes",
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });
    Tblsmrouteplan.beforeRemote('create', modifySaveRequest);
    Tblsmrouteplan.afterRemote('create', modifySaveResponse);
    Tblsmrouteplan.afterRemote('upsert', modifyUpsertResponse);
    Tblsmrouteplan.afterRemote('find', modifyFindResponse);
    Tblsmrouteplan.beforeRemote('find', modifyFindRequest);
    Tblsmrouteplan.afterRemote('updateAll', modifyUpsertResponse);
};