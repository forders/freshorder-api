/**
 * @Filename : tbl-stock-entry.js
 * @Description : To write hooks stock entry.
 * @Author : Monisha
 * @Date : Oct 22, 2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version  Date   Modified By   Remarks 
 * 0.1     Oct 22    Monisha    Remote hooks added
 * 0.2	   Jan 08 	 Nithya		Product stock data updated after a stock entry  
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var common = require('../services/commonServices.js');
module.exports = function(Tblstockentry) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.stockid,
                    message: appmsg.STOCK_SAVE_SUCCESS
                };
                var product = {
                    prodid: model.prodid,
                    stock: model.closing,
                    updateddt: model.updateddt
                };
                if (model.userid !== 0) {
                    app.models.TblUserProd.upsert(product, function(err, result) {
                        log.info(result);
                    });
                }
            } else {
                var response = {
                    status: false,
                    message: appmsg.STOCK_SAVE_FAILED
                };
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    // Response Message after UPDATE/UPSERT	
    function modifyUpdateResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model,
                    message: appmsg.STOCK_UPDATE_SUCCESS
                };
                var product = {
                    prodid: model.prodid,
                    stock: model.closing,
                    updateddt: model.updateddt
                };
                app.models.TblUserProd.upsert(product, function(err, result) {
                    console.log(result);
                });
            } else {
                var response = {
                    status: false,
                    message: appmsg.STOCK_UPDATE_FAILED
                };
            }
            log.info(filename + ">>modifyUpdateResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                if (model.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.LIST_NT_FOUND
                    };
                } else {
                    var response = {
                        status: true,
                        data: model,
                        message: appmsg.LIST_FOUND
                    };
                }
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                };
            }
            log.info(filename + ">>modifyFindResponse>>" + response.message);
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    // Request Message before save
    function modifySaveRequest(ctx, model, next) {
        try {
            log.info(ctx.args.data);
            if (ctx.args.data.stockarr != undefined) {
                ctx.args.data = ctx.args.data.stockarr;
                next();
            } else {
                app.models.tbl_stock_entry.findOne({
                        where: { prodid: ctx.args.data.prodid, userid: ctx.args.data.userid, islatest: 'Y' }
                    },
                    function(err, result) {
                        if (result != null) {
                            app.models.tbl_stock_entry.updateAll({ stockid: result.stockid, userid: ctx.args.data.userid }, { islatest: 'N' }, function(err, updated) {
                                log.info(err);
                                log.info(updated);
                                if (updated != null) {
                                    next();
                                } else {
                                    var response = {
                                        status: false,
                                        message: appmsg.INTERNAL_ERR
                                    };
                                    ctx.res.send(response);
                                }
                            });
                        } else {
                            next();
                        }
                    });
            }
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            };
            ctx.res.send(response);
        }
    }
    Tblstockentry.beforeRemote('create', modifySaveRequest);
    Tblstockentry.afterRemote('create', modifySaveResponse);
    Tblstockentry.afterRemote('updateAttributes', modifyUpdateResponse);
    Tblstockentry.beforeRemote('updateAttributes', modifySaveRequest);
    Tblstockentry.beforeRemote('updateAll', modifySaveRequest);
    Tblstockentry.afterRemote('updateAll', modifyUpdateResponse);
    Tblstockentry.beforeRemote('upsert', modifySaveRequest);
    Tblstockentry.afterRemote('upsert', modifyUpdateResponse);
    Tblstockentry.afterRemote('find', modifyFindResponse);
};