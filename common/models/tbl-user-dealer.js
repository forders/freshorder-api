/**
 * @Filename : tbl-user-dealer.js
 * @Description : To write hooks for the user related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 * 0.3		Nov 08	Preethi			Message changes in dealer's list(modifyFindResponse)
 */
var path = require('path');
var app = require('../../server/server');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
module.exports = function(TblUserDealer) {
    // Response Message before CREATE
    function modifySaveRequest(ctx, model, next) {
        log.info(ctx.args.data);
        try {
            TblUserDealer.findOne({
                where: {
                    muserid: ctx.args.data.muserid,
                    duserid: ctx.args.data.duserid
                }
            }, function(err, dealer) {
                log.info(dealer);
                if (dealer) {
                    var obj = {};
                    obj = dealer;
                    obj.dstatus = ctx.args.data.dstatus;
                    TblUserDealer.updateAll({ usrdlrid: obj.usrdlrid, duserid: obj.duserid }, { muserid: obj.muserid, dstatus: ctx.args.data.dstatus }, function(err, result) {
                        if (err) {
                            var response = {
                                status: false,
                                message: appmsg.INTERNAL_ERR
                            }
                            ctx.res.send(response);
                        } else {
                            var response = {
                                status: true,
                                message: appmsg.DEALER_SAVE_SUCCESS
                            }
                            ctx.res.send(response);
                        }
                    });

                } else {
                    next();
                }
            });
        } catch (e) {
            log.error(e);
            var response = {
                status: true,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }

    }
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                message: appmsg.DEALER_SAVE_SUCCESS
            }
            log.info(filename + ">>modifySaveResponse>>" + response.message);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifySaveResponse>>" + response.message);
        } else {
            var response = {
                status: false,
                message: appmsg.DEALER_SAVE_FAILED
            }
            log.error(filename + ">>modifySaveResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                message: appmsg.DEALER_UPDATE_SUCCESS
            }
            log.info(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        } else {
            var response = {
                status: false,
                message: appmsg.DEALER_UPDATE_FAILED
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            ctx.res.status(status);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.send(response);
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        var status = ctx.res.statusCode;

        if (status && status === 200) {
            if (model.length === 0) {
                var response = {
                    status: false,
                    message: appmsg.DLIST_NT_FOUND
                }
                log.info(filename + ">>modifyFindResponse>>" +
                    response.message);
            } else {
                var response = {
                    status: true,
                    data: model,
                    message: appmsg.LIST_FOUND
                }
                log.info(filename + ">>modifyFindResponse>>" +
                    response.message);
            }

        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyFindResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Request Alteration before FIND
    function modifyFindObject(ctx, model, next) {
        if (ctx.req.query.filter.where != undefined) {
            if (ctx.req.query.filter.where.muserid && ctx.req.query.prodname) {
                ctx.req.query.filter.include = [{
                    relation: "products",
                    scope: {
                        where: {
                            prodname: {
                                like: "%" + ctx.req.query.prodname + "%"
                            }
                        },
                        order: ['display_order asc', 'prodid asc']
                    }
                }]
            }
            if (ctx.req.query.filter.where.muserid) {
                ctx.req.query.filter.where.dstatus = appmsg.STATUS_ACTIVE;
                if (ctx.req.query.prod_dt != null) {
                    ctx.req.query.filter.include = [{
                        relation: "D",
                        scope: {
                            where: {
                                updateddt: {
                                    gt: ctx.req.query.prod_dt
                                }
                            },
                            fields: ['userid', 'mobileno',
                                'fullname', 'address',
                                'companyname'
                            ]
                        },

                    }, ];
                } else {
                    var search = {
                        relation: "products",
                        scope: {
                            where: {
                                prodstatus: ctx.req.query.prodstatus
                            },
                            order: ['display_order asc', 'prodid asc']
                        }
                    }
                    var stocks = {
                        relation: "stocks",
                        scope: {
                            fields: ['batchno', 'manufdt', 'expirydt']
                        }
                    };
                    if (ctx.req.query.prodstatus != undefined &&
                        ctx.req.query.prodstatus != null) {
                        search.scope.where = {
                            prodstatus: ctx.req.query.prodstatus
                        };
                    }
                    ctx.req.query.filter.include = [{
                        relation: "D",
                        scope: {
                            fields: ['userid', 'mobileno', 'fullname',
                                'address', 'companyname'
                            ]
                        }
                    }];
                    ctx.req.query.filter.include.push(stocks);
                    ctx.req.query.filter.include.push(search);
                }
            }
        }
        next();
    }
    // Dealer's dashboard - calculates sales count of the individual saleman's
    TblUserDealer.dashboard = function(ctx, cb) {
        try {
            var response = {};
            log.info(filename + ">>dashboard>>");
            var cdt = common.convertDate(new Date());
            log.info(ctx.args.data);
            if (ctx.args.data.usertype === 'D' && ctx.args.data.mode == 'WEB') {
                var weekdate = new Date((new Date()).valueOf() - 7 * (1000 * 60 * 60 * 24));
                var monthdate = new Date((new Date()).valueOf() - 30 * (1000 * 60 * 60 * 24));
                var weekdt = common.convertDate(weekdate);
                var monthdt = common.convertDate(monthdate);
                var qryArr = [cdt, ctx.args.data.duserid];
                var weekdata = [weekdt, cdt, ctx.args.data.duserid];
                var monthdata = [monthdt, cdt, ctx.args.data.duserid];
                var todaySales = 'SELECT tbl_user.fullname, SUM(ROUND(tbl_sale_bill.billtotal, 2)) AS today,suserid FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.orderdt, 1, 10) = ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                var weekSales = 'SELECT tbl_user.fullname, SUM(ROUND(tbl_sale_bill.billtotal, 2)) AS week,suserid FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.orderdt,1,10) BETWEEN ? AND ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                var monthSales = 'SELECT tbl_user.fullname, SUM(ROUND(tbl_sale_bill.billtotal, 2)) AS month,suserid FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.orderdt,1,10) BETWEEN ? AND ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                var todayCollection = 'SELECT tbl_user.fullname, SUM(ROUND(tbl_sale_bill.paidamount, 2)) AS t_collection,suserid FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.last_updated_dt, 1, 10) = ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                var weekCollection = 'SELECT tbl_user.fullname, SUM(ROUND(tbl_sale_bill.paidamount, 2)) AS w_collection,suserid FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.last_updated_dt,1,10) BETWEEN ? AND ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                var monthCollection = 'SELECT tbl_user.fullname, SUM(ROUND(tbl_sale_bill.paidamount, 2)) AS m_collection,suserid FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.last_updated_dt,1,10) BETWEEN ? AND ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                common.executeQuery(todaySales, qryArr, function(todaysales) {
                    var response = {};
                    response.status = true;
                    response.todaysales = todaysales.data;
                    common.executeQuery(weekSales, weekdata, function(weeksales) {
                        response.weeksales = weeksales.data;
                        common.executeQuery(monthSales, monthdata, function(monthsales) {
                            response.monthsales = monthsales.data;
                        });
                        common.executeQuery(todayCollection, qryArr, function(todaycollection) {
                            response.todayCollection = todaycollection.data;
                            common.executeQuery(weekCollection, weekdata, function(weekcollection) {
                                response.weekCollection = weekcollection.data;
                                common.executeQuery(monthCollection, monthdata, function(monthcollection) {
                                    response.monthCollection = monthcollection.data;
                                    ctx.res.send(response);
                                });
                            });
                        });
                    });
                });
            } else if (ctx.args.data.usertype === 'D' && ctx.args.data.mode == 'MOB') {
                var qryArr = [cdt, ctx.args.data.duserid];
                var query = 'SELECT tbl_user.fullname, SUM(tbl_sale_bill.billtotal) AS billtotal, FROM tbl_orders JOIN tbl_user ON tbl_user.userid = tbl_orders.suserid JOIN tbl_sale_bill ON tbl_sale_bill.ordid = tbl_orders.ordid WHERE SUBSTRING(tbl_sale_bill.orderdt, 1, 10) = ? AND tbl_orders.suserid IN (SELECT muserid FROM tbl_user_dealer WHERE duserid = ?) GROUP BY tbl_orders.suserid;';
                common.executeQuery(query, qryArr, function(result) {
                    if (result.status = true) {
                        response.status = true;
                        response.todaysales = result.data;
                        response.message = appmsg.LIST_FOUND;
                    } else {
                        response.status = false;
                        response.todaysales = 0;
                        response.message = appmsg.ORDER_LIST_NTFOUND;
                    }
                    ctx.res.send(response);
                });
            }
        } catch (e) {
            console.log(e);
            response.status = false;
            response.message = appmsg.INTERNAL_ERR;
            ctx.res.send(response);
        }

    }
    TblUserDealer
        .remoteMethod(
            'dashboard', {
                description: "Get ordertotals for requested dealer's individual salesman",
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });


    // Bulk update/save dealer beat
    TblUserDealer.batchPut = function(ctx, cb) {
        try {
            log.info(ctx.args.data);
            if (ctx.args.data.dealerlist) {
                var dealerlist = ctx.args.data.dealerlist;
                var len = dealerlist.length;
                for (var i = 0; i < dealerlist.length; i++) {
                    TblUserDealer.upsert(dealerlist[i], function(err, beat) {
                        if (err) {
                            var response = {
                                status: false,
                                message: appmsg.INTERNAL_ERR
                            }
                            log.error(filename + ">>batchPut>>");
                            ctx.res.status(500);
                            ctx.res.send(response);
                        } else {
                            len--;
                            if (len == 0) {
                                try {
                                    if (ctx.args.data.removelist) {
                                        if (ctx.args.data.removelist.length != 0) {
                                            for (var j = 0; j < ctx.args.data.removelist.length; j++) {
                                                console.log(ctx.args.data.removelist[j]);
                                                try {
                                                    var deleteqry = "DELETE FROM tbl_user_dealer WHERE usrdlrid=?";
                                                    var qryStg = [ctx.args.data.removelist[j]];
                                                    var ds = TblUserDealer.dataSource;
                                                    ds.connector.execute(deleteqry, qryStg, function(err, dealers) {
                                                        if (err) {
                                                            log.error(err);
                                                        } else {
                                                            log.info(dealers);
                                                        }

                                                    });
                                                } catch (e) {
                                                    log.info(e);
                                                }
                                            }
                                        }
                                    }
                                } catch (ex) {
                                    var response = {
                                        status: false,
                                        message: appmsg.INTERNAL_ERR
                                    }
                                    log.error(filename + ">>batchPut>>");
                                    ctx.res.status(500);
                                    ctx.res.send(response);
                                }
                                var response = {
                                    status: true,
                                    message: appmsg.SAVE_SUCCESSFULLY
                                }
                                log.info(filename + ">>batchPut>>");
                                ctx.res.status(200);
                                ctx.res.send(response);
                            }
                        }
                    });
                }
            } else {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>batchPut>>");
                ctx.res.status(500);
                ctx.res.send(response);
            }
        } catch (e) {
            console.log(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>batchPut>>");
            ctx.res.status(500);
            ctx.res.send(response);
        }

    };
    TblUserDealer.remoteMethod('batchPut', {
        description: ' Bulk update or save beat details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });


    TblUserDealer.salesmandealers = function(ctx, cb) {
        try {
            var response = {};
            var ds = TblUserDealer.dataSource;
            console.log(ctx.args.data);
            if (ctx.args.data.usertype === 'D') {
                var query = "select d.*,sm.fullname as smname,dis.fullname as dname from tbl_user_dealer as d join tbl_user as sm on sm.userid=d.duserid join tbl_user as dis on dis.userid=d.muserid where d.duserid in (select u.userid from tbl_user_dealer ud, tbl_user u where ud.muserid = u.userid and u.usertype = 'S' and ud.duserid=? and u.userstatus='Active' and ud.dstatus='Active')";
                var qryStg = [ctx.args.data.duserid];
                ds.connector.execute(query, qryStg, function(err, dealers) {
                    if (err) {
                        console.error(err);
                        response.status = true;
                        response.message = appmsg.INTERNAL_ERR;
                        ctx.res.send(response);
                    } else {
                        if (dealers.length != 0) {
                            response.status = true;
                            response.message = appmsg.LIST_FOUND;
                            response.data = dealers;
                        } else {
                            response.status = false;
                            response.message = appmsg.DORDER_NT_FOUND;
                        }
                        ctx.res.send(response);
                    }

                });
            } else {
                response.status = false;
                response.message = appmsg.LIST_NTFOUND;
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblUserDealer.remoteMethod('salesmandealers', {
        description: 'Salesman distributor list',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    TblUserDealer.beforeRemote('create', modifySaveRequest);
    TblUserDealer.afterRemote('create', modifySaveResponse);
    TblUserDealer.afterRemote('upsert', modifyUpsertResponse);
    TblUserDealer.afterRemote('updateAll', modifyUpsertResponse);
    TblUserDealer.afterRemote('find', modifyFindResponse);
    TblUserDealer.beforeRemote('find', modifyFindObject);
};