/**
 * @Filename : tbl-user-prod.js
 * @Description : To write hooks for the products related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 */
var app = require('../../server/server');
var path = require('path');
var filename = path.basename(__filename);
var appmsg = require('../config/messages.js');
var log = require('../config/logger.js').logger;
var config = require('../config/constants.js');
var common = require('../services/commonServices.js');
var csv = require("fast-csv");
var fs = require("fs");
module.exports = function (TblUserProd) {
    /**
     * To Upload CSV file contains product details
     * @method import
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUserProd.import = function (ctx, options, cb) {
        if (!options)
            options = {};
        ctx.req.params.container = 'products';
        var i = 0;
        var end = false;
        try {
            log.info(ctx.req.query);
            app.models.files.upload(ctx.req, ctx.result, options,
                function (err, fileObj) {
                    if (err) {
                        ctx.res.send(response);
                    } else {
                        var dataList = [];
                        if (JSON.stringify(fileObj.files) != '{}') {
                            var fileInfo = fileObj.files.file[0];
                            filepath = config.PRODUCTS + fileInfo.name;
                            var stream = fs.createReadStream(filepath);
                            var csvStream = csv()
                                .on("data", function (data) {
                                    dataList.push(data);
                                }).on("end", function () {
                                    var end = true;
                                    for (var i = 0; i < dataList.length; i++) {
                                        if (i != 0) {
                                            createProduct(dataList[i]);
                                        }
                                    }
                                    var response = {
                                        status: true,
                                        message: appmsg.PROD_SAVE_SUCCESS
                                    }
                                    log.info(filename + ">>upload>>" + response.message);
                                    ctx.res.send(response.message);
                                });
                            stream.pipe(csvStream);
                            try {
                                function createProduct(prodobj) {
                                    log.info(prodobj);
                                    var product = {
                                        userid: ctx.req.query.userid,
                                        importfile: fileInfo.name,
                                        prodname: prodobj[0],
                                        prodcode: prodobj[1],
                                        hsncode: prodobj[2],
                                        prodprice: prodobj[3],
                                        prodtax: prodobj[4],
                                        margin_percent: prodobj[5],
                                        mrp: prodobj[6],
                                        uom: prodobj[7],
                                        unitperuom: prodobj[8],
                                        display_order: prodobj[9],
                                        prodstatus: appmsg.STATUS_ACTIVE,
                                        updateddt: new Date()
                                    }
                                    if (product.prodprice != '' && product.margin_percent != '') {
                                        product.unitprice = (parseFloat(prodobj[3])) + (parseFloat(prodobj[3]) * (parseFloat(prodobj[5]) / 100));
                                    }if(product.margin_percent == '' || product.margin_percent <=0){
                                        product.unitprice = product.prodprice;
                                    } if(product.uom == '' && product.unitperuom == ''){
                                        product.uom = "Kilogram(kg)";
                                        product.unitperuom = 1;
                                    }if(product.uom == '' && product.unitperuom != ''){
                                        product.uom = "Kilogram(kg)";
                                    }if(product.uom != '' && product.unitperuom == ''){
                                        product.unitperuom = 1;
                                    }if (prodobj[0].length > 50 || prodobj[1].length > 50) {
                                        product.importrmrk = appmsg.MAXLENGTH_EXCEED;
                                    }
                                    log.info(product);
                                    TblUserProd.findOne({
                                        where: {
                                            userid: product.userid,
                                            prodcode: product.prodcode,
                                            prodname: product.prodname
                                        }
                                    }, function (err, products) {
                                        log.info(products);
                                        if(products!=null){
                                            product.prodid = products.prodid;
                                        }
                                        TblUserProd.upsert(product, function (err, result) {
                                            log.info(result);
                                            if (result != null)
                                                var uom = {
                                                    duserid: ctx.req.query.userid,
                                                    prodid: result.prodid,
                                                    uomname: result.uom,
                                                    uomvalue: result.unitperuom,
                                                    isdefault: 1,
                                                    status: appmsg.STATUS_ACTIVE,
                                                    updateddt: new Date(),
                                                    updatedby: ctx.req.query.userid,
                                                }
                                            app.models.tbl_prod_uom.upsert(uom, function (err, uresult) {
                                                log.info(uresult);
                                            });
                                        });
                                    });
                                }
                            } catch (ex) {
                                log.info(ex);
                            }
                        } else {
                            var response = {
                                status: true,
                                message: appmsg.PROD_SAVE_FAILED
                            }
                            log.info(filename + ">>upload>>" + response.message);
                            ctx.res.send(response.message);
                        }
                    }
                });
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            log.error(filename + ">>upload>>" + response.message);
        }

    };
    TblUserProd.remoteMethod('import', {
        description: 'Upload CSV file contains product details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'options',
            type: 'object',
            http: {
                source: 'query'

            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'fileObject',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        try {
            log.info(ctx.args.data);
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.prodid,
                    message: appmsg.PROD_SAVE_SUCCESS
                }
                log.info(filename + ">>modifySaveResponse>>" + response.message);
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifySaveResponse>>" + response.message);
            }

            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var response = {
                    status: true,
                    data: model.prodid,
                    message: appmsg.PROD_UPDATE_SUCCESS
                }
                log.info(filename + ">>modifyUpsertResponse>>" + response.message);
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifyUpsertResponse>>" + response.message);
            }
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    // Request Alteration before UpdateAll
    function modifyUpdateAllObject(ctx, model, next) {
        try {
            ctx.req.query.where = {
                prodid: ctx.args.data.prodid
            };
            log.info(filename + ">>modifyUpdateAllObject>>");
            next();
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    // Request Alteration before File upload
    function modifyFileObject(ctx, model, next) {
        try {
            log.info(filename + ">>modifyFileObject>>");
            app.dataSources.filestorage.connector.getFilename = function (file, req,
                res) {
                var origFilename = file.name;
                var parts = origFilename.split('.'),
                    extension = parts[parts.length - 1];
                var newFilename = common.generateOTP(4) + '-' +
                    common.generateOTP(2) + '.' + extension;
                return newFilename;
            };
            log.info(filename + ">>modifyFileObject>>");
            next();
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                if (model.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.PRODUCTS_LIST_NTFOUND
                    }
                    log.info(filename + ">>modifyFindResponse>>" + response.message);
                } else {
                    var response = {
                        status: true,
                        data: model,
                        message: appmsg.LIST_FOUND
                    }
                    log.info(filename + ">>modifyFindResponse>>" + response.message);
                }

            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifyFindResponse>>" + response.message);
            }
            ctx.res.set('Content-Location', 'the internet');
            ctx.res.status(status);
            ctx.res.send(response);
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.status(500);
            ctx.res.send(response);
        }
    }
    /**
     * To get the product list
     * @method productlist
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUserProd.productlist = function (ctx, cb) {
        try {
            var ds = TblUserProd.dataSource;
            response = {};
            log.info(ctx.args.data);
            if (ctx.args.data.status == undefined) {
                ctx.args.data.status = 'Active';
            }
            if (ctx.args.data.usertype == 'D') {
                if (ctx.args.data.duserid != null && ctx.args.data.duserid != undefined && ctx.args.data.duserid != '') {
                    var query = "SELECT p.*,p.prodid as id FROM tbl_user_prod as p WHERE p.userid=? and p.prodstatus=? order by display_order,prodid asc";
                    var paramArr = [ctx.args.data.duserid, ctx.args.data.status];
                } else {
                    var reply = {
                        status: false,
                        message: appmsg.VALID_DEALERID
                    }
                    ctx.res.send(response);
                }
            } else if (ctx.args.data.prod_dt != null && ctx.args.data.prod_dt != undefined) {
                if (ctx.args.data.userid != null && ctx.args.data.userid != undefined && ctx.args.data.userid != '') {
                    var query = "SELECT u.companyname, up . *,s.* FROM tbl_user_prod up LEFT JOIN tbl_stock_entry s ON s.prodid = up.prodid JOIN tbl_user u ON u.userid = up.userid WHERE up.updateddt>? and  up.userid IN (SELECT duserid FROM tbl_user_dealer WHERE muserid = ? AND dstatus = ?); and up.prodstatus=? order by display_order,prodid asc";
                    var paramArr = [ctx.args.data.prod_dt, ctx.args.data.userid, ctx.args.data.status, ctx.args.data.status];
                } else {
                    var reply = {
                        status: false,
                        message: appmsg.VALID_USERID
                    }
                    ctx.res.send(response);
                }
            } else if (ctx.args.data.prod_dt == undefined) {
                if (ctx.args.data.userid != null && ctx.args.data.userid != undefined && ctx.args.data.userid != '') {
                    var query = "SELECT u.companyname, up.prodid,up.userid,up.prodcode,up.prodname,up.unitprice as prodprice,(prodtax) as prodtax,up.importfile,up.importrmrk,up.prodstatus,up.mrp,up.uom,up.stock,up.unitsgm,up.unitperuom,up.display_order,up.margin_percent,up.updateddt FROM tbl_user_prod up LEFT JOIN tbl_user u ON u.userid = up.userid WHERE up.userid IN (SELECT duserid FROM tbl_user_dealer WHERE muserid = ? AND dstatus = ?) and up.prodstatus=? order by display_order,prodid asc;";
                    var paramArr = [ctx.args.data.userid, ctx.args.data.status, ctx.args.data.status];
                } else {
                    var reply = {
                        status: false,
                        message: appmsg.VALID_USERID
                    }
                    ctx.res.send(response);
                }
            }
            ds.connector.execute(query, paramArr, function (err, productlist) {
                if (err) {
                    log.error(err);
                    response.status = true;
                    response.message = appmsg.INTERNAL_ERR;
                    log.error(filename + ">>productlist>>" + response.message);
                    ctx.res.send(response);
                } else {
                    if (productlist.length != 0) {
                        response.status = true;
                        response.message = appmsg.LIST_FOUND;
                        response.data = productlist;
                    } else {
                        response.status = false;
                        response.message = appmsg.LIST_NT_FOUND;
                    }
                    log.info(filename + ">>productlist>>" + response.message);
                    ctx.res.send(response);
                }
            });
        } catch (err) {
            log.error(err);
            response.status = true;
            response.message = appmsg.INTERNAL_ERR;
            log.error(filename + ">>productlist>>" + response.message);
            ctx.res.send(response);
        }

    };
    TblUserProd.remoteMethod('productlist', {
        description: 'Get the product list based on the query triggered',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    function modifySaveRequest(ctx, model, next) {
        try {
            log.info(ctx.args.data);
            if (ctx.args.data.flag == 'I') {
                var query = 'SELECT * FROM tbl_user_prod where prodname=? and prodcode=? and userid=?';
                var qryArr = [ctx.args.data.prodname, ctx.args.data.prodcode, ctx.args.data.userid]
                common.executeQuery(query, qryArr, function (result) {
                    if (result.data == 0) {
                        next();
                    } else {
                        var response = {
                            status: false,
                            message: appmsg.PROD_AVAIL
                        }
                        ctx.res.send(response);
                    }
                })
            } else {
                next();
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNALERR
            }
            ctx.res.send(response);
        }
    };
    // Bulk update/save beat
    TblUserProd.createorupdate = function (ctx, cb) {
        try {
            log.info(ctx.args.data);
            var product = ctx.args.data.product;
            log.info(product);
            TblUserProd.upsert(product, function (err, prod) {
                if (err) {
                    var response = {
                        status: false,
                        message: appmsg.INTERNAL_ERR
                    }
                    log.error(filename + ">>batchPut>>");
                    ctx.res.status(500);
                    ctx.res.send(response);
                } else {
                    var uom = ctx.args.data.uom;
                    if (uom != undefined && uom.length != 0) {
                        var len = uom.length;
                        for (var i = 0; i < uom.length; i++) {
                            uom[i].prodid = prod.prodid;
                            if (uom[i].flag == 'I' || uom[i].uomid) {
                                app.models.tbl_prod_uom.upsert(uom[i], function (err, uom) {
                                    if (err) {
                                        var response = {
                                            status: false,
                                            message: appmsg.INTERNAL_ERR
                                        }
                                        log.error(err);
                                        ctx.res.status(500);
                                        ctx.res.send(response);
                                    } else {
                                        len--;
                                        if (len == 0) {
                                            var response = {
                                                status: true,
                                                message: appmsg.SAVE_SUCCESSFULLY
                                            }
                                            log.info(filename + ">>batchPut>>");
                                            ctx.res.status(200);
                                            ctx.res.send(response);
                                        }
                                    }
                                });
                            } else if (uom[i].flag == 'R') {
                                app.models.tbl_prod_uom.deleteById(uom[i].uomid, function (err, uomid) {
                                    if (err) {
                                        var response = {
                                            status: false,
                                            message: appmsg.INTERNAL_ERR
                                        }
                                        log.error(err);
                                        ctx.res.status(500);
                                        ctx.res.send(response);
                                    } else {
                                        len--;
                                        if (len == 0) {
                                            var response = {
                                                status: true,
                                                message: appmsg.SAVE_SUCCESSFULLY
                                            }
                                            log.info(filename + ">>batchPut>>");
                                            ctx.res.status(200);
                                            ctx.res.send(response);
                                        }
                                    }
                                });
                            } else {
                                len--;
                                if (len == 0) {
                                    var response = {
                                        status: true,
                                        message: appmsg.SAVE_SUCCESSFULLY
                                    }
                                    log.info(filename + ">>batchPut>>");
                                    ctx.res.status(200);
                                    ctx.res.send(response);
                                }
                            }
                        }
                    } else {
                        var response = {
                            status: true,
                            message: appmsg.SAVE_SUCCESSFULLY
                        }
                        log.info(filename + ">>batchPut>>");
                        ctx.res.status(200);
                        ctx.res.send(response);
                    }
                }
            });
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>batchPut>>");
            ctx.res.status(500);
            ctx.res.send(response);
        }

    };
    TblUserProd.remoteMethod('createorupdate', {
        description: ' Bulk update or save product details',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    TblUserProd.afterRemote('create', modifySaveResponse);
    TblUserProd.beforeRemote('create', modifySaveRequest);
    TblUserProd.beforeRemote('createorupdate', modifySaveRequest);
    TblUserProd.afterRemote('upsert', modifyUpsertResponse);
    TblUserProd.afterRemote('find', modifyFindResponse);
    TblUserProd.afterRemote('updateAll', modifyUpsertResponse);
    TblUserProd.beforeRemote('updateAll', modifyUpdateAllObject);
    TblUserProd.beforeRemote('import', modifyFileObject);
};