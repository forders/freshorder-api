/**
 * @Filename : tbl-user-js
 * @Description : To write hooks for the user related operation.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Nov 05  Nithya			Comments added
 * 0.3		Nov 08  Nithya			Customer save changes added
 */
var app = require('../../server/server');
var appmsg = require('../config/messages.js');
var constants = require('../config/constants.js');
var common = require('../services/commonServices.js');
var log = require('../config/logger.js').logger;
var path = require('path');
var fs = require('fs');
var csv = require("fast-csv");
var filename = path.basename(__filename);
var schedule = require('node-schedule');
var _ = require('underscore');

var validation = require('../validation/custom-validation');

module.exports = function(TblUser) {
    // Response Message after CREATE
    function modifySaveResponse(ctx, model, next) {
        try {
            var status = ctx.res.statusCode;
            if (status && status === 200) {
                var msg = constants.OTP_MSG + " " + model.otp;
                log.info(ctx.args.data);
                if (ctx.args.data.mode == 'WEB' && model.usertype == 'S') {
                    var smsResult = common.sendSMS(constants.smsgatewayuri,
                        constants.smsgatewayuser, constants.smsgatewaypassword,
                        constants.smsgatewaysenderID, model.mobileno, msg);
                    var pwd = constants.PWD_MSG + " " + common.decrypt(model.password);
                    var smsPwd = common.sendSMS(constants.smsgatewayuri,
                        constants.smsgatewayuser, constants.smsgatewaypassword,
                        constants.smsgatewaysenderID, model.mobileno, pwd);
                } else if (ctx.args.data.mode != 'WEB' && ctx.args.data.usertype != 'C') {
                    var smsResult = common.sendSMS(constants.smsgatewayuri,
                        constants.smsgatewayuser, constants.smsgatewaypassword,
                        constants.smsgatewaysenderID, model.mobileno, msg);
                }
                var response = {
                    status: true,
                    data: {
                        userid: model.userid,
                        usertype: model.usertype,
                        fullname: model.fullname,
                        pymttenure: model.pymttenure,
                        mobileno: model.mobileno,
                        email: model.emailid,
                        profilestatus: 'N'
                    },
                    message: appmsg.USER_SAVE_SUCCESS
                }
                if (ctx.args.data.mode == 'WEB') {
                    if (model.usertype == 'S') {
                        response.message = appmsg.SM_SAVE_SUCCESS;
                    } else {
                        response.message = appmsg.M_SAVE_SUCCESS;
                    }
                    app.models.tbl_user_dealer.create({ muserid: model.userid, duserid: ctx.args.data.duserid, dstatus: appmsg.STATUS_ACTIVE }, function(err, dealer) {
                        if (err) {
                            log.error(err);
                        } else {
                            log.info(dealer);
                        }
                    });
                }
                if (ctx.args.data.usertype == 'C' || ctx.args.data.usertype == 'M') {
                    log.info(ctx.args.data.slobj);
                    app.models.tbl_slno_gen.upsert(ctx.args.data.slobj, function(err, data) {
                        if (err) {
                            log.error(err);
                        } else {
                            log.info(data);
                            log.info("serial number updated successfully");
                        }
                    });
                }
                log.info(filename + ">>modifySaveResponse>>" + response.message);
                ctx.res.set('Content-Location', 'the internet');
                ctx.res.status(status);
                ctx.res.send(response);
            } else if (status && status === 500) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>modifySaveResponse>>" + response.message);
                ctx.res.set('Content-Location', 'the internet');
                ctx.res.status(status);
                ctx.res.send(response);
            }
        } catch (ex) {
            log.error(ex);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    }
    // Response Message after UPDATE
    function modifyUpsertResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                data: model,
                message: appmsg.PROFILE_SUCCESS
            }
            log.info(filename + ">>modifyUpsertResponse>>" + response.message);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyUpsertResponse>>" + response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Response Message after FIND
    function modifyFindResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            if (model.length === 0) {
                var response = {
                    status: false,
                    message: appmsg.LIST_NT_FOUND
                }
            } else {
                var response = {
                    status: true,
                    data: model,
                    message: appmsg.LIST_FOUND
                }
            }
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Response Message after UPDATEALL
    function modifyUpdateAllResponse(ctx, model, next) {
        var status = ctx.res.statusCode;
        if (status && status === 200) {
            var response = {
                status: true,
                message: appmsg.PROFILE_SUCCESS
            }
            log.info(filename + ">>modifyUpdateAllResponse>>" +
                response.message);
        } else if (status && status === 500) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(filename + ">>modifyUpdateAllResponse>>" +
                response.message);
        } else {
            var response = {
                status: false,
                message: appmsg.PROFILE_FAILED
            }
            log.error(filename + ">>modifyUpdateAllResponse>>" +
                response.message);
        }
        ctx.res.set('Content-Location', 'the internet');
        ctx.res.status(status);
        ctx.res.send(response);
    }
    // Request Alteration before SAVE
    function modifySaveObject(ctx, model, next) {
        try {
            condition = {};
            condition.mobileno = ctx.args.data.mobileno;
            console.log(condition);
            TblUser.find({ where: condition }, function(err, obj) {
                if (obj.length !== 0) {
                    var response = {
                        status: false,
                        exist: 'Y',
                        data: obj,
                        message: appmsg.EXISTING_USER
                    }
                    if (ctx.args.data.mode == 'WEB' && ctx.args.data.usertype == 'M') {
                        response.message = appmsg.EXISTING_MERCHANT;
                    } else if (ctx.args.data.mode == 'WEB' && ctx.args.data.usertype == 'S') {
                        response.message = appmsg.EXISTING_SALESMAN;
                    }
                    ctx.res.send(response);
                } else if (err) {
                    var response = {
                        status: false,
                        message: appmsg.INTERNAL_ERR
                    }
                    log.error(filename + ">>modifySaveObject>>" + response.message);
                    ctx.res.send(response);
                } else {
                    ctx.args.data.otp = common.generateOTP(4);
                    ctx.args.data.otpstatus = appmsg.STATUS_PENDING;
                    ctx.args.data.pymtstatus = appmsg.STATUS_PENDING;
                    if (ctx.args.data.mode == 'WEB') {
                        ctx.args.data.password = common.encrypt(common.generateOTP(6));
                        ctx.args.data.userstatus = appmsg.STATUS_ACTIVE;
                    } else if (ctx.args.data.usertype != 'C') {
                        ctx.args.data.password = common.encrypt(ctx.args.data.password);
                        ctx.args.data.userstatus = appmsg.STATUS_INACTIVE;
                    }
                    if (ctx.args.data.usertype == 'C' || ctx.args.data.usertype == 'M') {
                        ctx.args.data.userstatus = appmsg.STATUS_ACTIVE;
                        if (ctx.args.data.usertype == 'M') {
                            var serialnum = {
                                duserid: ctx.args.data.duserid,
                                ref_key: constants.PARTYCODE,
                                status: appmsg.STATUS_ACTIVE
                            };
                        } else {
                            var serialnum = {
                                duserid: ctx.args.data.duserid,
                                ref_key: constants.CUSCODE,
                                status: appmsg.STATUS_ACTIVE
                            };
                        }
                        log.info(serialnum);
                        app.models.tbl_slno_gen.findOne({
                            where: serialnum
                        }, function(err, result) {
                            log.info(result);
                            if (err) {
                                log.error(err);
                            } else if (result != null) {
                                var sno = result.prefix_key + "" + result.prefix_cncat +
                                    "" + result.suffix_key + "" +
                                    result.suffix_cncat + "" + result.curr_seqno;
                                log.info(sno);
                                if (ctx.args.data.usertype == 'M') {
                                    ctx.args.data.partycd = sno;
                                } else {
                                    ctx.args.data.cust_code = sno;
                                }
                                var slobj = {
                                    slno_id: result.slno_id,
                                    curr_seqno: result.curr_seqno + 1,
                                    last_seqno: result.curr_seqno,
                                    last_updated_dt: new Date(),
                                    last_updated_by: ctx.args.data.fullname
                                }
                                ctx.args.data.slobj = slobj;
                                log.info(slobj);
                                next();
                            } else {
                                if (ctx.args.data.usertype == 'M' || ctx.args.data.usertype == 'C') {

                                    var slobj = {
                                        duserid: ctx.args.data.duserid,
                                        ref_key: serialnum.ref_key,
                                        curr_seqno: 1,
                                        last_seqno: 0,
                                        status: appmsg.STATUS_ACTIVE,
                                        last_updated_dt: new Date(),
                                        last_updated_by: ctx.args.data.fullname
                                    }
                                    if (ctx.args.data.usertype == 'M') {
                                        slobj.prefix_key = 'PARTY';
                                        slobj.prefix_cncat = '-';
                                        slobj.suffix_key = 'CODE';
                                        slobj.suffix_cncat = '-';
                                        slobj.ref_key = constants.PARTYCODE;
                                    } else {
                                        slobj.prefix_key = 'CUS';
                                        slobj.prefix_cncat = '-';
                                        slobj.suffix_key = 'CODE';
                                        slobj.suffix_cncat = '-'
                                    }
                                    app.models.tbl_slno_gen.create(slobj, function(err, serialnumber) {
                                        if (err) {
                                            next();
                                        } else if (serialnumber) {
                                            var sno = serialnumber.prefix_key + "" + serialnumber.prefix_cncat +
                                                "" + serialnumber.suffix_key + "" +
                                                serialnumber.suffix_cncat + "" + serialnumber.curr_seqno;
                                            log.info(sno);
                                            if (ctx.args.data.usertype == 'M') {
                                                ctx.args.data.partycd = sno;
                                            } else {
                                                ctx.args.data.cust_code = sno;
                                            }
                                            var updateobj = {
                                                slno_id: serialnumber.slno_id,
                                                curr_seqno: serialnumber.curr_seqno + 1,
                                                last_seqno: serialnumber.curr_seqno + 1,
                                                last_updated_dt: new Date(),
                                                last_updated_by: ctx.args.data.fullname
                                            }
                                            ctx.args.data.slobj = updateobj;
                                            next();
                                        }
                                    });
                                } else {
                                    next();
                                }
                            }
                        });
                    } else {
                        next();
                    }
                }
            });
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            ctx.res.send(response);
        }
    }
    // Request Alteration before FIND
    function modifyFindObject(ctx, model, next) {
        if (ctx.args.filter) {
            var filter = {};
            filter = ctx.args.filter;
            console.log(typeof(filter.where.password));
            if (typeof(filter.where.password) == typeof(1)) {
                var pwd = filter.where.password.toString();
            } else {
                var pwd = filter.where.password;
            }
            filter.where.password = common.encrypt(pwd);
            ctx.args.filter = filter;
        }
        log.info(filename + ">>modifyFindObject>>");
        next();
    }
    // Request Alteration before UpdateAll
    function modifyUpdateAllObject(ctx, model, next) {
        ctx.req.query.where = {
            userid: ctx.args.data.userid
        };
        log.info(filename + ">>modifyUpdateAllObject>>");
        next();
    }
    /**
     * To send otp for the requested mobile number
     * @method resendotp
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.resendotp = function(ctx, cb) {
        try {
            TblUser.find({
                where: {
                    mobileno: ctx.args.data.mobileno
                }
            }, function(err, obj) {
                if (obj.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.NOT_USER
                    }
                    log.info(filename + ">>resendotp>>" + response.message);
                    ctx.res.send(response);
                } else if (err) {
                    var response = {
                        status: false,
                        message: appmsg.INTERNAL_ERR
                    }
                    log.error(filename + ">>resendotp>>" + response.message);
                    ctx.res.send(response);
                } else {
                    var otp = common.generateOTP(4);
                    var msg = constants.OTP_MSG + " " + otp;
                    var smsResult = common.sendSMS(constants.smsgatewayuri,
                        constants.smsgatewayuser, constants.smsgatewaypassword,
                        constants.smsgatewaysenderID, ctx.args.data.mobileno,
                        msg);
                    if (smsResult === true) {
                        TblUser.upsert({
                            userid: obj[0].userid,
                            otp: otp,
                            otpstatus: appmsg.STATUS_PENDING,
                            userstatus: appmsg.STATUS_INACTIVE,
                        }, function(err, obj) {
                            if (err) {
                                var response = {
                                    status: false,
                                    message: appmsg.INTERNAL_ERR
                                }
                                log.error(filename + ">>resendotp>>" +
                                    response.message);
                                ctx.res.send(response);
                            } else {
                                var response = {
                                    status: true,
                                    message: appmsg.OTP_SENT
                                }
                                log.info(filename + ">>resendotp>>" +
                                    response.message);
                                ctx.res.send(response);
                            }
                        });
                    } else {
                        var response = {
                            status: false,
                            message: appmsg.INTERNAL_ERR
                        }
                        log.error(filename + ">>resendotp>>" + response.message);
                        ctx.res.send(response);
                    }
                }
            });
        } catch (ex) {
            log.error(ex);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblUser.remoteMethod('resendotp', {
        description: 'Send otp to the requested mobile number if user exists',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    /**
     * To change the otp status for the requested mobilno
     * @method verifyotp
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.verifyotp = function(ctx, cb) {
        TblUser.find({
            where: {
                mobileno: ctx.args.data.mobileno,
                otp: ctx.args.data.otp
            }
        }, function(err, obj) {
            if (obj.length === 0) {
                var response = {
                    status: false,
                    message: appmsg.OTP_NT_VERIFIED
                }
                log.info(filename + ">>verifyotp>>" + response.message);
                ctx.res.send(response);
            } else if (err) {
                var response = {
                    status: false,
                    message: appmsg.INTERNAL_ERR
                }
                log.error(filename + ">>verifyotp>>" + response.message);
                ctx.res.send(response);
            } else {
                TblUser.upsert({
                        userid: obj[0].userid,
                        otpstatus: appmsg.STATUS_VERIFIED,
                        userstatus: appmsg.STATUS_ACTIVE,
                        nextpymtdt: new Date(),
                        lastpymtdt: new Date(),
                    },
                    function(err, obj) {
                        if (err) {
                            var response = {
                                status: false,
                                message: appmsg.INTERNAL_ERR
                            }
                            log.error(filename + ">>verifyotp>>" +
                                response.message);
                            ctx.res.send(response);
                        } else {
                            var response = {
                                status: true,
                                message: appmsg.OTP_VERIFIED
                            }
                            log.info(filename + ">>verifyotp>>" +
                                response.message);
                            ctx.res.send(response);
                        }
                    });
            }
        });
    };

    TblUser
        .remoteMethod(
            'verifyotp', {
                description: 'Verify otp to the requested mobile number if user exists',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });
    /**
     * To save the profile picture on to server and file name in user table
     * @method upload
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.upload = function(ctx, options, data, cb) {
        try {
            if (!options)
                options = {};
            ctx.req.params.container = 'profile';
            var userObj = {
                userid: ctx.req.query.userid
            }
            TblUser.app.models.files.upload(ctx.req, ctx.result, options, function(
                err, fileObj) {
                if (err) {
                    cb(err);
                    log.error(filename + ">>upload>>");
                } else {
                    var fileInfo = fileObj.files.file[0];
                    userObj.profileimg = fileInfo.name;
                    TblUser.upsert(userObj, function(err, obj) {
                        if (err !== null) {
                            var response = {
                                status: false,
                                message: appmsg.PROFILE_FAILED
                            }
                            log.error(filename + ">>upload>>" + response.message);
                            cb(null, response.message);
                        } else {
                            var response = {
                                data: obj.id,
                                message: appmsg.PROFILE_SUCCESS,
                                status: true
                            }
                            log.info(filename + ">>upload>>" + response.message);
                            cb(null, response.message);
                        }
                    });
                }
            });
        } catch (ex) {
            log.error(ex);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }


    };
    TblUser.remoteMethod('upload', {
        description: 'Uploads a profile picture of the user',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'options',
            type: 'string',
            http: {
                source: 'query'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'fileObject',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    // Request Alteration before File upload
    function modifyUploadbject(ctx, model, next) {
        var response = {};
        try {
            app.dataSources.filestorage.connector.getFilename = function(file, req,
                res) {
                var origFilename = file.name;
                var parts = origFilename.split('.'),
                    extension = parts[parts.length - 1];
                var newFilename = common.generateOTP(4) + '-' +
                    common.generateOTP(2) + '.' + extension;
                return newFilename;
            };
            log.info(filename + ">>modifyFileObject>>");
            next();
        } catch (ex) {
            validation.generateAppError(ex, response, ctx.res);
        }

    }
    // Request Alteration before File upload
    function modifyFileObject(ctx, model, next) {
        var response = {};
        try {
            app.dataSources.filestorage.connector.getFilename = function(file, req,
                res) {
                var origFilename = file.name;
                var parts = origFilename.split('.'),
                    extension = parts[parts.length - 1];
                var newFilename = common.generateOTP(4) + '-' +
                    common.generateOTP(2) + '.' + extension;
                return newFilename;
            };
            var serialnum = {
                duserid: ctx.req.query.duserid,
                ref_key: constants.PARTYCODE,
                status: appmsg.STATUS_ACTIVE
            };
            log.info(serialnum);
            app.models.tbl_slno_gen.findOne({
                where: serialnum
            }, function(err, result) {
                log.info(result);
                if (err) {
                    log.error(err);
                    next();
                } else if (result != null) {
                    ctx.req.query.slobj = result;
                    log.info(slobj);
                    next();
                } else {
                    var slobj = {
                        duserid: ctx.req.query.duserid,
                        ref_key: appmsg.PARTYCODE,
                        curr_seqno: 1,
                        last_seqno: 0,
                        status: appmsg.STATUS_ACTIVE,
                        last_updated_dt: new Date(),
                        last_updated_by: ctx.args.data.fullname
                    }
                    slobj.prefix_key = 'PARTY';
                    slobj.prefix_cncat = '-';
                    slobj.suffix_key = 'CODE';
                    slobj.suffix_cncat = '-';
                    slobj.ref_key = constants.PARTYCODE;
                    app.models.tbl_slno_gen.create(slobj, function(err, serialnumber) {
                        if (err) {
                            next();
                        } else if (serialnumber) {
                            ctx.req.query.slobj = serialnumber;
                            next();
                        }
                    });
                }
            });
            // log.info(filename + ">>modifyFileObject>>");
            // next();
        } catch (ex) {
            validation.generateAppError(ex, response, ctx.res);
        }

    }

    /**
     * To send password of the requested mobileno
     * @method forgetpassword
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.forgetpassword = function(ctx, cb) {
        var response = {};
        try {
            validation.isMandatoryString(ctx.args.data.mobileno, 'Mobile No', ctx.res, 1, 10);
            TblUser.find({
                where: {
                    mobileno: ctx.args.data.mobileno,
                }
            }, function(err, user) {
                if (user.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.NOT_USER
                    }
                    log.info(filename + ">>forgetpassword>>" + response.message);
                    ctx.res.send(response);
                } else if (err) {
                    var response = {
                        status: false,
                        message: appmsg.INTERNAL_ERR
                    }
                    log.error(filename + ">>forgetpassword>>" + response.message);
                    ctx.res.send(response);
                } else {
                    var usrpwd = common.decrypt(user[0].password);
                    console.log(usrpwd);
                    var response = {};
                    var msg = constants.PWD_MSG + " " + usrpwd;
                    var smsResult = common.sendSMS(constants.smsgatewayuri,
                        constants.smsgatewayuser, constants.smsgatewaypassword,
                        constants.smsgatewaysenderID, ctx.args.data.mobileno, msg);
                    if (smsResult === true) {
                        response = {
                            status: true,
                            message: appmsg.PWD_SENT_SUCCESS
                        }
                    } else {
                        response = {
                            status: false,
                            message: appmsg.PWD_SENT_FAIL
                        }
                    }
                    log.info(filename + ">>forgetpassword>>");
                    ctx.res.send(response);
                }
            });
        } catch (ex) {
            validation.generateAppError(ex, response, ctx.res);
        }

    };
    TblUser
        .remoteMethod(
            'forgetpassword', {
                description: 'Send Password message to the requested mobile number if user exists',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });

    /**
     * To change password of the requested user 
     * @method changepassword
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.changepassword = function(ctx, cb) {
        var response = {};
        try {
            //validation.isMandatoryString(ctx.args.data.mobileno, 'Mobile No', ctx.res, 1, 10);
            validation.isMandatoryString(ctx.args.data.userid, 'User id', ctx.res, 1, 10);
            validation.isMandatoryString(ctx.args.data.oldpassword, 'Old password', ctx.res, 1, 10);
            validation.isMandatoryString(ctx.args.data.newpassword, 'New password', ctx.res, 3, 10);
            TblUser.find({
                where: {
                    //mobileno: ctx.args.data.mobileno,
                    userid: ctx.args.data.userid,
                }
            }, function(err, user) {
                if (user.length == 0) {
                    var response = {
                        status: false,
                        message: appmsg.NOT_USER
                    }
                    log.info(filename + ">>changepassword>>" + response.message);
                    ctx.res.send(response);
                } else if (err) {
                    var response = {
                        status: false,
                        message: appmsg.PWD_CHANGE_FAILED
                    }
                    log.error(filename + ">>changepassword>>" + response.message);
                    ctx.res.send(response);
                } else {
                    var usrpwd = common.decrypt(user[0].password);
                    var newpwd = common.encrypt(ctx.args.data.newpassword);
                    if (ctx.args.data.oldpassword === usrpwd) {
                        TblUser.upsert({
                            userid: user[0].userid,
                            mobileno: ctx.args.data.mobileno,
                            password: newpwd,
                        }, function(err, obj) {
                            if (err) {
                                var response = {
                                    status: false,
                                    message: appmsg.PWD_CHANGE_FAILED
                                }
                                log.error(filename + ">>changepassword>>" +
                                    response.message);

                                ctx.res.send(response);
                            } else if (obj) {
                                var response = {
                                    status: true,
                                    message: appmsg.PWD_CHANGED
                                }
                                log.info(filename + ">>changepassword>>" +
                                    response.message);

                                ctx.res.send(response);
                            }
                        });
                    } else {
                        var response = {
                            status: false,
                            message: appmsg.INVALID_PWD
                        }

                        ctx.res.send(response);
                    }
                }
            });
        } catch (ex) {
            validation.generateAppError(ex, response, ctx.res);
        }
    };
    TblUser
        .remoteMethod(
            'changepassword', {
                description: 'Change the password to the requested mobile number if user exists',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });
    /**
     * To get the dealers list of the logged in user
     * @method dealerlist
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.dealerlist = function(ctx, cb) {
        try {
            TblUser.find({
                where: {
                    usertype: "D",
                    userstatus: appmsg.STATUS_ACTIVE,
                    userid: { neq: ctx.args.data.userid }
                },
                fields: ['userid', 'mobileno', 'companyname', 'address', 'usertype', 'fullname']
            }, function(err, user) {
                if (user.length === 0) {
                    var response = {
                        status: false,
                        message: appmsg.DLIST_NT_FOUND
                    }
                    log.info(filename + ">>dealerlist>>" + response.message);
                    ctx.res.send(response);
                } else if (err) {
                    var response = {
                        status: false,
                        message: appmsg.INTERNAL_ERR
                    }
                    log.error(filename + ">>dealerlist>>" + response.message);
                    log.error(err);
                    ctx.res.send(response);
                } else {
                    app.models.TblUserDealer.find({
                        where: {
                            muserid: ctx.args.data.userid,
                            dstatus: appmsg.STATUS_ACTIVE
                        },
                        fields: ['duserid']
                    }, function(err, dealers) {
                        if (err) {
                            var response = {
                                status: false,
                                message: appmsg.INTERNAL_ERR
                            }
                            log.error(filename + ">>dealerlist>>" + response.message);
                            ctx.res.send(response);
                        } else {
                            var myDiff = common.makeSymmDiffFunc(function(x, y) {
                                return x.userid === y.duserid;
                            });
                            var result = myDiff(user, dealers);
                            if (result.length == 0) {
                                var response = {
                                    status: false,
                                    message: appmsg.DEALERS_EMPTY
                                }
                            } else {
                                var response = {
                                    status: true,
                                    data: result,
                                    message: appmsg.LIST_FOUND
                                }
                            }
                            log.info(filename + ">>dealerlist>>" + response.message);
                            ctx.res.send(response);
                        }
                    });
                }
            });
        } catch (e) {
            log.error(e);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblUser
        .remoteMethod(
            'dealerlist', {
                description: 'Get the dealer list based on the query triggered',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });

    var rule = new schedule.RecurrenceRule();
    rule.minute = 5;
    rule.hour = 12;
    var j = schedule.scheduleJob(rule, function() {
        //sendnotification();
        log.info(filename + ">>sendtriggered>>");
    });
    /**
     * Check dues and Send SMS to remaind about payment subscription in regular basis
     * @method sendnotification
     */
    function sendnotification() {
        try {
            TblUser.find({ where: { userstatus: appmsg.STATUS_ACTIVE } }, function(err, users) {
                if (err) {
                    log.error(err);
                } else {
                    log.info(filename + ">>send>>");
                    for (var i = 0; i < users.length; i++) {
                        var date2 = new Date(users[i].nextpymtdt);
                        var NextpymtDt = date2.getDate() + '-' + date2.getMonth() + '-' + date2.getFullYear();
                        console.log(NextpymtDt);
                        if (users[i].gcmid != null) {
                            var message = "Your next payment dues on" + NextpymtDt;
                            var date1 = new Date();
                            var date2 = new Date(users[i].nextpymtdt);
                            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            var diffMonths = (date2.getFullYear() - date1.getFullYear()) * 12;
                            var NextpymtDt = date2.getDate() + '-' + date2.getMonth() + '-' + date2.getFullYear();
                            console.log(NextpymtDt);
                            //	console.log("diffDays"+diffDays);
                            //	console.log(users[i].pymttenure);
                            if (users[i].pymttenure === appmsg.PYMT_MONTHLY) {
                                if ((diffDays - 3) >= 27) {
                                    console.log(appmsg.PYMT_MONTHLY + "send notification");
                                    common.pushNotifcation(message, users[i].gcmid);
                                } else if (diffDays == 30) {
                                    //	common.pushNotifcation(message,ctx.args.data.regId);
                                    updateUser(users[i].userid);
                                    console.log(appmsg.PYMT_MONTHLY + "Dont send response");
                                }
                            } else if (users[i].pymttenure === appmsg.PYMT_YEARLY) {
                                if ((diffDays - 3) >= 362 && diffMonths === 12) {
                                    console.log(appmsg.PYMT_YEARLY + "send notification");
                                    common.pushNotifcation(message, users[i].gcmid);
                                } else if (diffDays === 365) {
                                    updateUser(users[i].userid);
                                    console.log(appmsg.PYMT_YEARLY + "Dont send response");
                                }
                            } else if (users[i].pymttenure === appmsg.PYMT_QUARTERLY) {
                                if ((diffDays - 3) >= 87 && diffMonths === 3) {
                                    console.log(appmsg.PYMT_QUARTERLY + "send notification");
                                    common.pushNotifcation(message, users[i].gcmid);
                                } else if (diffDays === 90) {
                                    updateUser(users[i].userid);
                                    console.log(appmsg.PYMT_QUARTERLY + "Dont send response");
                                }
                            } else {
                                if ((diffDays - 3) >= 57 && diffMonths === 6) {
                                    console.log(appmsg.PYMT_QUARTERLY + "send notification");
                                    common.pushNotifcation(message, users[i].gcmid);
                                } else if (diffDays === 90) {
                                    updateUser(users[i].userid);
                                    console.log(appmsg.PYMT_QUARTERLY + "Dont send response");
                                }
                            }
                        }
                    }
                }
            });

            function updateUser(id) {
                app.models.TblUser.upsert({ userid: id, pymtstatus: appmsg.STATUS_PENDING });
                log.info("User Status updated");
            }

        } catch (ex) {
            log.error(ex);
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);

        }
    };

    /**
     * To get the merchants/salesman list of the logged in user
     * @method merchantlist
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.merchantlist = function(ctx, cb) {
        var ds = TblUser.dataSource;
        response = {};
        log.info(ctx.args.data);
        if (ctx.args.data.status != null && ctx.args.data.status != undefined) {
            var status = ctx.args.data.status;
        } else {
            var status = appmsg.STATUS_ACTIVE;
        }
        var qryArr = [ctx.args.data.userid, status, status];
        if (ctx.args.data.usertype == "S") {
            if (ctx.args.data.prod_dt != null &&
                ctx.args.data.prod_dt != undefined) {
                var qryArr = [ctx.args.data.userid, ctx.args.data.prod_dt, status, status];
                var query = "select u.* from tbl_user_dealer ud, tbl_user u where ud.muserid = u.userid and u.usertype = 'M' and ud.duserid in (select duserid from tbl_user_dealer where muserid = ? and dstatus=?) and u.updateddt>? and u.userstatus=?";
            } else {
                if (ctx.args.data.beat_type == 'D') {
                    var qryArr = [ctx.args.data.userid, status, status, status];
                    var query = "select u.* from tbl_beat b , tbl_user u where u.userid=b.muserid and u.usertype='M' and b.suserid=? and b.status =? and u.userstatus=?";
                } else {
                    //var query = "SELECT u.* FROM tbl_user u WHERE u.userid IN (SELECT DISTINCT bt.muserid FROM tbl_beat b, tbl_beat bt, tbl_user_dealer d WHERE bt.muserid IS NOT NULL AND bt.id = b.id AND b.duserid = d.muserid AND d.duserid = ? AND d.dstatus = ?) AND u.userstatus = ?";
                    var query="select u.* from (select distinct muserid from tbl_beat where id in(select distinct(id) from tbl_beat where duserid in (select muserid FROM freshorders.tbl_user_dealer where duserid=? and dstatus=?)) and muserid is not null) as `tt` Left Join tbl_user u on u.userid=tt.muserid where u.userstatus = ?";
                    var qryArr = [ctx.args.data.userid, status, status, status];
                }
            }
        } else if (ctx.args.data.usertype == "D" && ctx.args.data.saleslist == "N" && ctx.args.data.mode != "WEB") {
            var query = "select u.*,ud.usrdlrid from tbl_user_dealer ud, tbl_user u where ud.muserid = u.userid and u.usertype = 'M' and ud.duserid=? and u.userstatus=? and ud.dstatus=?";
        } else if (ctx.args.data.saleslist == "Y" && ctx.args.data.usertype == "D" && ctx.args.data.mode != "WEB") {
            var query = "select u.*,ud.usrdlrid from tbl_user_dealer ud, tbl_user u where ud.muserid = u.userid and u.usertype = 'S' and ud.duserid=? and ud.dstatus=?";
        } else if (ctx.args.data.usertype == "D" && ctx.args.data.saleslist == "N" && ctx.args.data.mode == "WEB") {
            var query = "select u.*,u.userid as id,ud.usrdlrid from tbl_user_dealer ud, tbl_user u where ud.muserid = u.userid and u.usertype = 'M' and ud.duserid=? and u.userstatus=? and ud.dstatus=?";
        } else if (ctx.args.data.saleslist == "Y" && ctx.args.data.usertype == "D" && ctx.args.data.mode == "WEB") {
            var query = "select u.*,u.userid as id,ud.usrdlrid from tbl_user_dealer ud, tbl_user u where ud.muserid = u.userid and u.usertype = 'S' and ud.duserid=? and u.userstatus=? and ud.dstatus=?";
        }

        ds.connector.execute(query, qryArr, function(err, merchantlist, saleslist) {
            if (err) {
                console.error(err);
                response.status = true;
                response.message = appmsg.INTERNAL_ERR;
                log.error(filename + ">>merchantlist>>" + response.message);
                ctx.res.send(response);
            } else {
                if (merchantlist.length != 0) {
                    response.status = true;
                    response.message = appmsg.LIST_FOUND;
                    response.count = merchantlist.length;
                    response.data = _.sortBy(merchantlist, 'partycd');
                } else {
                    response.status = false;
                    if (ctx.args.data.saleslist == "N") {
                        response.message = appmsg.MLIST_NT_FOUND;
                    } else {
                        response.message = appmsg.SLIST_NT_FOUND;
                    }

                }
                log.info(filename + ">>merchantlist>>" + response.message);
                ctx.res.send(response);
            }
        });
    };
    TblUser
        .remoteMethod(
            'merchantlist', {
                description: 'Get the merchant list based on the query triggered',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });
    /**
     * To Import Dealer's merchant/customer List from CSV File
     * @method import
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.import = function(ctx, options, cb) {
        if (!options)
            options = {};
        log.info(ctx.req.query);
        ctx.req.params.container = 'customer';
        var curr_seqno = ctx.req.query.slobj.curr_seqno;
        var serialnumber = ctx.req.query.slobj;
        try {
            app.models.files.upload(ctx.req, ctx.result, options, function(err, fileObj) {
                if (err) {
                    console.log(err);
                    ctx.res.send(err);
                } else {
                    var dataList = [];
                    if (JSON.stringify(fileObj.files) != '{}') {
                        var fileInfo = fileObj.files.file[0];
                        filepath = constants.CUSTOMER_PATH + fileInfo.name;
                        var stream = fs.createReadStream(filepath);
                        var csvStream = csv()
                            .on("data", function(data) {
                                dataList.push(data);
                            })
                            .on("end", function() {
                                for (var i = 0; i < dataList.length; i++) {
                                    if (i != 0) {
                                        createUser(dataList[i], i);
                                    }
                                    if (i == dataList.length - 1) {
                                        log.info(ctx.req.query.slobj);
                                        var updateobj = {
                                            slno_id: serialnumber.slno_id,
                                            curr_seqno: serialnumber.curr_seqno + (dataList.length - 1),
                                            last_seqno: serialnumber.curr_seqno + (dataList.length - 2),
                                            last_updated_dt: new Date(),
                                            last_updated_by: ctx.req.query
                                        }
                                        app.models.tbl_slno_gen.upsert(updateobj, function(err, data) {
                                            if (err) {
                                                log.error(err);
                                            } else {
                                                log.info(data);
                                                log.info("serial number updated successfully");
                                            }
                                        });
                                    }
                                }
                                var response = {
                                    status: true,
                                    message: appmsg.SAVE_SUCCESSFULLY
                                }
                                log.info(filename + ">>import>>" + response.message);
                                ctx.res.send(response);
                            });
                        stream.pipe(csvStream);

                        function createUser(Mlist, i) {
                            var userObj = {
                                partycd: Mlist[0],
                                fullname: Mlist[1],
                                companyname: Mlist[2],
                                usertype: 'M',
                                mobileno: Mlist[3],
                                address: Mlist[4],
                                tin: Mlist[5],
                                userstatus: appmsg.STATUS_ACTIVE
                            }
                            var sno = serialnumber.prefix_key + "" + serialnumber.prefix_cncat +
                                "" + serialnumber.suffix_key + "" +
                                serialnumber.suffix_cncat + "" + (curr_seqno + (i - 1));
                            userObj.partycd = sno;
                            TblUser.create(userObj, function(err, user) {
                                if (err !== null) {
                                    var response = {
                                        status: false,
                                        message: appmsg.SAVE_FAILED
                                    }
                                } else {
                                    log.info(user);
                                    app.models.tbl_user_dealer.create({ muserid: user.userid, duserid: ctx.req.query.duserid, dstatus: appmsg.STATUS_ACTIVE }, function(err, dealer) {
                                        if (err) {
                                            log.error(err);
                                            var response = {
                                                status: false,
                                                message: appmsg.SAVE_FAILED
                                            }
                                        } else {
                                            log.info(dealer);
                                        }
                                    });
                                }
                            });
                            return true;
                        }
                    } else {
                        var response = {
                            status: true,
                            message: appmsg.SAVE_FAILED
                        }
                        log.info(filename + ">>import>>" + response.message);
                        ctx.res.send(response);
                    }
                }
            });
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            console.log(e);
            log.error(filename + ">>upload>>" + response.message);
        }
    };
    TblUser.remoteMethod('import', {
        description: 'Import Merchant list from CSV file to user tables',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'options',
            type: 'object',
            http: {
                source: 'query'

            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'fileObject',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });
    /**
     * To check the credentials provided for user login
     * @method login
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.login = function(ctx, cb) {
        var response = {};
        try {
            validation.isMandatoryString(ctx.args.data.mobileno, 'Mobile No', ctx.res, 1, 10);
            var datares = {};
            TblUser.findOne({
                where: {
                    mobileno: ctx.args.data.mobileno,
                    //password : common.encrypt(ctx.args.data.password)
                }
            }, function(err, user) {
                log.info(err);
                if (!user) {
                    var response = {
                        status: false,
                        message: appmsg.NOT_USER
                    }
                    log.error(filename + ">>login>>" + response.message);
                    ctx.res.send(response);
                } else if (err) {
                    var response = {
                        status: false,
                        message: appmsg.INTERNAL_ERR
                    }
                    log.error(err);
                    ctx.res.send(response);
                } else {
                    if (common.encrypt(ctx.args.data.password) == user.password) {
                        if (user.otpstatus === appmsg.STATUS_VERIFIED &&
                            user.userstatus === appmsg.STATUS_ACTIVE) {
                            app.models.TblUserDealer.findOne({
                                where: { muserid: user.userid, dstatus: 'Active' },
                                fields: ['duserid', 'dstatus'],
                                include: {
                                    relation: "D",
                                    scope: {
                                        fields: ['mobileno', 'fullname', 'address', 'companyname']
                                    },
                                }
                            }, function(err, dealer) {
                                datares.dealer = dealer;
                                datares.userid = user.userid;
                                datares.usertype = user.usertype;
                                datares.fullname = user.fullname;
                                datares.companyname = user.companyname;
                                datares.usertier = user.usertier;
                                datares.pymttenure = user.pymttenure;
                                datares.mobileno = user.mobileno;
                                datares.profilestatus = 'N';
                                datares.pymtstatus = user.pymtstatus;
                                datares.email = user.emailid;
                                datares.profileimg = user.profileimg;
                                datares.tin = user.tin;
                                if (user.fullname != null) {
                                    datares.profilestatus = 'Y';
                                }
                                var response = {
                                    status: true,
                                    data: datares,
                                    message: appmsg.LOGIN_SUCCESS
                                }
                                ctx.res.send(response);
                            });
                        } else if (user.otpstatus === appmsg.STATUS_PENDING &&
                            user.userstatus === appmsg.STATUS_INACTIVE) {
                            var response = {
                                status: false,
                                message: appmsg.OTP_PENDING
                            }
                            ctx.res.send(response);
                        } else if (user.otpstatus === appmsg.STATUS_VERIFIED &&
                            user.userstatus === appmsg.STATUS_INACTIVE) {
                            var response = {
                                status: false,
                                message: appmsg.INACTIVE_USER
                            }
                        } else if (user.otpstatus === appmsg.STATUS_PENDING &&
                            user.userstatus === appmsg.STATUS_ACTIVE) {
                            var response = {
                                status: false,
                                message: appmsg.OTP_PENDING
                            }
                            ctx.res.send(response);
                        }
                    } else {
                        var response = {
                            status: false,
                            message: appmsg.INCORRECT_PWD
                        }
                        ctx.res.send(response);
                    }

                }
            });
        } catch (e) {
            validation.generateAppError(e, response, ctx.res);
        }
    };
    TblUser.remoteMethod('login', {
        description: 'Find the provided user-credentails for user login',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    // Bulk Update Order details
    TblUser.distributor = function(ctx, cb) {
        log.info(filename + ">>distributor>>");
        try {
            log.info(ctx.args.data);
            if (ctx.args.data.user && ctx.args.data.beatlist) {
                TblUser.upsert(ctx.args.data.user, function(err, user) {
                    if (err) {
                        var response = {
                            status: false,
                            message: appmsg.INTERNAL_ERR
                        }
                        ctx.res.status(500);
                        ctx.res.send(response);
                    } else {
                        if (user.userid) {
                            if (ctx.args.data.beatlist.length != 0) {
                                var beatlist = ctx.args.data.beatlist;
                                log.info(beatlist);
                                var len = beatlist.length;
                                for (var i = 0; i < beatlist.length; i++) {
                                    beatlist[i].duserid = user.userid;
                                    app.models.tbl_beat.upsert(beatlist[i], function(err, beats) {
                                        if (err) {
                                            var response = {
                                                status: false,
                                                message: appmsg.INTERNAL_ERR
                                            }
                                            ctx.res.status(500);
                                            ctx.res.send(response);
                                        } else {
                                            len--;
                                            if (len == 0) {
                                                console.log(ctx.args.data.removelist);
                                                if (ctx.args.data.removelist) {
                                                    if (ctx.args.data.removelist.length != 0) {
                                                        for (var j = 0; j < ctx.args.data.removelist.length; j++) {
                                                            app.models.tbl_beat.deleteById(ctx.args.data.removelist[j], function(err, beats) {
                                                                if (err) {
                                                                    log.error(err);
                                                                } else {
                                                                    log.info(beats);
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                                var response = {
                                                    status: true,
                                                    message: appmsg.SAVE_SUCCESSFULLY
                                                }
                                                ctx.res.status(200);
                                                ctx.res.send(response);
                                            }
                                        }
                                    });
                                }
                            } else {
                                var response = {
                                    status: true,
                                    message: appmsg.SAVE_SUCCESSFULLY
                                }
                                ctx.res.status(200);
                                ctx.res.send(response);
                            }
                        }
                    }
                });
            } else {
                var response = {
                    status: false,
                    message: appmsg.DATA_REQUIRED
                }
                ctx.res.status(200);
                ctx.res.send(response);
            }
        } catch (e) {
            var response = {
                status: false,
                message: appmsg.INTERNAL_ERR
            }
            log.error(e);
            ctx.res.status(500);
            ctx.res.send(response);
        }
    };
    TblUser.remoteMethod('distributor', {
        description: 'Save distributor and assign beat',
        accepts: [{
            arg: 'ctx',
            type: 'object',
            http: {
                source: 'context'
            }
        }, {
            arg: 'data',
            type: 'object',
            http: {
                source: 'body'
            }
        }],
        returns: {
            arg: 'message',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post'
        }
    });

    function checkDistributor(ctx, model, next) {
        try {
            log.info(ctx.args.data);
            if (ctx.args.data.user) {
                var query = 'SELECT * FROM tbl_user where usertype=? and duserid=? and fullname=?';
                var qryArr = [ctx.args.data.user.usertype, ctx.args.data.user.duserid, ctx.args.data.user.status]
                common.executeQuery(query, qryArr, function(result) {
                    if (result.data == 0) {
                        next();
                    } else {
                        var response = {
                            status: false,
                            message: appmsg.USER_AVAIL
                        }
                        ctx.res.send(response);
                    }
                });
            } else {
                var response = {
                    status: false,
                    message: appmsg.USERDATA_REQUIRED
                }
                ctx.res.status(200);
                ctx.res.send(response);
            }
        } catch (E) {
            log.error(E);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };

    /**
     * To get the distributor list of the logged in user
     * @method distributorlist
     * @param  {Object} ctx - Request object 
     * @param  {Object} cb - Callback object
     * @return {Object} response - Object contains status and message
     */
    TblUser.distributorlist = function(ctx, cb) {
        var ds = TblUser.dataSource;
        response = {};
        log.info(ctx.args.data);
        var query = "select * from tbl_beat as u where u.duserid in (select userid from tbl_user where duserid = ? and usertype='E') and u.muserid IS NULL and u.suserid IS NULL";
        var qryArr = [ctx.args.data.duserid]
        ds.connector.execute(query, qryArr, function(err, list) {
            if (err) {
                console.error(err);
                response.status = true;
                response.message = appmsg.INTERNAL_ERR;
                log.error(filename + ">>distributorlist>>" + response.message);
                ctx.res.send(response);
            } else {
                if (list.length != 0) {
                    response.status = true;
                    response.message = appmsg.LIST_FOUND;
                    response.data = list;
                } else {
                    response.status = false;
                    response.message = appmsg.LIST_NT_FOUND;
                }
                log.info(filename + ">>distributorlist>>" + response.message);
                ctx.res.send(response);
            }
        });
    };
    TblUser
        .remoteMethod(
            'distributorlist', {
                description: 'Get the distrubutor list based on the query triggered',
                accepts: [{
                    arg: 'ctx',
                    type: 'object',
                    http: {
                        source: 'context'
                    }
                }, {
                    arg: 'data',
                    type: 'object',
                    http: {
                        source: 'body'
                    }
                }],
                returns: {
                    arg: 'message',
                    type: 'object',
                    root: true
                },
                http: {
                    verb: 'post'
                }
            });

    function beforeMerchantlist(ctx, model, next) {
        try {
            log.info(ctx.args.data);
            if (ctx.args.data.usertype == 'S') {
                var query = 'SELECT * FROM tbl_user where userid=?';
                var qryArr = [ctx.args.data.userid]
                common.executeQuery(query, qryArr, function(salesman) {
                    var d_query = 'SELECT * FROM tbl_app_setting where userid=? and refkey="BEAT_TYPE"';
                    var d_qryArr = [salesman.data[0].duserid];
                    common.executeQuery(d_query, d_qryArr, function(result) {
                        if (result.status) {
                            ctx.args.data.beat_type = result.data[0].refvalue;
                        } else {
                            ctx.args.data.beat_type = 'D';
                        }
                        next();
                    });
                });
            } else {
                next();
            }
        } catch (E) {
            log.error(E);
            var response = {
                status: false,
                data: e,
                message: appmsg.INTERNAL_ERR
            }
            ctx.res.send(response);
        }
    };
    TblUser.beforeRemote('create', modifySaveObject);
    //TblUser.beforeRemote('find', modifyFindObject);
    TblUser.beforeRemote('updateAll', modifyUpdateAllObject);
    TblUser.afterRemote('create', modifySaveResponse);
    TblUser.afterRemote('upsert', modifyUpsertResponse);
    TblUser.afterRemote('find', modifyFindResponse);
    TblUser.afterRemote('updateAll', modifyUpdateAllResponse);
    TblUser.beforeRemote('upload', modifyUploadbject);
    TblUser.beforeRemote('import', modifyFileObject);
    TblUser.beforeRemote('distributor', checkDistributor);
    TblUser.beforeRemote('merchantlist', beforeMerchantlist);
};