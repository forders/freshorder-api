/**
 * @Filename : commonServices.js
 * @Description : To write Common functions.
 * @Author : Nithya
 * @Date : Jun 08,2016
 * 
 * Copyright (C) 2016 GNTS Technologies Pvt. Ltd. All rights reserved.
 * 
 * This software is the confidential and proprietary information of GNTS
 * Technologies Pvt. Ltd.
 * 
 * Version 	Date 	Modified By 	Remarks 
 * 0.1 		Jun 08 	Nithya 			
 * 0.2		Oct 20  Nithya			Instamojo API integration with instamojo-nodejs
 */
var fs = require('fs');
var path = require('path');
var filename = path.basename(__filename);
var crypto = require('crypto');
var gcm = require('node-gcm');
var algorithm = 'aes-256-ctr';
var password = 'd6F3Efeq';
var constants = require('../config/constants.js');
var log = require('../config/logger.js').logger;
var nodemailer = require('nodemailer');
var _ = require('underscore');
var AUTH_MAIL_ID = "noreply@gnts.in";
var AUTH_MAIL_PWD = "GntS@2016";
var from = "noreply@gnts.in";
var app = require('../../server/server');
var Insta = require('instamojo-nodejs'); //npm to integrate API calls on INSTAMOJO
var gdistance = require('google-distance-matrix');
var app = require('../../server/server');
Insta.setKeys(constants.pymtgatewayApiKey, constants.pymtgatewayAuthToken);
Insta.isSandboxMode(true);
exports.generateOTP = function (digit) {
    return (Math.random().toString().substr(2, digit));
};

exports.sendSMS = function (gatewayuri, user, password, senderID, mobilenumber,
    smscontent) {
    var result = true;
    require('request').post({
        uri: gatewayuri,
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        },
        form: {
            user: user + ':' + password,
            senderID: senderID,
            receipientno: mobilenumber,
            msgtxt: smscontent,
            state: "4"
        }
    }, function (err, res, body) {
        console.log(err + '-' + res + '-' + body);
        if (err) {
            result = false;
            console.log(err);
        } else {
            result = true;
        }
    });
    return result;
};

exports.encrypt = function (text) {
    var cipher = crypto.createCipher(algorithm, password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
};

exports.decrypt = function (text) {
    var decipher = crypto.createDecipher(algorithm, password);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
};

exports.makeSymmDiffFunc = (function () {
    var contains = function (pred, a, list) {
        var idx = -1,
            len = list.length;
        while (++idx < len) {
            if (pred(a, list[idx])) {
                return true;
            }
        }
        return false;
    };
    var complement = function (pred, a, b) {
        return a.filter(function (elem) {
            return !contains(pred, elem, b);
        });
    };
    return function (pred) {
        return function (a, b) {
            return complement(pred, a, b).concat(complement(pred, b, a));
        };
    };
}());

exports.pushNotifcation = function (msg, deviceID) {
    var message = new gcm.Message({
        data: {
            title: constants.gcmAppname,
            message: message
        },
        delay_while_idle: false,
        dry_run: false,
        notification: {
            title: constants.gcmAppname,
            //icon: "ic_launcher",
            body: message
        }
    });
    var sender = new gcm.Sender(constants.gcmServerApiKey);

    console.log(sender);
    //sender.setAPIKey(constants.gcmServerApiKey);
    //console.log("title"+title);
    console.log("message" + msg);
    sender.send(message, {
        registrationTokens: [deviceID]
    }, function (err, response) {
        if (err)
            console.error(err);
        else
            console.log(response);
    });
    /*sender.sendMessage(message.toString(), deviceID, true, function(err, data){
    	console.log(data);
    });*/
};

exports.sendMail = function mail(toObj, file, res) {
    console.log(toObj);
    // create reusable transport method (opens pool of SMTP connections)
    var smtpTransport = nodemailer.createTransport("Godaddy", {
        transport: "SMTP",
        host: "mail.gnts.in",
        secureConnection: false,
        port: 587,
        requiresAuth: true,
        auth: {
            user: AUTH_MAIL_ID,
            pass: AUTH_MAIL_PWD
        }
    });
    var mailOptions = {};
    mailOptions = {
        from: from,
        to: toObj,
        subject: "Export File", // Subject line
        html: "Please find the Attachment",
        attachments: [{
            filename: file,
            filePath: constants.CONFIG_PATH + file,
            contentType: 'application/csv'
        }]
    };
    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log(response);
            // res.send("Message sent: " + response.message);
        }
        smtpTransport.close();
    });
};
// Convert the date in yyyy-MM-dd format
exports.convertDate = function (date, cb) {
    console.log(date);
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var yr = date.getFullYear();
    var cdt = yr + '-' + mm + '-' + dd;
    return cdt;
};
// Raw Query Execution 
exports.executeQuery = function (query, arr, cb) {
    var ds = app.models.TblUserDealer.dataSource;
    var response = {};
    ds.connector.execute(query, arr, function (err, result) {
        if (err) {
            console.error(err);
            response.data = 0;
            response.status = false;
            cb(response);
        } else {
            if (result.length !== 0) {
                response.data = result;
                response.status = true;
                cb(response);
            } else {
                response.data = 0;
                response.status = false;
                cb(response);
            }
        }

    });
};
//Request to make payment with INSTAMOJO
exports.makePayment = function (input, cb) {
    var data = {};
    data.purpose = input.purpose; // REQUIRED 
    data.amount = input.amount; // REQUIRED 
    data.currency = 'INR';
    data.buyer_name = input.buyername;
    data.email = input.buyeremail;
    data.phone = input.mobileno;
    data.send_sms = 'False';
    data.send_email = 'False';
    data.allow_repeated_payments = 'False';
    data.redirect_url = constants.REDIRECTURL + input.pymtid;
    data.webhook = 'http://192.168.1.74:2001/api/paymentdetails/update?where[pymtid]=' + input.pymtid;
    var result = {};
    Insta.createPayment(data, function (error, response) {
        if (error) {
            log.error(error);
            result.status = false;
            result.data = error;
            cb(result);
        } else {
            // Payment redirection link at response.payment_request.longurl 
            //log.info(response);
            result.status = true;
            result.data = response;
            cb(result);
        }
    });
};
//Get payment details -from INSTAMOJO
exports.getPaymentdetails = function (requestid, paymentid, cb) {
    var result = {};
    Insta.getPaymentDetails(requestid, paymentid, function (error, response) {
        if (error) {
            log.error(error);
            result.status = false;
            result.data = error;
            cb(result);
        } else {
            //log.info(response);
            result.status = true;
            result.data = response;
            cb(result);
        }
    });
};
//Get payment status -from INSTAMOJO
exports.paymentstatus = function (pymtreqid, cb) {
    var result = {};
    Insta.getPaymentRequestStatus(pymtreqid, function (error, response) {
        if (error) {
            log.error(error);
            result.status = false;
            result.data = error;
            cb(result);
        } else {
            log.info(response);
            result.status = true;
            result.data = response;
            cb(result);
        }
    });
};
exports.calculateDistance = function (data, key, cb) {
    try {
        gdistance.key('AIzaSyCHG7Q2Eie0zO85-Y7WgUOBuZGq3A_zxJE');
        origins = [];
        destinations = [];
        newdata = [];
        newdata = data;
        var distance = 0;
        var entry = newdata.length;
        if (newdata.length !== 0) {
            _.each(newdata, function (dobj, index) {
                var fullname = dobj.fullname;
                if (index != (newdata.length - 1)) {
                    origins[0] = (dobj.geolat + ',' + dobj.geolong);
                    destinations[0] = (newdata[index + 1].geolat + ',' + newdata[index + 1].geolong);

                }
                /*else {
					origins.push(dobj.geolat + ',' + dobj.geolong);
					destinations.push(dobj.geolat + ',' + dobj.geolong);
				}*/
                /*if (index == (newdata.length - 1)) {*/
                console.log(origins);
                console.log(destinations);
                var origin_length = origins.length;
                var destinations_length = destinations.length;
                gdistance.matrix(origins, destinations, function (err, distances) {
                    entry = entry - 1;
                    if (err) {
                        log.info(err);
                        var n_obj = {
                            status: true,
                            distance: distance,
                            fullname: fullname,
                        };
                        cb(n_obj);
                    } else if (distances.status == 'OK') {
                        log.info(distances);
                        /*for (var i = 0; i < origin_length; i++) {
                        	for (var j = 0; j < destinations_length; j++) {*/
                        /*var origin = distances.origin_addresses[i];
                        var destination = distances.destination_addresses[j];*/
                        if (distances.rows[0].elements[0]) {
                            if (distances.rows[0].elements[0].status == 'OK') {
                                log.info(distances.rows[0].elements[0].distance.value);
                                distance += distances.rows[0].elements[0].distance.value;
                            }
                        }
                        /*
                        									log.info("j"+j);
                        									log.info("i"+i);*/
                        console.log(entry);
                        console.log(distance);
                        if (entry === 0) {
                            var n_obj = {
                                status: true,
                                distance: distance,
                                fullname: fullname,
                                key: key
                            };
                            cb(n_obj);
                        }
                        /*	}
                        }*/
                    } else {
                        console.log(distances);
                        var n_obj = {
                            status: true,
                            distance: distance,
                            fullname: fullname,
                            key: key
                        };
                        cb(n_obj);
                    }
                });
                /*}*/
            });
        } else {
            cb({ status: false });
        }
    } catch (e) {
        console.log(e);
        var n_obj = {
            status: true,
            distance: distance,
            key: key
        };
        cb(n_obj);
    }
};

exports.updateStock = function (data, duserid, callback) {
    try {
        var ds = app.models.TblUserDealer.dataSource;
        var response = {};
        var query = '';
        var arr = [];
        var len = data.length;
        _.map(data, function (item) {
            console.log(item);
            var getquery = "SELECT * from tbl_stock_entry WHERE prodid = ? and userid=? and islatest='Y'";
            var getarr = [item.prodid, duserid];
            ds.connector.execute(getquery, getarr, function (err, products) {
                if (err) {
                    console.log(err);
                } else if (products != null) {
                    item.products=products;
                    var upquery = "UPDATE tbl_stock_entry SET `islatest` = 'N' WHERE prodid = ? and userid=? and islatest='Y'";
                    var uparr = [item.prodid, duserid];
                    ds.connector.execute(upquery, uparr, function (err, result) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log(item.products);
                            if (item.products!=null && item.products!=undefined && item.products.length!=0) {
                                var insert =item.products[0];
                                query = "INSERT INTO tbl_stock_entry (userid,prodid,opening,qty,closing,in_out,islatest,defaultuom,ledgeruom) VALUES(?,?,?,?,?,'O','Y',?,? )";
                                console.log("inserted");
                                arr = [insert.userid, insert.prodid, insert.closing, item.qty,parseFloat(insert.closing)-parseFloat(item.cqty), item.default_uomid, item.ord_uomid];
                                console.log(arr);
                            } else {
                                query = "INSERT INTO tbl_stock_entry (userid,prodid,opening,qty,closing,in_out,islatest,defaultuom,ledgeruom) VALUES (?,?,0,?,?,'O','Y',?,?)";
                                var closing = '-' + item.cqty;
                                arr = [duserid, item.prodid, item.qty, closing, item.default_uomid, item.ord_uomid];
                            
                            }
                            ds.connector.execute(query, arr, function (err, created) {
                                console.error(err);
                                if (err) {
                                    log.error(err);
                                    len--;
                                } else {
                                    log.error(err);
                                    log.info(created);
                                    var updateproduct = "UPDATE tbl_user_prod SET `stock` = (select closing from tbl_stock_entry where prodid=? and islatest='Y' and userid=? ) WHERE prodid = ?";
                                    var uparr = [item.prodid, duserid, item.prodid];
                                    ds.connector.execute(updateproduct, uparr, function (err, updated) {
                                        if (err) {
                                            log.error(err);
                                            len--;
                                        } else {
                                            log.info(" log.info(updated);");
                                            log.info(updated);
                                            len--;
                                        }
                                        if (len === 0) {
                                            callback(true);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    } catch (e) {
        log.info(e);
        callback(false);
    }
};