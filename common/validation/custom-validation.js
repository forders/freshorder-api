/**
 * @file 
 * Provides validation feature.
 *
 * @name Reshma.R
 */
var validMsg = require('../validation/validation-messages.js');
var AppError = require('../validation/AppError.js');
var log = require('../config/logger.js').logger;


/**
 * This function used to validate string value.
 *
 * @name isMandatoryString
 * 
 * @param inputvalue, columnname, res, strlimitmin, strlimitmax
 * @return boolean
 */
function isMandatoryString(inputvalue, columnname, res, strlimitmin, strlimitmax) {

    if (isEmptyValue(inputvalue)) {

        errorResponse(columnname, res, validMsg.EMPTY);

    } else if (inputvalue.length < strlimitmin || inputvalue.length > strlimitmax) {
        errorResponse(columnname, res, validMsg.LIMIT);

    } else {
        return true;
    }
}

/**
 * This function used to validate string value.
 *
 * @name isOptionalString
 * 
 * @param inputvalue, columnname, res, strlimitmin, strlimitmax
 * @return boolean
 */
function isOptionalString(inputvalue, columnname, res, strlimitmin, strlimitmax) {
    if (isEmptyValue(inputvalue)) {
        return true;
    } else {
        isMandatoryString(inputvalue, columnname, res, strlimitmin, strlimitmax);
    }

}

/**
 * This function used to validate long value.
 *
 * @name isMandatoryLong
 * 
 * @param inputvalue, columnname, res, strlimitmin, strlimitmax
 * @return boolean
 */
function isMandatoryLong(inputvalue, columnname, res, strlimitmin, strlimitmax) {

    if (isEmptyValue(inputvalue)) {
        errorResponse(columnname, res, validMsg.EMPTY);
    } else if (inputvalue.length < strlimitmin || inputvalue.length > strlimitmax) {

        errorResponse(columnname, res, validMsg.LIMIT);
    } else if (typeof inputvalue !== 'number') {
        errorResponse(columnname, res, validMsg.NUM);
    } else if (inputvalue % 1 != 0) {

        errorResponse(columnname, res, validMsg.NUM);
    } else {
        return true;
    }
}

/**
 * This function used to validate long value.
 *
 * @name isOptionalLong
 * 
 * @param inputvalue, columnname, res, strlimitmin, strlimitmax
 * @return boolean
 */
function isOptionalLong(inputvalue, columnname, res, strlimitmin, strlimitmax) {
    if (isEmptyValue(inputvalue)) {
        return true;
    } else {
        isMandatoryLong(inputvalue, columnname, res, strlimitmin, strlimitmax);
    }

}

/**
 * This function used to validate decimal value.
 *
 * @name isMandatoryDecimal
 * 
 * @param inputvalue, columnname, res
 * @return boolean
 */
function isMandatoryDecimal(inputvalue, columnname, res) {

    if (isEmptyValue(inputvalue)) {
        errorResponse(columnname, res, validMsg.EMPTY);

    } else if (typeof inputvalue !== 'number') {
        errorResponse(columnname, res, validMsg.NUM);
    } else if (inputvalue % 1 == 0) {
        errorResponse(columnname, res, validMsg.DECIMAL);

    } else {
        return true;
    }
}

/**
 * This function used to validate decimal value.
 *
 * @name isOptionalDecimal
 * 
 * @param inputvalue, columnname, res
 * @return boolean
 */
function isOptionalDecimal(inputvalue, columnname, res) {
    if (isEmptyValue(inputvalue)) {
        return true;
    } else {
        isMandatoryDecimal(inputvalue, columnname, res);
    }

}

/**
 * This function used to validate date value.
 *
 * @name isMandatoryDate
 * 
 * @param inputvalue, columnname, res
 * @return boolean
 */
function isMandatoryDate(inputvalue, columnname, format, res) {
    if (format == 'YYYY-MM-DD') {
        var dtRegex = new RegExp(/\b\d{4}[\/-]\d{1,2}[\/-]\b\d{1,2}$\b/);
    } else {
        var dtRegex = new RegExp(/\b\d{4}[\/-]\d{1,2}[\/-]\b\d{1,2} (0\d|1\d|2[0-3]):[0-5]\d:[0-5]\d$\b/);
    }
    // console.log(dtRegex.test(inputvalue));
    // return dtRegex.test(inputvalue);
    // var dateformat = /^\d{4}-[0-1][0-2]-[0-3]\d\s([0-1][0-9]|2[0-3]):[0-5]\d$/;
    var result = new Date(Date.parse(inputvalue));
    if (isEmptyValue(inputvalue)) {
        errorResponse(columnname, res, validMsg.EMPTY);
    } else if (!dtRegex.test(inputvalue)) {
        errorResponse(columnname, res, validMsg.NUM);
    } else if (result == "Invalid Date") {
        errorResponse(columnname, res, validMsg.DATE);
    } else {
        return true;
    }
}

/**
 * This function used to validate date value.
 *
 * @name isOptionalDate
 * 
 * @param inputvalue, columnname, res
 * @return boolean
 */
function isOptionalDate(inputvalue, columnname, res) {
    if (isEmptyValue(inputvalue)) {
        return true;
    } else {
        isMandatoryDate(inputvalue, columnname, res);
    }

}

/**
 * This function used to validate email id.
 *
 * @name isMandatoryEmail
 * 
 * @param inputvalue, columnname, res , strlimitmin, strlimitmax
 * @return boolean
 */
function isMandatoryEmail(inputvalue, columnname, res, strlimitmin, strlimitmax) {


    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (isEmptyValue(inputvalue)) {

        errorResponse(columnname, res, validMsg.EMPTY);
    } else if (inputvalue.length < strlimitmin || inputvalue.length > strlimitmax) {
        errorResponse(columnname, res, validMsg.LIMIT);

    } else if (!mailformat.test(inputvalue)) {

        errorResponse(columnname, res, validMsg.EMAIL);
    } else {

        return true;

    }
}
/**
 * This function used to validate mail id.
 *
 * @name isOptionalEmail
 * 
 * @param inputvalue, columnname, res , strlimitmin, strlimitmax
 * @return boolean
 */

function isOptionalEmail(inputvalue, columnname, res, strlimitmin, strlimitmax) {
    if (isEmptyValue(inputvalue)) {
        return true;
    } else {
        isMandatoryEmail(inputvalue, columnname, res, strlimitmin, strlimitmax);
    }

}

/**
 * Loosely validate a URL `string`.
 *
 * @param {String} string
 * @return {Boolean}
 */
function isValidURL(string) {
    var matcher = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
    return matcher.test(string);
}

function isEmptyValue(inputvalue) {
    if (inputvalue === null || inputvalue === undefined || inputvalue.length === 0) {
        return true;
    }
    return false;
}

function errorResponse(columnname, res, msg) {
    throw new AppError(columnname + msg);
}

function generateAppError(e, response, res) {
    log.error(e);
    response.status = false;
    if (e instanceof AppError) {
        response.code = "APP_ERROR";
        response.message = e.message;
    } else {
        response.code = "SYS_ERROR";
        response.message = "Internal Error";
    }
    res.send(response);
}

module.exports = {
    isMandatoryEmail: isMandatoryEmail,
    isOptionalEmail: isOptionalEmail,
    isMandatoryDate: isMandatoryDate,
    isOptionalDate: isOptionalDate,
    isMandatoryLong: isMandatoryLong,
    isOptionalLong: isOptionalLong,
    isMandatoryString: isMandatoryString,
    isOptionalString: isOptionalString,
    isMandatoryDecimal: isMandatoryDecimal,
    isOptionalDecimal: isOptionalDecimal,
    isValidURL: isValidURL,
    generateAppError: generateAppError

};