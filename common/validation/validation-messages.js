/**
 * @file 
 * Provides validation meassages.
 *
 * @name Reshma.R
 */

exports.EMPTY = " is empty";
exports.DATE = " is invalid date";
exports.LIMIT = "  size is invalid ";
exports.NUM = " is invalid input";
exports.EMAIL = " is invalid email";
exports.DECIMAL = " is not in decimal";
