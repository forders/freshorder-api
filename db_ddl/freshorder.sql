CREATE TABLE `tbl_pymt_setup` (
  `psetupid` 	int(11) NOT NULL AUTO_INCREMENT,
  `usertier`	varchar(10),			-- Added for #757
  `pymttenure` 	varchar(15) NOT NULL,
  `pymtamount` 	int NOT NULL,
  PRIMARY KEY (`psetupid`)
);

CREATE TABLE `tbl_industry` (			-- Added for 1687
  `industryid` 	int(11) NOT NULL AUTO_INCREMENT,
  `industrynm`	varchar(50),			
  `status` 		varchar(10),
  `updateddt` 	datetime,
  PRIMARY KEY (`industryid`)
);

CREATE TABLE `tbl_user` (
  `userid` 		int(11) NOT NULL AUTO_INCREMENT,
  `partycd` 	varchar(10),	-- Thirdparty ref no
  `mobileno` 	varchar(10) NOT NULL,
  `usertype` 	varchar(1) NOT NULL,	-- D-Dealer, M-Merchant, S-Salesman, C-Customer
  `usertier` 	varchar(10) NOT NULL,	-- Gold, Silver, Bronze, etc (Added for #757)
  `password` 	varchar(50) NOT NULL,
  `fullname` 	varchar(25),
  `duserid` 	int(11),  				-- Only if usertype is C
  `profileimg` 	varchar(50),
  `companyname` varchar(50),
  `emailid` 	varchar(50),
  `cust_code` 	varchar(45),  
  `address` 	varchar(1000),
  `city` 		varchar(45),  
  `pincode` 	varchar(10),
  `tax_no` 		varchar(30),
  `pymttenure` 	varchar(10),
  `otp` 		varchar(10),
  `otpstatus` 	varchar(10),		-- pending, verified
  `pymtstatus` 	varchar(10),		-- pending, paid
  `gcmid` 		varchar(1000),
  `geolat` 		varchar(15),		-- Added for #798
  `geolong` 	varchar(15),		-- Added for #798
  `creditlimit` int(11),			-- Added for #754
  `cflag` 		varchar(5),			-- Added for #2968
  `updateddt` 	timestamp,
  `lastpymtdt` 	timestamp,
  `nextpymtdt` 	timestamp,
  `lastlogin` 	timestamp,
  `userstatus` 	varchar(15) DEFAULT 'inactive',		-- active, inactive
  PRIMARY KEY (`userid`)
);

CREATE TABLE `tbl_user_prod` (
  `prodid` 		int(11) NOT NULL AUTO_INCREMENT,
  `userid` 		int(11) NOT NULL,
  `prodcode` 	varchar(30),
  `prodname` 	varchar(50),
  `prodprice` 	float,				-- Added for #765
  `prodtax` 	float,				-- Added for #631
  `mrp` 		float,
  `uom` 		varchar(30),		-- Added for 
  `stock` 		float,
  `importfile` 	varchar(50),
  `importrmrk` 	varchar(50),
  `prodstatus` 	varchar(10)  DEFAULT 'active',	-- active, inactive
  `updateddt` 	timestamp,
  PRIMARY KEY (`prodid`)
);

CREATE TABLE `tbl_stock_entry` (	
  `stockid` 	int(11) NOT NULL AUTO_INCREMENT,
  `userid` 		int(11) NOT NULL,
  `suserid` 	int(11) ,
  `muserid` 	int(11) ,
  `prodid` 		int(11) NOT NULL,
  `batchno` 	varchar(30),		
  `opening` 	float,
  `qty` 		float,
  `closing` 	float,
  `remark` 		varchar(50),
  `manufdt` 	date,	-- Added for #3054
  `expirydt` 	date,	-- Added for #3054
  `updateddt` 	timestamp,
  `updatedby` 	varchar(10),  
  PRIMARY KEY (`stockid`)
);

CREATE TABLE `tbl_prod_setting` (	-- Added for 1687
  `prdsetngid` 	int(11) NOT NULL AUTO_INCREMENT,
  `prodid` 		int(11) NOT NULL,
  `muserid` 	int(11) ,
  `refkey` 		varchar(10),
  `refvalue` 	varchar(50),
  `status` 		varchar(10)  DEFAULT 'active',	-- active, inactive
  `updateddt` 	timestamp,
  PRIMARY KEY (`prdsetngid`)
);

CREATE TABLE `tbl_pymt_details` (
  `pymntid` 	int(11) NOT NULL AUTO_INCREMENT,
  `userid` 		int(11) NOT NULL,
  `pymtamount` 	int NOT NULL,
  `pymtdate` 	timestamp,
  `pymtref` 	varchar(50),
  `pymtstatus` 	varchar(10),
  `remarks` 	varchar(100),
  PRIMARY KEY (`pymntid`)
);

CREATE TABLE `tbl_user_dealer` (
  `usrdlrid` 	int(11) NOT NULL AUTO_INCREMENT,
  `muserid` 	int(11) NOT NULL,
  `duserid` 	int(11) NOT NULL,
  `dstatus` 	varchar(10),
  `updateddt` 	timestamp,  
  PRIMARY KEY (`usrdlrid`)
);

CREATE TABLE `tbl_orders` (
  `ordid` 		int(11) NOT NULL AUTO_INCREMENT,
  `muserid` 	int(11) ,	-- merchant user who raised the order
  `suserid` 	int(11) ,	-- salesman user who raised the order
  `oflnordid` 	double,		-- should be in  suserid+ddmmyyhhmmss format
  `orderdt` 	datetime,
  `iscash` 		varchar(2)	-- C - Cash, Cr - Credit
  `discntval` 	int,		-- Added for #763
  `discntprcnt` int,		-- Added for #763
  `aprxordval` 	int,		-- Added for #631
  `ordertotal` 	int,		-- Added for #762
  `pymttotal` 	int,		-- Added for #761
  `pymtdate` 	date,		-- Added for #1689
  `geolat` 		varchar(25),	-- Added for #2884
  `geolong` 	varchar(25),	-- Added for #2884,
  `start_time` 	varchar(10),	
  `end_time` 	varchar(10),	
  PRIMARY KEY (`ordid`)
);

CREATE TABLE `tbl_order_dtl` (
  `orddtlid` 	int(11) NOT NULL AUTO_INCREMENT,
  `ordid` 		int(11) NOT NULL,
  `ordslno` 	int(6) NOT NULL,
  `duserid` 	int(11) NOT NULL,	-- user who received the order
  `prodid` 		int(11) NOT NULL,
  `batchno` 	varchar(20) NOT NULL, -- Added for #NF011
  `manufdt` 	date,	-- Added for #3054
  `expirydt` 	date,	-- Added for #3054
  `qty` 		float NOT NULL,
  `prate` 		float NOT NULL,		-- Added for #631
  `ptax` 		float NOT NULL,		-- Added for #631
  `pvalue` 		float NOT NULL,		-- Added for #631
  `isfree`		varchar(2),			-- F - Free, NF - Not Free
  `iscash`		varchar(2),			-- C - Cash, Cr - Credit
  `isread`		varchar(20),		-- Store the duesrid, muserid as comma seperated, once read remove the particular user id
  `deliverydt` 	date,
  `discntval` 	int,				-- Added for #2091 
  `discntprcnt` int,				-- Added for #2091 
  `ordimage`	varchar(150),		-- Added for #764 (Rel 3) and image based order taking (rel 2.1)
  `ordstatus` 	varchar(10),		-- pending, progress, cancelled, delivered
  `stock` 	varchar(150),		-- Added for #NF012
  `updateddt` 	timestamp,
  `updatedby` 	varchar(10),
  PRIMARY KEY (`orddtlid`)
);

CREATE TABLE `tbl_complaints` (
  `complid` 	int(11) NOT NULL AUTO_INCREMENT,
  `muserid` 	int(11) NOT NULL,
  `duserid` 	int(11) NOT NULL,
  `cmplnttype` 	varchar(1),		-- U - User, S - System
  `particular` 	varchar(1000),
  `attchmnt` 	varchar(50),
  `status` 		varchar(10),	-- pending, deleted
  `updateddt` 	timestamp,
  PRIMARY KEY (`complid`)
);

CREATE TABLE `tbl_app_setting` (
  `settingid` 	int(11) NOT NULL AUTO_INCREMENT,
  `industryid` 	int(11) NOT NULL,	
  `userid` 		int(11) NOT NULL,	
  `refkey` 		varchar(10),		-- GEO, ROUTEMAP, NEWSTORE, etc
  `refvalue` 	varchar(50),
  `data_type` 	varchar(10) NOT NULL,	-- List, String, Text, Integer, Datetime  
  `remarks` 	varchar(50),
  `status` 		varchar(10)  DEFAULT 'active',	-- active, inactive
  `updateddt` 	datetime,
  PRIMARY KEY (`settingid`)
);

-- For Redmine Feature #805 
CREATE TABLE `tbl_salesman_geolog` (
  `ulogid` 		int(11) NOT NULL AUTO_INCREMENT,
  `suserid` 	int(11) NOT NULL,
  `logtime` 	datetime,
  `geolat` 		varchar(15),
  `geolong` 	varchar(15),
  `status` 		varchar(10)  DEFAULT 'active',	-- active, inactive
  `updateddt` 	datetime,
  PRIMARY KEY (ulogid)
);

-- For Redmine Feature #797
CREATE TABLE `tbl_smroute_plan` (
  `planid` 				int(11) NOT NULL AUTO_INCREMENT,
  `suserid` 			int(11) NOT NULL,
  `duserid` 			int(11),
  `muserid` 			int(11),
  `staffname` 			varchar(11),
  `geolat` 				varchar(15),				-- Added for client visit module
  `geolong` 			varchar(15),  
  `orderplndt` 			datetime,
  `remarks` 			varchar(500),				-- Added for 1889
  `exportfilenm` 		varchar(30),
  `reporteedesignatn` 	varchar(25),
  `status` 				varchar(10) DEFAULT 'planned',	-- planned, completed
  `updateddt` 			datetime,
  PRIMARY KEY (planid)
);

CREATE TABLE `tbl_sale_bill` (
  `billid` 				int(11) NOT NULL AUTO_INCREMENT,
  `billno` 				varchar(15),
  `billdt` 				datetime,
  `billtype` 			varchar(2),			-- C-Cash, Cr-Credit
  `ordid` 				int(11),
  `orderdt` 			datetime,
  `userid` 				int(11),			-- Customer id
  `basictotal` 			float,		  
  `discntprcnt` 		float,		
  `discntval` 			float,		
  `taxprcnt` 			float,		
  `taxval` 				float,		
  `roundoff` 			float,		
  `billtotal` 			float,		
  `pymtid`				int(11),
  `paidamount` 			float,		
  `balamount` 			float,	
  `packid` 				int(11),
  `status` 				varchar(10),		-- Billed, Packed
  `last_updated_dt` 	datetime,
  `last_updated_by` 	varchar(10),
  PRIMARY KEY (`billid`)
);

CREATE TABLE `tbl_sale_bill_dtl` (
  `billdtlid` 		int(11) NOT NULL AUTO_INCREMENT,
  `billid` 			int(11) NOT NULL,
  `ordslno` 		int(6) NOT NULL,
  `prodid` 			int(11) NOT NULL,
  `qty` 			float NOT NULL,
  `mrp` 			float,
  `rate` 			float NOT NULL,	
  `discount` 		float,	  
  `amount` 			float,	  
  `taxprcnt` 		float,	
  `taxval` 			float,		
  `basicvalue` 		float,		
  `packverify` 		varchar(1),				-- Y/N
  `verifieddt` 		timestamp,
  `verifiedby` 		varchar(10),  
  PRIMARY KEY (`billdtlid`)
);

CREATE TABLE `tbl_packing_lst` (
  `packid` 				int(11) NOT NULL AUTO_INCREMENT,
  `packdt` 				datetime,
  `packdescr`			varchar(100),
  `billid`				varchar(500),		-- comma seperated
  `suserid` 			int(11),  
  `status` 				varchar(10),		-- Draft, Verified, Delivered
  `last_updated_dt` 	datetime,
  `last_updated_by` 	varchar(10),
  PRIMARY KEY (`packid`)
);

CREATE TABLE `tbl_slno_gen` (
  `slno_id` 			int(11) NOT NULL AUTO_INCREMENT,
  `duserid` 			int(11) NOT NULL,
  `ref_key` 			varchar(15),
  `key_desc` 			varchar(25),
  `prefix_key` 			varchar(5),
  `prefix_cncat` 		varchar(1),
  `suffix_key` 			varchar(5),
  `suffix_cncat` 		varchar(1),
  `curr_seqno` 			int(11),
  `last_seqno` 			int(11),
  `status` 				varchar(10),
  `last_updated_dt` 	datetime NULL,
  `last_updated_by` 	varchar(10),
  PRIMARY KEY (`slno_id`)
);

CREATE TABLE `tbl_pymt_collectin` (
  `pymtid` 				int(11) NOT NULL AUTO_INCREMENT,
  `pymt_dt` 			datetime,
  `billid`				varchar(500),		-- comma seperated
  `suserid` 			int(11),  
  `status` 				varchar(10),		-- Active, Cancelled
  `last_updated_dt` 	datetime,
  `last_updated_by` 	varchar(10),
  PRIMARY KEY (`pymtid`)
);

CREATE TABLE  `tbl_schemes` (
 `id`                   int(11) NOT NULL AUTO_INCREMENT,
 `duserid`              int(11) NOT NULL,
 `prodid`               int(11) NOT NULL,       
 `start_date`           date,
 `end_date`             date,
 `schemetype`           varchar(1),          -- P-Product, R-Rate
 `discount`             int,                 -- If Rate
 `free_prodid`          int,                 -- If Product
 `free_qty`             int,                 -- If Product
 `remarks`              varchar(100),
 `status`               varchar(10),       
 `last_updated_dt`      datetime,
 `last_updated_by`      varchar(50),
  PRIMARY KEY (`id`)
);

CREATE TABLE `tbl_beat` (
  `beatid` int(11) NOT NULL AUTO_INCREMENT,
  `muserid` int(11) DEFAULT NULL,
  `suserid` int(11) DEFAULT NULL,
  `duserid` int(11) DEFAULT NULL,
  `beattype` varchar(1) DEFAULT NULL,
  `day_mon` varchar(1) DEFAULT NULL,
  `day_tue` varchar(1) DEFAULT NULL,
  `day_wed` varchar(1) DEFAULT NULL,
  `day_thu` varchar(1) DEFAULT NULL,
  `day_fri` varchar(1) DEFAULT NULL,
  `day_sat` varchar(1) DEFAULT NULL,
  `day_sun` varchar(1) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `beatnm` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `createddt` datetime DEFAULT NULL,
  `updateddt` datetime DEFAULT NULL,
  PRIMARY KEY (`beatid`)
) ;

CREATE TABLE `tbl_dealer_beat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beatnm` varchar(100) DEFAULT NULL,
  `duserid` varchar(15) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `updateddt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedby` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

 -- My Day plan  
ALTER TABLE `tbl_smroute_plan`
ADD COLUMN `distid` INT(11) NULL AFTER `remarks`,
ADD COLUMN `worktype` VARCHAR(25) NULL AFTER `distid`,
ADD COLUMN `id` INT(11) NULL AFTER `worktype`,
ADD COLUMN `beatnm` VARCHAR(50) NULL AFTER `id`;

-- Delivery schedule/standard changes
ALTER TABLE `tbl_order_dtl`
ADD COLUMN `deliverytime` VARCHAR(5) NULL AFTER `default_uom`,
ADD COLUMN `deliverytype` VARCHAR(3) NULL AFTER `deliverytime`;

-- UOM convertion - converted quantity column
ALTER TABLE `tbl_order_dtl`
ADD COLUMN `cqty` FLOAT NULL AFTER `deliverytype`;

ALTER TABLE `tbl_pymt_setup` 
ADD COLUMN `suserid` VARCHAR(250) NULL AFTER `pymtamount`;


ALTER TABLE `tbl_sale_bill` 
ADD COLUMN `additional_discount_value` FLOAT NULL AFTER `discntval`,
ADD COLUMN `additional_discount_percent` FLOAT NULL AFTER `additional_discount_value`,
ADD COLUMN `grant_total` DECIMAL(20,4) NULL AFTER `additional_discount_percent`;


ALTER TABLE `tbl_orders` 
CHANGE COLUMN `discntval` `discntval` DECIMAL(20,4) NULL DEFAULT NULL ,
CHANGE COLUMN `discntprcnt` `discntprcnt` FLOAT NULL DEFAULT NULL ,
CHANGE COLUMN `aprxordval` `aprxordval` DECIMAL(20,4) NULL DEFAULT NULL ,
CHANGE COLUMN `ordertotal` `ordertotal` DECIMAL(20,4) NULL DEFAULT NULL ,
CHANGE COLUMN `pymttotal` `pymttotal` DECIMAL(20,4) NULL DEFAULT NULL ;

-- Update order detail quantity changes
UPDATE `tbl_order_dtl` SET `ord_uom` = 'Kilogram(kg)', `default_uom` = 'Kilogram(kg)',`cqty` = qty WHERE cqty is null;

-- UoM implementation in Invoice
ALTER TABLE `tbl_sale_bill_dtl` 
ADD COLUMN `ord_uom` VARCHAR(45) NULL AFTER `qty`,
ADD COLUMN `default_uom` VARCHAR(45) NULL AFTER `ord_uom`,
ADD COLUMN `cqty` FLOAT NULL AFTER `default_uom`;

ALTER TABLE `tbl_sale_bill_dtl` 
ADD COLUMN `discntprcnt` FLOAT NULL DEFAULT NULL AFTER `discount`;



-- #6258 - Product Display order

ALTER TABLE `tbl_user_prod` 
CHANGE COLUMN `updateddt` `updateddt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `unitperuom`,
ADD COLUMN `display_order` INT NOT NULL DEFAULT 1 AFTER `updateddt`;

-- #6259 - Print Option for Orders
ALTER TABLE `tbl_sale_bill`
ADD COLUMN `duserid` INT NOT NULL AFTER `userid`;

ALTER TABLE `tbl_order_dtl`
CHANGE COLUMN `discntval` `discntval` DECIMAL NOT NULL DEFAULT '0.00' ,
CHANGE COLUMN `discntprcnt` `discntprcnt` FLOAT NOT NULL DEFAULT '0' ;

-- Update query to update the duserid
update tbl_sale_bill as b
join tbl_user_dealer as ud on ud.muserid = b.userid
set b.duserid=ud.duserid;

-- August 2017 Inventory Management 

ALTER TABLE `tbl_user_prod` 
ADD COLUMN `margin_percent` FLOAT NOT NULL DEFAULT 0 AFTER `display_order`;

ALTER TABLE `tbl_sale_bill_dtl` 
ADD COLUMN `margin_percent` FLOAT NOT NULL DEFAULT 0 AFTER `taxval`;
ALTER TABLE `tbl_sale_bill_dtl` 
ADD COLUMN `margin_value` DECIMAL(20,2) NULL AFTER `margin_percent`;

ALTER TABLE `tbl_stock_entry` 
ADD COLUMN `defaultuom` INT(11) NOT NULL AFTER `closing`,
ADD COLUMN `in_out` VARCHAR(1) NOT NULL AFTER `defaultuom`;

ALTER TABLE `tbl_prod_uom` 
ADD COLUMN `isdefault` TINYINT(1) NOT NULL AFTER `status`;

ALTER TABLE `tbl_stock_entry` 
ADD COLUMN `islatest` VARCHAR(1) NOT NULL AFTER `expirydt`;

-- #6880

ALTER TABLE `tbl_user_prod` 
ADD COLUMN `unitprice` DECIMAL(20,2) NOT NULL AFTER `uom`;

ALTER TABLE `tbl_user_prod` 
ADD COLUMN `hsncode` VARCHAR(50) NOT NULL AFTER `prodcode`;

--#6893

ALTER TABLE `tbl_stock_entry`
ADD COLUMN `ledgeruom` INT NULL AFTER `defaultuom`;

--#6779

INSERT INTO freshorder.tbl_prod_uom(prodid,duserid,uomname,uomvalue,status,isdefault,updateddt,updatedby) 
SELECT prodid,userid,IFNULL(uom,'Kilogram(kg)'),IFNULL(unitperuom,'1'),'Active',1,now(),'ADMIN' FROM freshorder.tbl_user_prod

--#6880

UPDATE tbl_user_prod SET unitprice = prodprice WHERE unitprice=0 and margin_percent=0;
