var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require("path");
var app = module.exports = loopback();
var filename = path.basename(__filename);
app.start = function() {
	// start the web server
	return app.listen(function() {
				app.emit('started');
				var baseUrl = app.get('url').replace(/\/$/, '');
				console.log('Web server listening at: %s', baseUrl);
				if (app.get('loopback-component-explorer')) {
					var explorerPath = app.get('loopback-component-explorer').mountPath;
					console.log('Browse your REST API at %s%s', baseUrl,
							explorerPath);
				}
			});
};
app.use(loopback.logger('dev'));
var ds = loopback.createDataSource({
	connector : require('loopback-component-storage'),
	provider : 'filesystem',
	root : path.join(__dirname, 'storage')
});

var files = ds.createModel('files');
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
	if (err)
		throw err;

	// start the server if `$ node server.js`
	if (require.main === module)
		app.start();
});
